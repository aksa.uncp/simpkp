
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?= $app_title ?> | <?= $page_title ?></title>
		<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
		<meta name="description" content="<?= $app_description ?>">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
		<link rel="stylesheet" href="<?= base_url('assets/css/vendor.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/elephant.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/application.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/node_modules/font-awesome/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/third_party/waitme/waitMe.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
	</head>
	<body class="layout layout-header-fixed">
		<div class="layout-header">
			<div class="navbar navbar-default">
				<div class="navbar-header">
					<a class="navbar-brand navbar-brand-center" href="<?= site_url('dashboard') ?>">
						<i class="fa fa-fw fa-user-md"></i> <?= $app_title ?>
					</a>
					<button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
						<span class="sr-only">Toggle navigation</span>
						<span class="bars">
							<span class="bar-line bar-line-1 out"></span>
							<span class="bar-line bar-line-2 out"></span>
							<span class="bar-line bar-line-3 out"></span>
						</span>
						<span class="bars bars-x">
							<span class="bar-line bar-line-4"></span>
							<span class="bar-line bar-line-5"></span>
						</span>
					</button>
					<button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="arrow-up"></span>
						<span class="ellipsis ellipsis-vertical">
							<img class="ellipsis-object" width="32" height="32" src="<?= base_url('assets/images/user-profile.png') ?>" alt="Teddy Wilson">
						</span>
					</button>
				</div>
				<div class="navbar-toggleable">
					<nav id="navbar" class="navbar-collapse collapse">
						<button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
							<span class="sr-only">Toggle navigation</span>
							<span class="bars">
								<span class="bar-line bar-line-1 out"></span>
								<span class="bar-line bar-line-2 out"></span>
								<span class="bar-line bar-line-3 out"></span>
								<span class="bar-line bar-line-4 in"></span>
								<span class="bar-line bar-line-5 in"></span>
								<span class="bar-line bar-line-6 in"></span>
							</span>
						</button>
						<ul class="nav navbar-nav navbar-right">
							<li class="visible-xs-block">
								<h4 class="navbar-text text-center">Hi, <?= $user->nama_lengkap ?></h4>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true">
									<span class="icon-with-child hidden-xs">
										<span class="icon icon-envelope-o icon-lg"></span>
										<span class="badge badge-primary badge-above right"></span>
									</span>
									<span class="visible-xs-block">
										<span class="icon icon-envelope icon-lg icon-fw"></span>
										<span class="badge badge-primary pull-right"></span>
										Pesan
									</span>
								</a>
								<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
									<div class="dropdown-header">
										<a class="dropdown-link" href="compose.html">New Message</a>
										<h5 class="dropdown-heading">Recent messages</h5>
									</div>
									<div class="dropdown-body">
										
									</div>
									<div class="dropdown-footer">
										<a class="dropdown-btn" href="#">See All</a>
									</div>
								</div>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true">
									<span class="icon-with-child hidden-xs">
										<span class="icon icon-bell-o icon-lg"></span>
										<span class="badge badge-primary badge-above right"></span>
									</span>
									<span class="visible-xs-block">
										<span class="icon icon-bell icon-lg icon-fw"></span>
										<span class="badge badge-primary pull-right"></span>
										Notifikasi
									</span>
								</a>
								<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
									<div class="dropdown-header">
										<a class="dropdown-link" href="#">Mark all as read</a>
										<h5 class="dropdown-heading">Recent Notifications</h5>
									</div>
									<div class="dropdown-body">
										<div class="list-group list-group-divided custom-scrollbar">
											
										</div>
									</div>
									<div class="dropdown-footer">
										<a class="dropdown-btn" href="#">See All</a>
									</div>
								</div>
							</li>
							<li class="dropdown hidden-xs">
								<button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
									<img class="circle" width="36" height="36" src="<?= base_url('assets/images/user-profile.png') ?>" alt="<?= $user->nama_lengkap ?>"> <?= $user->nama_lengkap ?>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="<?= site_url('users/profile') ?>">Profil</a></li>
									<li><a href="<?= site_url('logout') ?>">Keluar</a></li>
								</ul>
							</li>
							<li class="visible-xs-block">
								<a href="<?= site_url('users/profile') ?>">
									<span class="icon icon-user icon-lg icon-fw"></span>
									Profil
								</a>
							</li>
							<li class="visible-xs-block">
								<a href="<?= site_url('logout') ?>">
									<span class="icon icon-power-off icon-lg icon-fw"></span>
									Keluar
								</a>
							</li>
						</ul>
						<div class="title-bar hidden-xs">
							<h1 class="title-bar-title">
								<span class="d-ib"><?= $page_title ?></span>
							</h1>
							<p class="title-bar-description">
								<small><?= $page_description ?></small>
							</p>
						</div>
					</nav>
				</div>
			</div>
		</div>
		<div class="layout-main">
			<div class="layout-sidebar">
				<div class="layout-sidebar-backdrop"></div>
				<div class="layout-sidebar-body">
					<div class="custom-scrollbar">
						<?php
							if ( $this->ion_auth->in_group('admin') ) {
								
								require 'includes/administrator_menu.php';
							
							} else if ( $this->ion_auth->in_group('perawat') ) {

								require 'includes/perawat_menu.php';

							}
						?>
					</div>
				</div>
			</div>
			<div class="layout-content">
				<div id="main-content" class="layout-content-body">
					<?= $template['body'] ?>
				</div>
			</div>
			<div class="layout-footer">
				<div class="layout-footer-body">
					<small class="version">Versi 1.0</small>
					<!-- <small class="copyright">2018 &copy; <?= App::get_option('nama_rs') ?></small> -->
				</div>
			</div>
		</div>

		<script type="text/javascript" src="<?= base_url('assets/js/vendor.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/elephant.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/application.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/datatables.net/js/jquery.dataTables.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/moment/min/moment.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/moment/locale/id.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/accounting/accounting.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/waypoints/lib/jquery.waypoints.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/jquery.counterup/jquery.counterup.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/jquery-form/dist/jquery.form.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/sweetalert/dist/sweetalert.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/form-serializer/dist/jquery.serialize-object.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/filesaver.js/FileSaver.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/third_party/waitme/waitMe.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/app.js') ?>"></script>

		<!-- includes -->
		<?php require 'includes/app.js.php' ?>

		<!-- modals -->
		<?php if ( isset($template['partials']['modals']) ) echo $template['partials']['modals']; ?>

		<!-- js -->
		<?php if ( isset($template['partials']['js']) ) echo $template['partials']['js']; ?>
	</body>
</html>