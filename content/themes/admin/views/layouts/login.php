
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?= $app_title ?> | Login</title>
		<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
		<meta name="description" content="<?= $app_description ?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/vendor.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/elephant.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/node_modules/font-awesome/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/login.min.css') ?>">
	</head>
	<body>
		<div class="login">
			<div class="login-body">
				<a class="login-brand" href="<?= site_url() ?>">
					<img class="img-responsive" src="<?= base_url('assets/images/logo.png') ?>" alt="<?= $app_title . ' ' . 'Logo' ?>" />
				</a>
				<h3 class="login-heading">
					<strong><?= $app_title ?></strong><br />
					<small><?= $app_description ?></small>
				</h3>
				<div class="login-form">
					<form id="form-login" role="form">
						<div class="form-group">
							<label for="username" class="control-label">Username</label>
							<input id="username" class="form-control" type="text" name="username" spellcheck="false" autocomplete="off" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<label for="password" class="control-label">Password</label>
							<input id="password" class="form-control" type="password" name="password" required>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group">
							<button class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-fw fa-circle-o-notch fa-spin'></i> loading..." type="submit">Sign in</button>
							<div class="has-error has-danger">
								<div id="login-errors" class="help-block with-errors text-center"></div>
							</div>
						</div>
						<div class="form-group">
							<ul class="list-inline">
								<li>
									<label class="custom-control custom-control-primary custom-checkbox">
										<input name="remember" value="true" class="custom-control-input" type="checkbox">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-label">Ingat saya</span>
									</label>
								</li>
							</ul>
						</div>
					</form>
				</div>
			</div>
			<div class="login-footer">
				<ul class="list-inline">
					<!-- <li>2018 &copy; <?= App::get_option('nama_rs') ?></li> -->
				</ul>
			</div>
		</div>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/jquery/dist/jquery.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/elephant.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/bootstrap-validator/dist/validator.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/form-serializer/dist/jquery.serialize-object.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/node_modules/jquery-form/dist/jquery.form.min.js') ?>"></script>

		<!-- includes -->
		<?php require 'includes/app.login.js.php' ?>
	</body>
</html>