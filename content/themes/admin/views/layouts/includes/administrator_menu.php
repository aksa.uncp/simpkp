<nav id="sidenav" class="sidenav-collapse collapse">
	<ul class="sidenav">
		<li class="sidenav-item">
			<a href="<?= site_url('dashboard') ?>">
				<span class="sidenav-icon icon icon-dashboard"></span>
				<span class="sidenav-label">Dashboard</span>
			</a>
		</li>

		<li class="sidenav-heading">DATA POKOK</li>
		<li class="sidenav-item">
			<a href="<?= site_url('unit_kerja') ?>">
				<span class="sidenav-icon icon icon-institution"></span>
				<span class="sidenav-label">Unit Kerja</span>
			</a>
		</li>
		<li class="sidenav-item has-subnav">
			<a href="#" aria-haspopup="true">
				<span class="sidenav-icon icon icon-database"></span>
				<span class="sidenav-label">Pangkat & Jabatan</span>
			</a>
			<ul class="sidenav-subnav collapse">
				<li><a href="<?= site_url('jabatan') ?>">Jabatan</a></li>
				<li><a href="<?= site_url('pangkat') ?>">Pangkat</a></li>
			</ul>
		</li>
		<li class="sidenav-item">
			<a href="<?= site_url('perawat') ?>">
				<span class="sidenav-icon icon icon-user-md"></span>
				<span class="sidenav-label">Perawat</span>
			</a>
		</li>
		<li class="sidenav-item">
			<a href="<?= site_url('pasien') ?>">
				<span class="sidenav-icon icon icon-wheelchair"></span>
				<span class="sidenav-label">Pasien</span>
			</a>
		</li>
		<li class="sidenav-item has-subnav">
			<a href="#" aria-haspopup="true">
				<span class="sidenav-icon icon icon-stethoscope"></span>
				<span class="sidenav-label">Kewenangan Klinis</span>
			</a>
			<ul class="sidenav-subnav collapse">
				<li><a href="<?= site_url('kewenangan_klinis/kategori') ?>">Kategori Kompetensi</a></li>
				<li><a href="<?= site_url('kewenangan_klinis/kompetensi') ?>">Kewenangan Klinis</a></li>
			</ul>
		</li>

		<li class="sidenav-heading">PENGATURAN</li>
		<li class="sidenav-item">
			<a href="<?= site_url('settings/general') ?>">
				<span class="sidenav-icon icon icon-cog"></span>
				<span class="sidenav-label">Pengaturan Umum</span>
			</a>
		</li>
		<li class="sidenav-item">
			<a href="<?= site_url('target') ?>">
				<span class="sidenav-icon icon icon-check-square-o"></span>
				<span class="sidenav-label">Target Capaian</span>
			</a>
		</li>
		<li class="sidenav-item">
			<a href="<?= site_url('users') ?>">
				<span class="sidenav-icon icon icon-users"></span>
				<span class="sidenav-label">Akses Pengguna</span>
			</a>
		</li>

		<li class="sidenav-heading">TRANSAKSI</li>
		<li class="sidenav-item">
			<a href="<?= site_url('logbook') ?>">
				<span class="sidenav-icon icon icon-book"></span>
				<span class="sidenav-label">Log Book</span>
			</a>
		</li>

		<li class="sidenav-heading">PENILAIAN</li>
		<li class="sidenav-item">
			<a href="<?= site_url('penilaian_kinerja') ?>">
				<span class="sidenav-icon icon icon-book"></span>
				<span class="sidenav-label">Kinerja Perawat</span>
			</a>
		</li>

		<li class="sidenav-heading">AKUN SAYA</li>
		<li class="sidenav-item">
			<a href="<?= site_url('users/profile') ?>">
				<span class="sidenav-icon icon icon-user"></span>
				<span class="sidenav-label">Profil</span>
			</a>
		</li>
	</ul>
</nav>