<script type="text/javascript">
	$(document).ready(function() {
		$('#form-login').validator().on('submit', function (e) {
			if (e.isDefaultPrevented()) {
				// handle the invalid form...
			} else {
				let formData = $('#form-login').serializeObject();
				$.ajax({
					url: '<?= site_url("api/users/users/auth") ?>',
					method: 'post',
					dataType: 'json',
					data: formData,
					beforeSend: function() {
						$('#form-login [type="submit"]').button('loading');
					},
					success: function(xhr) {
						if ( xhr.metadata.code != "200" ) {
							$('#login-errors').html(xhr.metadata.message);
							console.log(xhr);
						}
						else {
							window.location = '<?= site_url("dashboard") ?>';
						}
					},
					error: function(xhr) {
						console.log(xhr);
					},
					complete: function() {
						$('#form-login [type="submit"]').button('reset');
						$('#form-login').clearForm();
					}
				});

				return false;
			}
		})
	});
</script>