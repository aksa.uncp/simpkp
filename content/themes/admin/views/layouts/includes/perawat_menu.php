<nav id="sidenav" class="sidenav-collapse collapse">
	<ul class="sidenav">
		<li class="sidenav-item">
			<a href="<?= site_url('dashboard') ?>">
				<span class="sidenav-icon icon icon-dashboard"></span>
				<span class="sidenav-label">Dashboard</span>
			</a>
		</li>

		<li class="sidenav-heading">TRANSAKSI</li>
		<li class="sidenav-item">
			<a href="<?= site_url('logbook') ?>">
				<span class="sidenav-icon icon icon-book"></span>
				<span class="sidenav-label">Log Book</span>
			</a>
		</li>

		<li class="sidenav-heading">AKUN SAYA</li>
		<li class="sidenav-item">
			<a href="<?= site_url('users/profile') ?>">
				<span class="sidenav-icon icon icon-user"></span>
				<span class="sidenav-label">Profil</span>
			</a>
		</li>
	</ul>
</nav>