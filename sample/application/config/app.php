<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Default timezone
|--------------------------------------------------------------------------
|
*/
date_default_timezone_set('Asia/Makassar');

/*
|--------------------------------------------------------------------------
| Default moment locale
|--------------------------------------------------------------------------
|
*/
\Moment\Moment::setLocale('id_ID');

/*
|--------------------------------------------------------------------------
| Application title
|--------------------------------------------------------------------------
|
| application title
|
*/
$config['app_title'] = 'SIMPKP';

/*
|--------------------------------------------------------------------------
| Application description
|--------------------------------------------------------------------------
|
| application description
|
*/
$config['app_description'] = 'Sistem Informasi Penilaian Kinerja Perawat';

/*
|--------------------------------------------------------------------------
| Application version
|--------------------------------------------------------------------------
|
| application version number
|
*/
$config['app_version'] = '1.0';

/*
|--------------------------------------------------------------------------
| Admin theme
|--------------------------------------------------------------------------
|
| WARNING: You MUST set this value!
|
| admin theme directory name
|
| 	Default: 'admin'
|
*/
$config['admin_theme'] = 'admin';

/*
|--------------------------------------------------------------------------
| Client theme
|--------------------------------------------------------------------------
|
| WARNING: You MUST set this value!
|
| client theme directory name
|
| 	Default: 'client'
|
*/
$config['client_theme'] = 'client';