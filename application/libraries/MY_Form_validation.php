<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * check conflict user email
	 * @param  [type] $str        [description]
	 * @param  [type] $exclude_id [description]
	 * @return [type]             [description]
	 */
	public function conflict_user_email($str, $exclude_id=NULL)
	{
		if ( $exclude_id !== NULL ) $this->CI->db->where_not_in('id', $exclude_id);

		$result = $this->CI->db->select('id')->from('users')->where('email', $str)->get();
		
		if ( $result->num_rows() > 0 ) {

			$this->set_message('conflict_user_email', '{field} telah digunakan oleh pengguna lain.');

			return FALSE;

		} else {

			return TRUE;

		}
	}

	public function unique_field($str, $params)
	{
		$params = explode('.', $params);

		$table = $params[0];
		$field = $params[1];

		if (count($params) === 4) {

			$exclude_field = $params[2];
			$exclude_value = $params[3];

			$this->CI->db->where_not_in($exclude_field, $exclude_value);

		}

		$result = $this->CI->db->select($field)->from($table)->where($field, $str)->get();
		
		if ( $result->num_rows() > 0 ) {

			$this->set_message('unique_field', '{field} telah digunakan.');

			return FALSE;
		
		} else {
			
			return TRUE;
		
		}
	}
}

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */
