<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Application
{

	/**
	 * options information
	 */
	public static function get_option($option=null)
	{
		$res = self::_get_instance()->db->get_where('pengaturan', array('parameter' => $option));
		$row = $res->row();
		return $row->nilai;
	}

	protected static function _get_instance()
	{
		$ci =& get_instance();
		return $ci;
	}

}

class App extends Application {}

/* End of file Application.php */
/* Location: ./application/libraries/Application.php */