<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("indikator/add") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-plus-circle"></i> <span class="hidden-xs">Tambah Kompetensi</span></a>
		</div>
	</div>
	<div class="card-body">
		<?php if ( $data['count'] > 0 ) : ?>
			<ul style="list-style:none;padding:0">
				<?php foreach ( $data['data'] as $row ) : ?>
					<li style="margin-bottom:5px">
						<a href="" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-plus-circle"></i></a>
						<a href="" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-trash"></i></a>
						<a href="" class="btn btn-xs btn-info"><i class="fa fa-fw fa-pencil"></i></a>
						<?= $row->nama_indikator ?>

						<?php if ( $row->child['count'] > 0 ) : ?>
							<ul style="list-style:none;padding-left:31px;margin-top:5px">
								<?php foreach ( $row->child['data'] as $row2 ) : ?>
									<li style="margin-bottom:5px">
										<a href="" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-plus-circle"></i></a>
										<a href="" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-trash"></i></a>
										<a href="" class="btn btn-xs btn-info"><i class="fa fa-fw fa-pencil"></i></a>
										<?= $row2->nama_indikator ?>

										<?php if ( $row2->child['count'] > 0 ) : ?>
											<ul style="list-style:none;padding-left:62px;margin-top:5px">
												<?php foreach ( $row2->child['data'] as $row3 ) : ?>
													<li style="margin-bottom:5px">
														<a href="" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-plus-circle"></i></a>
														<a href="" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-trash"></i></a>
														<a href="" class="btn btn-xs btn-info"><i class="fa fa-fw fa-pencil"></i></a>
														<?= $row3->nama_indikator ?>
													</li>
												<?php endforeach; ?>
											</ul>
										<?php endif; ?>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>