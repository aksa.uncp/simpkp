<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('indikator/m_kategori_indikator', 'm_kategori');
	}

	public function index()
	{
		$this->data['page_title'] = 'Kategori';
		$this->data['page_description'] = 'Daftar Kategori Indikator Penilaian';

		$this->template->set_partial('js', 'indikator/kategori/js/index.js.php');
		$this->template->set_partial('modals', 'indikator/kategori/modals/index.modal.php');

		$this->template->build('indikator/kategori/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah Kategori';
		$this->data['page_description'] = 'Menambahkan Data Kategori Kategori Indikator Penilaian';

		$this->template->set_partial('js', 'indikator/kategori/js/add.js.php');

		$this->template->build('indikator/kategori/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Kategori';
		$this->data['page_description'] = 'Mengubah Data Kategori Indikator Penilaian';

		$this->data['data'] = $this->m_kategori->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'indikator/kategori/js/edit.js.php');

		$this->template->build('indikator/kategori/edit', $this->data);
	}

}

/* End of file kategori.php */
/* Location: ./application/modules/kategori/controllers/kategori.php */