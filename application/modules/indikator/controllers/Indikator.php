<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indikator extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('indikator/m_indikator', 'm_indikator');
	}

	public function index()
	{
		$this->data['page_title'] = 'Indikator';
		$this->data['page_description'] = 'Daftar Indikator Penilaian Kinerja';

		$this->data['data'] = $this->m_indikator->get_indikator_tree();

		$this->template->set_partial('js', 'indikator/indikator/js/index.js.php');
		$this->template->set_partial('modals', 'indikator/indikator/modals/index.modal.php');

		$this->template->build('indikator/indikator/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah Indikator';
		$this->data['page_description'] = 'Menambahkan Data Indikator Penilaian Kinerja';

		$this->template->set_partial('js', 'indikator/indikator/js/add.js.php');

		$this->template->build('indikator/indikator/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Indikator';
		$this->data['page_description'] = 'Mengubah Data Indikator Penilaian Kinerja';

		$this->data['data'] = $this->m_indikator->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'indikator/indikator/js/edit.js.php');

		$this->template->build('indikator/indikator/edit', $this->data);
	}

}

/* End of file indikator.php */
/* Location: ./application/modules/kompetensi/controllers/indikator.php */