<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Indikator extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('indikator/m_indikator', 'm_indikator');
	}

	/**
	 * response for jquery datatables
	 */
	public function datatables_get()
	{
		$query = [];

		// columns index
		$column_index = array(null, 'id_kategori', 'nama_kategori', 'urutan_indikator', 'nama_indikator');

		// filter column handler / search
		$columns = $this->get('columns');
		$search  = $this->get('search');
		$orders  = $this->get('order');
		$length  = $this->get('length');
		$start   = $this->get('start');

		if ( ! empty($search['value']) ) {

			$search_value = xss_clean($search['value']);

			$query['where'][$column_index[4].' LIKE'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 1 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];
					
					} else {

						$query['where'][$column_index[$column_key].' LIKE '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = $this->m_indikator->count($query);

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $orders as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// limit
		$query['limit_offset'] = array($length, $start);

		$data = $this->m_indikator->get($query);

		$this->response([
			'draw' => $this->get('draw'),
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_total,
			'data' => $data['data']
		]);
	}

	/**
	 * add new data
	 */
	public function index_post()
	{
		$validation_rules = [
			[
				'field' => 'nama_kompetensi',
				'label' => 'Nama Kompetensi',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'jenjang_pk',
				'label' => 'Jenjang PK',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_kategori',
				'label' => 'Kategori',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'urutan',
				'label' => 'No. Urut Laporan',
				'rules' => 'required|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'nama_kompetensi' => $this->post('nama_kompetensi'),
				'jenjang_pk'      => $this->post('jenjang_pk'),
				'id_kategori'     => $this->post('id_kategori'),
				'urutan'          => $this->post('urutan')
			];

			if ( $this->m_indikator->insert($data) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menambahkan data.'
					]
				], 500);

			}

		}
	}
 
	/** 
	 * update data 
	 */ 
	public function index_put() 
	{
		$this->form_validation->set_data($this->put());
		$id = $this->put('id', TRUE);
 
		$validation_rules = [
			[
				'field' => 'nama_kompetensi',
				'label' => 'Nama Kompetensi',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'jenjang_pk',
				'label' => 'Jenjang PK',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_kategori',
				'label' => 'Kategori',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'urutan',
				'label' => 'No. Urut Laporan',
				'rules' => 'required|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'nama_kompetensi' => $this->put('nama_kompetensi'),
				'jenjang_pk'      => $this->put('jenjang_pk'),
				'id_kategori'     => $this->put('id_kategori'),
				'urutan'          => $this->put('urutan')
			];

			if ( $this->m_indikator->update($data, $id) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menyimpan perubahan.'
					]
				], 500);

			}

		}
	}

	/**
	 * delete data
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		if ( is_array($id) ) {
			$result = $this->m_indikator->delete_batch($id);
		} else {
			$result = $this->m_indikator->delete($id);
		}

		if ( $result == false ) {
			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => 'Server error. Gagal menghapus data.'
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => 'OK'
				]
			]);
		}
	}

}

/* End of file indikator.php */
/* Location: ./application/controllers/indikator.php */