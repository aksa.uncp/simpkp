<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_indikator extends MY_Model {

	public $table = 'indikator';
	public $view = 'v_indikator';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_indikator_tree($id_parent = NULL)
	{
		$data = $this->get([
			'where' => [
				'id_parent' => $id_parent
			],
			'order_by' => 'urutan_kategori ASC, urutan_indikator ASC'
		]);

		if ( $data['count'] > 0 ) {
			
			for ($i=0; $i < $data['count']; $i++) {

				$child = $this->get_indikator_tree($data['data'][$i]->id);
			
				$data['data'][$i]->child = $child;
			
			}

		}

		return $data;
	}

}

/* End of file M_indikator.php */
/* Location: ./application/modules/kewenangan_klinis/models/M_indikator.php */