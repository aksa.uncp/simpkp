<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori_indikator extends MY_Model {

	public $table = 'kategori_indikator';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_kategori_indikator.php */
/* Location: ./application/modules/kewenangan_klinis/models/M_kategori_indikator.php */