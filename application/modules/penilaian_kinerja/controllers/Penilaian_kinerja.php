<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Moment\Moment;

class Penilaian_kinerja extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('penilaian_kinerja/m_nilai');
		$this->load->model('perawat/m_perawat');
	}

	public function index()
	{
		$this->data['page_title'] = 'Kinerja Perawat';
		$this->data['page_description'] = 'Penilaian Kinerja Perawat';

		$this->template->set_partial('js', 'penilaian_kinerja/js/index.js.php');
		$this->template->set_partial('modals', 'penilaian_kinerja/modals/index.modal.php');

		$this->template->build('penilaian_kinerja/index', $this->data);
	}

	public function input_nilai($id_perawat, $periode)
	{
		$periode = urldecode($periode);

		$this->data['page_title'] = 'Input/Ubah Nilai';
		$this->data['page_description'] = 'Input/Ubah Nilai Perawat';

		$this->data['periode'] = $periode;
		$this->data['moment'] = new Moment($periode);

		$this->data['perawat'] = $this->m_perawat->row([
			'where' => [
				'id' => $id_perawat
			]
		]);

		$this->data['penilaian'] = $this->m_nilai->getPenilaianPerPerawat($id_perawat, $periode);

		$this->template->set_partial('js', 'penilaian_kinerja/js/input_nilai.js.php');

		$this->template->build('penilaian_kinerja/input_nilai', $this->data);
	}

}

/* End of file jabatan.php */
/* Location: ./application/modules/jabatan/controllers/jabatan.php */