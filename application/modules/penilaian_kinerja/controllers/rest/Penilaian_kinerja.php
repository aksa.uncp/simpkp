<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Carbon\Carbon;
use Moment\Moment;

class Penilaian_kinerja extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('penilaian_kinerja/m_nilai');
		$this->load->model('unit_kerja/m_unit_kerja');
	}

	public function rekap_per_unit_kerja_get()
	{
		$this->data['id_unit_kerja'] = $this->get('id_unit_kerja', true);
		$this->data['periode']       = $this->get('periode', true);
		$this->data['moment']        = new Moment($this->data['periode']);

		$this->data['unit_kerja'] = $this->m_unit_kerja->row([
			'where' => [
				'id' => $this->data['id_unit_kerja']
			]
		]);

		$this->data['data'] = $this->m_nilai->getRekapPenilaianPerUnitKerja($this->data['id_unit_kerja'], $this->data['periode']);

		if ($this->data['data']['count'] > 0) {

			$this->load->view('penilaian_kinerja/rekap_penilaian_per_unit_kerja', $this->data);

		} else {

			$this->response([
				'metadata' => [
					'code' => 404,
					'message' => "Tidak ada data perawat ditemukan."
				]
			], 404);

		}
	}
 
	public function index_put() 
	{
		$this->form_validation->set_data($this->put());
		$id = $this->put('id', TRUE);
 
		$validation_rules = [
			[
				'field' => 'id_perawat',
				'label' => 'ID Perawat',
				'rules' => 'required|trim|integer|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$this->db->trans_begin();

			$periode    = Carbon::parse($this->put('periode', TRUE))->format('Y-m-d');
			$id_perawat = $this->put('id_perawat', TRUE);
			$penilaian  = $this->put('penilaian', TRUE);

			if ( count($penilaian) > 0 ) {

				$this->m_nilai->delete([
					'periode' => $periode,
					'id_perawat' => $id_perawat
				]);

				for ($i=0; $i < count($penilaian); $i++) { 

					$this->m_nilai->insert([
						'periode' => $periode,
						'id_perawat' => $id_perawat,
						'id_indikator' => $penilaian[$i]['id'],
						'target' => $penilaian[$i]['target'],
						'capaian' => $penilaian[$i]['capaian'],
						'bobot' => $penilaian[$i]['bobot']
					]);

				}

			}

			if ( $this->db->trans_status() == FALSE ) {

				$this->db->trans_rollback();

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menyimpan data.'
					]
				], 500);

			} else {

				$this->db->trans_commit();

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			}

		}
	}

	/**
	 * delete data
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		if ( is_array($id) ) {
			$result = $this->m_nilai->delete_batch($id);
		} else {
			$result = $this->m_nilai->delete($id);
		}

		if ( $result == false ) {
			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => 'Server error. Gagal menghapus data.'
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => 'OK'
				]
			]);
		}
	}

}

/* End of file jabatan.php */
/* Location: ./application/modules/jabatan/controllers/rest/jabatan.php */