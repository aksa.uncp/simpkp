<div class="text-center">
	<h4>Penilaian Kinerja Perawat</h4>
	<div><?= $unit_kerja->nama_unit_kerja ?></div>
	<div><?= $moment->format('F Y'); ?></div>
</div>
<table class="table table-nowrap table-bordered table-hover" id="table-rekap-penilaian" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th width="25"></th>
			<th>NIP</th>
			<th>Nama Perawat</th>
			<th>Pangkat</th>
			<th>Golongan</th>
			<th>Jabatan</th>
			<th>Nilai</th>
			<th width="10"></th>
			<th width="10"></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data['data'] as $row) : ?>
			<tr>
				<td></td>
				<td><?= $row->nip ?></td>
				<td><?= $row->nama_lengkap ?></td>
				<td><?= $row->nama_pangkat ?></td>
				<td><?= $row->golongan ?></td>
				<td><?= $row->nama_jabatan ?></td>
				<td>
					<?php if ( $row->sudah_dinilai == FALSE ) : ?>
						<i class="text-danger">Belum Dinilai</i>
					<?php else : ?>
						<?= number_format($row->nilai, 1) ?>%
					<?php endif; ?>
				</td>
				<td><a href="<?= site_url('penilaian_kinerja/input_nilai/' . $row->id . '/' . $periode) ?>" title="Input/Ubah Nilai Perawat"><i class="fa fa-fw fa-edit"></i></a></td>
				<td><a href="#" class="btn-download-penilaian-kinerja" title="Download Lembar Penilaian" data-id_perawat="<?= $row->id ?>" data-nama_perawat="<?= $row->nama_lengkap ?>" data-periode="<?= $periode ?>"><i class="fa fa-fw fa-download"></i></a></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<?php require 'js/rekap_penilaian_per_unit_kerja.js.php' ?>