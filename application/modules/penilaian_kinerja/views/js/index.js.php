<script type="text/javascript">
	$(function() {
		$('#form-filter').find('[name="periode"]').datetimepicker({
			format: 'MMM YYYY',
			locale: 'en'
		});

		$('#form-filter').ajaxForm({
			url: '<?= site_url("api/penilaian_kinerja/penilaian_kinerja/rekap_per_unit_kerja") ?>',
			method: 'GET',
			beforeSubmit: function(arr, $form, options) {
				$('#rekap-presensi-result-container').remove();

				$('#main-content').waitMe({
					effect: 'rotation',
					text: 'Tunggu...',
					bg: 'rgba(255,255,255,0.7)',
					color: '#000',
					waitTime: -1,
					textPos: 'vertical'
				});
			},
			success: function(xhr) {
				try {
					$('#form-filter').append(function() {
						let html  = '<div class="card-footer" id="rekap-presensi-result-container">';
								html += xhr;
							html += '</div>';

						return html;
					});
				} catch(e) {
					swal('Oops..', 'Terjadi kesalahan1', 'error');
					console.log(e);
				}
			},
			error: function(xhr) {
				try {
					let error = JSON.parse(xhr.responseText);
					swal({
						title: 'Oops..',
						icon: 'error',
						text: error.metadata.message
					});
				} catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
					console.log(e);
				}
			},
			complete: function(xhr) {
				$('#main-content').waitMe('hide');
			}
		});
	});
</script>