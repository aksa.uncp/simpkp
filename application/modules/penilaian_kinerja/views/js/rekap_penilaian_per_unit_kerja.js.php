<script type="text/javascript">
	$(function() {
		let tableRekapPenilaian;

		tableRekapPenilaian = $('#table-rekap-penilaian').DataTable({
			language: {
				emptyTable: "<center>Tidak ada data ditemukan</center>"
			},
			ordering : true,
			order : [[1,'asc']],
			processing : true,
			responsive: true,
			columnDefs: [
				{ targets: 0, className: "text-center gray", orderable: false },
				{ targets: [1,3,4,5,6,7], className: "text-center" },
				{ targets: 0, responsivePriority: 1 },
				{ targets: 1, responsivePriority: 2 },
				{ targets: -1, responsivePriority: 3, orderable: false },
				{ targets: -2, responsivePriority: 4, orderable: false }
			]
		});

		tableRekapPenilaian.on( 'preDraw', function () {
			tableRekapPenilaian.column(0).nodes().each( function(cell, i) {
				cell.innerHTML = i + 1;
			} );
		} ).draw();

		/**
		 * Download penilaian kinerja perawat
		 */
		$('.btn-download-penilaian-kinerja').on('click', function(e) {
			e.preventDefault();

			let namaPerawat = $(this).data('nama_perawat');

			$.ajax({
				url: '<?= site_url("report/penilaian_kinerja/download/pdf") ?>',
				type: 'GET',
				data: {
					id_perawat: $(this).data('id_perawat'),
					periode: $(this).data('periode')
				},
				cache: false,
				xhr: function() {
					var xhr = new XMLHttpRequest();
					xhr.responseType= 'blob';
					return xhr;
				},
				beforeSend: function() {
					$('body').waitMe({
						effect: 'progressBar',
						text: 'Sedang mengunduh berkas.<br />Mungkin akan memakan waktu beberapa menit...',
						bg: 'rgba(255,255,255,0.7)',
						color: '#000',
						waitTime: -1,
						textPos: 'vertical'
					});
				},
				error: function(e) {
					swal('Maaf..', 'Terjadi error pada server. Tidak dapat mengunduh berkas.', 'error');
					console.log(e);
				},
				success: function(data) {
					var blob = new Blob([data], {type: "application/octet-stream"});
					var fileName = "Penilaian Kinerja Perawat <?= $moment->format('F Y') ?> "+ namaPerawat +".pdf";
					saveAs(blob, fileName);
				},
				complete: function() {
					$('body').waitMe('hide');
				}
			});
		});
	});
</script>