<script type="text/javascript">
	function __hitungBobotKategori(id_kategori) {
		let bobot = 0;

		$('.bobot-kategori-' + id_kategori).each(function( i, e ) {
			bobot += Number($(e).val());
		});

		return bobot;
	}

	function __hitungNilaiKategori(id_kategori) {
		let nilai = 0;

		$('.nilai-kategori-' + id_kategori).each(function( i, e ) {
			nilai += Number($(e).html());
		});

		return nilai;
	}

	function __hitungTotalNilai() {
		let totalNilai = 0;

		$('.jumlah-nilai-kategori').each(function( i, e ) {
			totalNilai += Number($(e).html());
		});

		return totalNilai;
	}

	function __setNilaiKategori(id_kategori) {
		let nilai = __hitungNilaiKategori(id_kategori);
		$('#jumlah-nilai-kategori-' + id_kategori).html(nilai);
	}

	function __setTotalNilai() {
		let totalNilai = __hitungTotalNilai;
		$('#total-nilai').html(totalNilai);
	}

	$(function() {
		$('#form-input-nilai .input-capaian').on('blur', function() {
			let id = $(this).data('id');
			let id_kategori = $(this).data('id_kategori');

			let capaian = Number($(this).val());
			let target = Number($('#target-' + id).val());
			let bobot = Number($('#bobot-' + id).val());

			let nilai = (capaian / target) * bobot;
			nilai = nilai.toFixed(1);

			$('#nilai-' + id).html(nilai);

			__setNilaiKategori(id_kategori);
			__setTotalNilai();
		});

		$('#form-input-nilai').on('submit', function(e) {
			e.preventDefault();

			let data = $(this).serializeObject();

			swal({
				title: 'Simpan Penilaian Kinerja',
				text: "Apakah anda yakin akan menyimpan penilaian kinerja periode <?= $moment->format('F Y') ?> a.n. <?= $perawat->nama_lengkap ?>?",
				icon: 'warning',
				dangerMode: true,
				buttons: {
					cancel: "Tidak",
					confirm: "Iya"
				}
			})
			.then((isConfirm) => {
				if (isConfirm) {
					$.ajax({
						url: '<?= site_url("api/penilaian_kinerja/penilaian_kinerja") ?>',
						method: 'PUT',
						dataType: 'json',
						data: data,
						beforeSend: function() {
							$('#main-content').waitMe({
								effect: 'rotation',
								text: 'Tunggu...',
								bg: 'rgba(255,255,255,0.7)',
								color: '#000',
								waitTime: -1,
								textPos: 'vertical'
							});
						},
						success: function(xhr) {
							try {
								let data = xhr.response;
								swal('Berhasil', 'Berhasil menyimpan penilaian', 'success');
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						error: function(xhr) {
							try {
								let error = JSON.parse(xhr.responseText);
								swal({
									title: 'Oops..',
									icon: 'error',
									text: error.metadata.message
								});
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						complete: function(xhr) {
							$('#main-content').waitMe('hide');
						}
					});
				}
			});
		});
	});
</script>