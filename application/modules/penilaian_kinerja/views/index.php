<div class="card">
	<form id="form-filter" class="form-horizontal" role="form">
		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="text-center m-b">
					<h3 class="m-b-0"><i class="fa fa-fw fa-book"></i> Kinerja Perawat</h3>
					<small>Penilaian Kinerja Perawat</small>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Unit Kerja</label>
					<div class="col-xs-9">
						<?= dynamic_dropdown([
							'name' => 'id_unit_kerja',
							'table' => 'unit_kerja',
							'key' => 'id',
							'label' => 'nama_unit_kerja',
							'selected' => '',
							'empty_first' => FALSE,
							'empty_first_label' => '- Pilih -',
							'attr' => 'class="form-control select2-single"'
						]) ?>
					</div>
				</div>
				<div id="jenis-rekap-perbulan" class="form-group">
					<label class="control-label col-xs-3">Bulan</label>
					<div class="col-xs-9">
						<input name="periode" value="<?= date('Y-m') ?>" type="text" class="form-control" autocomplete="off" />
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-search"></i> Tampilkan</button>
			</div>
		</div>
	</form>
</div>