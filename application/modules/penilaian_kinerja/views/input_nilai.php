<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("penilaian_kinerja") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<div class="card-header">
		<div class="text-center">
			<h4>Penilaian Kinerja Perawat</h4>
			<div><?= $moment->format('F Y') ?></div>
			<div><?= $perawat->nama_unit_kerja ?></div>
		</div>
	</div>
	<div class="card-header">
		<div class="form-horizontal zero-line-height">
			<div class="row">
				<label class="control-label col-xs-6">Nama Perawat</label>
				<div class="col-xs-6">
					<p class="form-control-static"><?= $perawat->nama_lengkap ?></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-xs-6">NIP</label>
				<div class="col-xs-6">
					<p class="form-control-static"><?= $perawat->nip ?></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-xs-6">Pangkat</label>
				<div class="col-xs-6">
					<p class="form-control-static"><?= $perawat->nama_pangkat ?></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-xs-6">Golongan</label>
				<div class="col-xs-6">
					<p class="form-control-static"><?= $perawat->golongan ?></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-xs-6">Jabatan</label>
				<div class="col-xs-6">
					<p class="form-control-static"><?= $perawat->nama_jabatan ?></p>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-xs-6">Jenjang PK</label>
				<div class="col-xs-6">
					<p class="form-control-static"><?= $perawat->nama_jenjang_pk ?></p>
				</div>
			</div>
		</div>
	</div>
	<form id="form-input-nilai" class="form-horizontal" role="form">
		<input type="hidden" name="id_perawat" value="<?= $perawat->id ?>" />
		<input type="hidden" name="periode" value="<?= $periode ?>" />

		<div class="card-body">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2">INDIKATOR PENILAIAN</th>
						<th>TARGET</th>
						<th>SATUAN</th>
						<th>CAPAIAN</th>
						<th>BOBOT</th>
						<th>NILAI</th>
					</tr>
					<tr>
						<th colspan="2">1</th>
						<th>2</th>
						<th>3</th>
						<th>4</th>
						<th>5</th>
						<th>(4/2)x5</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$alpha = range('A', 'Z');

					$index = 0;
					$index_penilaian = 0;
					$index_kategori = 0;
					$index_indikator = 1;
					$id_kategori = $penilaian['data'][0]->id_kategori;

					$jumlah_bobot = 0;
					$jumlah_nilai = 0;
					$total_nilai = 0;
					?>
					<?php foreach ( $penilaian['data'] as $row ) : ?>

						<?php if ( $row->has_child == 0 ) : ?>
							<input type="hidden" name="penilaian[<?= $index_penilaian ?>][id]" value="<?= $row->id ?>" />
						<?php endif; ?>

						<?php if ( $index == 0 ) : ?>
							<tr class="gray">
								<td class="text-center" style="font-weight:bold" width="30"><?= $alpha[$index_kategori] ?></td>
								<td style="font-weight:bold"><?= strtoupper($row->nama_kategori) ?></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
							</tr>

							<?php $index_kategori++; ?>
						<?php endif; ?>
						
						<?php if ( $id_kategori != $row->id_kategori ) : ?>
							<?php $id_kategori = $row->id_kategori; ?>
							<?php $index_indikator = 1; ?>

							<tr class="gray">
								<td colspan="2" class="text-center" style="font-weight:bold">Jumlah</td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center">
									<span id="jumlah-bobot-kategori-<?= $penilaian['data'][$index - 1]->id_kategori ?>"><?= $jumlah_bobot ?></span>%
								</td>
								<td class="text-center">
									<span class="jumlah-nilai-kategori" id="jumlah-nilai-kategori-<?= $penilaian['data'][$index - 1]->id_kategori ?>"><?= $jumlah_nilai ?></span>%
								</td>
							</tr>
							<tr class="gray">
								<td class="text-center" style="font-weight:bold"><?= $alpha[$index_kategori] ?></td>
								<td style="font-weight:bold"><?= strtoupper($row->nama_kategori) ?></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td class="text-center">
									<?= ($row->parent == 0) ? $index_indikator : '' ?>
								</td>
								<td>
									<?= $row->nama_indikator ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?><input type="hidden" class="target-kategori-<?= $row->id_kategori ?>" id="target-<?= $row->id ?>" name="penilaian[<?= $index_penilaian ?>][target]" value="<?= $row->target ?>" /><?php endif; ?>
									<?= $row->target ?>
								</td>
								<td class="text-center">
									<?= $row->satuan ?>
								</td>
								<td class="text-center" width="50">
									<?php if ( $row->has_child == 0 ) : ?>
										<input type="text" id="capaian-<?= $row->id ?>" data-id="<?= $row->id ?>" data-id_kategori="<?= $row->id_kategori ?>" name="penilaian[<?= $index_penilaian ?>][capaian]" class="capaian-kategori-<?= $row->id_kategori ?> input-capaian text-center" value="<?= $row->capaian ?>" style="width:100%;padding:0;height:20px;border:solid 1px #B2B2B2" <?= ( $row->id == 2 || $row->id == 3 || $row->id == 4 || $row->id == 5 || $row->id == 6 || $row->id == 7 || $row->id == 8 || $row->id == 9 ) ? 'readonly' : '' ?> />
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?>
										<?php if ( $row->has_child == 0 ) : ?><input type="hidden" class="bobot-kategori-<?= $row->id_kategori ?>" id="bobot-<?= $row->id ?>" name="penilaian[<?= $index_penilaian ?>][bobot]" value="<?= $row->bobot ?>" /><?php endif; ?>
										<?= $row->bobot ?>%
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?>
										<span class="nilai-kategori-<?= $row->id_kategori ?>" id="nilai-<?= $row->id ?>"><?= $row->nilai ?></span>%
									<?php endif; ?>
								</td>
							</tr>

							<?php if ( $row->id == 6 ) : ?>
								<?php if ( $row->tindakan['count'] > 0 ) : ?>
									<?php foreach ( $row->tindakan['data'] as $tindakan ) : ?>
										<tr>
											<td></td>
											<td><?= $tindakan->nama_kategori ?></td>
											<td></td>
											<td></td>
											<td class="text-center"><?= $tindakan->jumlah_tindakan ?></td>
											<td></td>
											<td></td>
										</tr>
									<?php endforeach; ?>
								<?php endif; ?>
							<?php endif; ?>

							<?php $jumlah_bobot = 0; ?>
							<?php $jumlah_nilai = 0; ?>
							<?php $index_kategori++; ?>
						<?php else : ?>
							<tr>
								<td class="text-center">
									<?= ($row->parent == 0) ? $index_indikator : '' ?>
								</td>
								<td>
									<?= $row->nama_indikator ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?><input type="hidden" class="target-kategori-<?= $row->id_kategori ?>" id="target-<?= $row->id ?>" name="penilaian[<?= $index_penilaian ?>][target]" value="<?= $row->target ?>" /><?php endif; ?>
									<?= $row->target ?>
								</td>
								<td class="text-center">
									<?= $row->satuan ?>
								</td>
								<td class="text-center" width="50">
									<?php if ( $row->has_child == 0 ) : ?>
										<input type="text" id="capaian-<?= $row->id ?>" data-id="<?= $row->id ?>" data-id_kategori="<?= $row->id_kategori ?>" name="penilaian[<?= $index_penilaian ?>][capaian]" class="capaian-kategori-<?= $row->id_kategori ?> input-capaian text-center" value="<?= $row->capaian ?>" style="width:100%;padding:0;height:20px;border:solid 1px #B2B2B2" <?= ( $row->id == 2 || $row->id == 3 || $row->id == 4 || $row->id == 5 || $row->id == 6 || $row->id == 7 || $row->id == 8 || $row->id == 9 ) ? 'readonly' : '' ?> />
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?>
										<?php if ( $row->has_child == 0 ) : ?><input type="hidden" class="bobot-kategori-<?= $row->id_kategori ?>" id="bobot-<?= $row->id ?>" name="penilaian[<?= $index_penilaian ?>][bobot]" value="<?= $row->bobot ?>" /><?php endif; ?>
										<?= $row->bobot ?>%
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?>
										<span class="nilai-kategori-<?= $row->id_kategori ?>" id="nilai-<?= $row->id ?>"><?= $row->nilai ?></span>%
									<?php endif; ?>
								</td>
							</tr>

							<?php if ( $row->id == 6 ) : ?>
								<?php if ( $row->tindakan['count'] > 0 ) : ?>
									<?php foreach ( $row->tindakan['data'] as $tindakan ) : ?>
										<tr>
											<td></td>
											<td style="font-style:italic"><?= $tindakan->nama_kategori ?></td>
											<td></td>
											<td></td>
											<td class="text-center"><?= $tindakan->jumlah_tindakan ?></td>
											<td></td>
											<td></td>
										</tr>
									<?php endforeach; ?>
								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>

						<?php $jumlah_bobot += $row->bobot; ?>
						<?php $jumlah_nilai += $row->nilai; ?>
						<?php $total_nilai += $row->nilai; ?>

						<?php if ( $index == ($penilaian['count'] - 1) ) : ?>
							<tr class="gray">
								<td colspan="2" class="text-center" style="font-weight:bold">Jumlah</td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center">
									<span id="jumlah-bobot-kategori-<?= $row->id_kategori ?>"><?= $jumlah_bobot ?></span>%
								</td>
								<td class="text-center">
									<span class="jumlah-nilai-kategori" id="jumlah-nilai-kategori-<?= $row->id_kategori ?>"><?= $jumlah_nilai ?></span>%
								</td>
							</tr>
							<tr>
								<td colspan="6" class="text-center" style="font-weight:bold">TOTAL NILAI KRITERIA INDIVIDU</td>
								<td class="text-center" style="font-weight:bold"><span id="total-nilai"><?= $total_nilai ?></span>%</td>
							</tr>
						<?php endif; ?>

						<?php $index_indikator += ($row->parent == 0) ? 1 : 0; ?>
						<?php $index++; ?>
						<?php $index_penilaian += ($row->has_child == 0) ? 1 : 0; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('penilaian_kinerja') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>