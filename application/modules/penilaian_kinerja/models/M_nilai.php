<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class M_nilai extends MY_Model {

	public $table = 'nilai';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getRekapPenilaianPerUnitKerja($id_unit_kerja, $periode)
	{
		$periode  = Carbon::parse($periode)->format('Y-m-d');

		$query = $this->db->query("CALL px_rekap_penilaian_per_unit_kerja('{$id_unit_kerja}','{$periode}')");
		$data = $query->result();
		mysqli_next_result($this->db->conn_id);

		return [
			'count' => $query->num_rows(),
			'data' => $data
		];
	}

	public function getPenilaianPerPerawat($id_perawat, $periode)
	{
		$periode  = Carbon::parse($periode)->format('Y-m-d');

		$query = $this->db->query("CALL px_penilaian_per_perawat('{$id_perawat}','{$periode}')");
		$data = $query->result();
		mysqli_next_result($this->db->conn_id);

		if ( $query->num_rows() > 0 ) {
			
			for ($i=0; $i < count($data); $i++) { 

				if ( $data[$i]->id == 6 ) {

					$data[$i]->tindakan = $this->getTindakanPerawat($id_perawat, $periode);

				}

			}

		}

		return [
			'count' => $query->num_rows(),
			'data' => $data
		];
	}

	public function getTindakanPerawat($id_perawat, $periode)
	{
		$periode  = Carbon::parse($periode)->format('Y-m-d');

		$query = $this->db->query("CALL px_tindakan_perawat('{$id_perawat}','{$periode}')");
		$data = $query->result();
		mysqli_next_result($this->db->conn_id);

		return [
			'count' => $query->num_rows(),
			'data' => $data
		];
	}

}

/* End of file M_nilai.php */
/* Location: ./application/modules/penilaian_kinerja/models/M_nilai.php */