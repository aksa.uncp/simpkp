<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Target extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('target/m_indikator');
	}
 
	public function index_put() 
	{
		$this->form_validation->set_data($this->put());
 
		$validation_rules = [
			[
				'field' => 'indikator[]',
				'label' => 'Indikator',
				'rules' => 'required|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$this->db->trans_begin();

			$indikator = $this->put('indikator', TRUE);

			if ( count($indikator) > 0 ) {

				for ($i=0; $i < count($indikator); $i++) { 

					$this->m_indikator->update([
						'target' => $indikator[$i]['target'],
						'satuan' => $indikator[$i]['satuan'],
						'bobot' => $indikator[$i]['bobot']
					], [
						'id' => $indikator[$i]['id']
					]);

				}

			}

			if ( $this->db->trans_status() == FALSE ) {

				$this->db->trans_rollback();

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menyimpan pengaturan.'
					]
				], 500);

			} else {

				$this->db->trans_commit();

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			}

		}
	}

	/**
	 * delete data
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		if ( is_array($id) ) {
			$result = $this->m_nilai->delete_batch($id);
		} else {
			$result = $this->m_nilai->delete($id);
		}

		if ( $result == false ) {
			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => 'Server error. Gagal menghapus data.'
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => 'OK'
				]
			]);
		}
	}

}

/* End of file Target.php */
/* Location: ./application/modules/target/controllers/rest/Target.php */