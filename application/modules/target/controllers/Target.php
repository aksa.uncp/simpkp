<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('target/m_indikator');
	}

	public function index()
	{
		$this->data['page_title'] = 'Target Kinerja';
		$this->data['page_description'] = 'Pengaturan Target Kinerja';

		$this->data['indikator'] = $this->m_indikator->get();

		$this->template->set_partial('js', 'target/js/index.js.php');

		$this->template->build('target/index', $this->data);
	}

}

/* End of file Target.php */
/* Location: ./application/modules/target/controllers/Target.php */