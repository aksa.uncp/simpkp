<div class="card">
	<form id="form-indikator" class="form-horizontal" role="form">
		<div class="card-body">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2">INDIKATOR PENILAIAN</th>
						<th>TARGET</th>
						<th>SATUAN</th>
						<th>BOBOT</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$alpha = range('A', 'Z');

					$index                = 0;
					$index_indikator      = 0;
					$index_kategori       = 0;
					$index_nama_indikator = 1;
					$id_kategori          = $indikator['data'][0]->id_kategori;
					$jumlah_bobot         = 0;
					?>
					<?php foreach ( $indikator['data'] as $row ) : ?>

						<?php if ( $row->has_child == FALSE ) : ?>
							<input type="hidden" name="indikator[<?= $index_indikator ?>][id]" value="<?= $row->id ?>" />
						<?php endif; ?>

						<?php if ( $index == 0 ) : ?>
							<tr class="gray">
								<td class="text-center" style="font-weight:bold" width="30"><?= $alpha[$index_kategori] ?></td>
								<td style="font-weight:bold"><?= strtoupper($row->nama_kategori) ?></td>
								<td class="text-center" width="100"></td>
								<td class="text-center" width="100"></td>
								<td class="text-center" width="100"></td>
							</tr>

							<?php $index_kategori++; ?>
						<?php endif; ?>
						
						<?php if ( $id_kategori != $row->id_kategori ) : ?>
							<?php $id_kategori = $row->id_kategori; ?>
							<?php $index_nama_indikator = 1; ?>

							<tr class="gray">
								<td class="text-center" style="font-weight:bold"><?= $alpha[$index_kategori] ?></td>
								<td style="font-weight:bold"><?= strtoupper($row->nama_kategori) ?></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td class="text-center">
									<?= ( $row->id_parent == FALSE ) ? $index_nama_indikator : '' ?>
								</td>
								<td>
									<?= $row->nama_indikator ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == FALSE ) : ?>
										<input type="text" style="width:100%;padding:0;height:20px;border:solid 1px #B2B2B2" class="target-kategori-<?= $row->id_kategori ?> text-center" id="target-<?= $row->id ?>" name="indikator[<?= $index_indikator ?>][target]" value="<?= $row->target ?>" />
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == FALSE ) : ?>
										<input type="text" style="width:100%;padding:0;height:20px;border:solid 1px #B2B2B2" class="satuan-kategori-<?= $row->id_kategori ?> text-center" id="satuan-<?= $row->id ?>" name="indikator[<?= $index_indikator ?>][satuan]" value="<?= $row->satuan ?>" />
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == FALSE ) : ?>
										<input type="text" style="width:100%;padding:0;height:20px;border:solid 1px #B2B2B2" class="bobot-kategori-<?= $row->id_kategori ?> text-center" id="bobot-<?= $row->id ?>" name="indikator[<?= $index_indikator ?>][bobot]" value="<?= $row->bobot ?>" />
									<?php endif; ?>	
								</td>
							</tr>

							<?php $jumlah_bobot = 0; ?>
							<?php $index_kategori++; ?>
						<?php else : ?>
							<tr>
								<td class="text-center">
									<?= ($row->id_parent == FALSE) ? $index_nama_indikator : '' ?>
								</td>
								<td>
									<?= $row->nama_indikator ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == FALSE ) : ?>
										<input type="text" style="width:100%;padding:0;height:20px;border:solid 1px #B2B2B2" class="target-kategori-<?= $row->id_kategori ?> text-center" id="target-<?= $row->id ?>" name="indikator[<?= $index_indikator ?>][target]" value="<?= $row->target ?>" />
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == FALSE ) : ?>
										<input type="text" style="width:100%;padding:0;height:20px;border:solid 1px #B2B2B2" class="satuan-kategori-<?= $row->id_kategori ?> text-center" id="satuan-<?= $row->id ?>" name="indikator[<?= $index_indikator ?>][satuan]" value="<?= $row->satuan ?>" />
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == FALSE ) : ?>
										<input type="text" style="width:100%;padding:0;height:20px;border:solid 1px #B2B2B2" class="bobot-kategori-<?= $row->id_kategori ?> text-center" id="bobot-<?= $row->id ?>" name="indikator[<?= $index_indikator ?>][bobot]" value="<?= $row->bobot ?>" />
									<?php endif; ?>	
								</td>
							</tr>
						<?php endif; ?>

						<?php $jumlah_bobot += $row->bobot; ?>
						<?php $index_nama_indikator += ( $row->id_parent == FALSE ) ? 1 : 0; ?>
						<?php $index++; ?>
						<?php $index_indikator += ( $row->has_child == FALSE ) ? 1 : 0; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('target') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>