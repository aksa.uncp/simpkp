<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class M_indikator extends MY_Model {

	public $table = 'indikator';
	public $view = 'v_indikator';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

}

/* End of file M_indikator.php */
/* Location: ./application/modules/target/models/M_indikator.php */