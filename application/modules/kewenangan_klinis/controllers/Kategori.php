<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('kewenangan_klinis/m_kategori_kewenangan_klinis', 'm_kategori');
	}

	public function index()
	{
		$this->data['page_title'] = 'Kategori';
		$this->data['page_description'] = 'Daftar Kategori Kewenangan Klinis';

		$this->template->set_partial('js', 'kewenangan_klinis/kategori/js/index.js.php');
		$this->template->set_partial('modals', 'kewenangan_klinis/kategori/modals/index.modal.php');

		$this->template->build('kewenangan_klinis/kategori/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah Kategori';
		$this->data['page_description'] = 'Menambahkan Data Kategori Kewenangan Klinis';

		$this->template->set_partial('js', 'kewenangan_klinis/kategori/js/add.js.php');

		$this->template->build('kewenangan_klinis/kategori/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Kategori';
		$this->data['page_description'] = 'Mengubah Data Kategori Kewenangan Klinis';

		$this->data['data'] = $this->m_kategori->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'kewenangan_klinis/kategori/js/edit.js.php');

		$this->template->build('kewenangan_klinis/kategori/edit', $this->data);
	}

}

/* End of file kategori.php */
/* Location: ./application/modules/kategori/controllers/kategori.php */