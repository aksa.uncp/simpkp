<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tindakan extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('kewenangan_klinis/m_tindakan');
	}

	/**
	 * response for jquery datatables
	 */
	public function datatables_get()
	{
		$query = [];

		if ( $this->ion_auth->in_group('perawat') ) {

			$query['where']['jenjang_pk <='] = $this->userdata->jenjang_pk;

		}

		// columns index
		$column_index = array(null, 'id_kategori', 'nama_kategori', 'urutan', 'nama_kompetensi', 'jenjang_pk', 'nama_jenjang_pk');

		// filter column handler / search
		$columns = $this->get('columns');
		$search  = $this->get('search');
		$orders  = $this->get('order');
		$length  = $this->get('length');
		$start   = $this->get('start');

		if ( ! empty($search['value']) ) {

			$search_value = xss_clean($search['value']);

			$query['where'][$column_index[4].' LIKE'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 2 || $column_key == 4 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];
					
					} else {

						$query['where'][$column_index[$column_key].' LIKE '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = $this->m_tindakan->count($query);

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $orders as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// limit
		$query['limit_offset'] = array($length, $start);

		$data = $this->m_tindakan->get($query);

		$this->response([
			'draw' => $this->get('draw'),
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_total,
			'data' => $data['data']
		]);
	}

}

/* End of file tindakan.php */
/* Location: ./application/controllers/tindakan.php */