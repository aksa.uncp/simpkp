<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kompetensi extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('kewenangan_klinis/m_kewenangan_klinis', 'm_kompetensi');
	}

	public function index()
	{
		$this->data['page_title'] = 'Kewenangan Klinis';
		$this->data['page_description'] = 'Daftar Kompetensi Kewenangan Klinis';

		$this->template->set_partial('js', 'kewenangan_klinis/kompetensi/js/index.js.php');
		$this->template->set_partial('modals', 'kewenangan_klinis/kompetensi/modals/index.modal.php');

		$this->template->build('kewenangan_klinis/kompetensi/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah Kewenangan Klinis';
		$this->data['page_description'] = 'Menambahkan Data kompetensi Kewenangan Klinis';

		$this->template->set_partial('js', 'kewenangan_klinis/kompetensi/js/add.js.php');

		$this->template->build('kewenangan_klinis/kompetensi/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Kewenangan Klinis';
		$this->data['page_description'] = 'Mengubah Data kompetensi Kewenangan Klinis';

		$this->data['data'] = $this->m_kompetensi->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'kewenangan_klinis/kompetensi/js/edit.js.php');

		$this->template->build('kewenangan_klinis/kompetensi/edit', $this->data);
	}

}

/* End of file kompetensi.php */
/* Location: ./application/modules/kompetensi/controllers/kompetensi.php */