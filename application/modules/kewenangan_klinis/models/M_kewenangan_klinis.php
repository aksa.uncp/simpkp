<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kewenangan_klinis extends MY_Model {

	public $table = 'kewenangan_klinis';
	public $view = 'v_kewenangan_klinis';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_kewenangan_klinis.php */
/* Location: ./application/modules/kewenangan_klinis/models/M_kewenangan_klinis.php */