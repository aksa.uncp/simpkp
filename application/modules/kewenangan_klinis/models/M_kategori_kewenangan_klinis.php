<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori_kewenangan_klinis extends MY_Model {

	public $table = 'kategori_kewenangan_klinis';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_kategori_kewenangan_klinis.php */
/* Location: ./application/modules/kewenangan_klinis/models/M_kategori_kewenangan_klinis.php */