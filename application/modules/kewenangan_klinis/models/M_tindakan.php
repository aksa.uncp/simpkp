<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tindakan extends MY_Model {

	public $table = 'kewenangan_klinis';
	public $view = 'v_tindakan';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file M_tindakan.php */
/* Location: ./application/modules/kewenangan_klinis/models/M_tindakan.php */