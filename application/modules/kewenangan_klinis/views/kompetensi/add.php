<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("kewenangan_klinis/kompetensi") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<form id="form-add" class="form-horizontal" role="form">
		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="group">
					<div class="form-group">
						<label class="control-label col-xs-3">Kewenangan Klinis</label>
						<div class="col-xs-9">
							<input name="nama_kompetensi" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Jenjang PK</label>
						<div class="col-xs-9">
							<?= form_dropdown([
								'name' => 'jenjang_pk',
								'options' => [
									1 => 'PK I',
									2 => 'PK II',
									3 => 'PK III',
									4 => 'PK IV',
									5 => 'PK V'
								],
								'selected' => 'PK I',
								'class' => 'form-control select2-single'
							]) ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Kategori Kompetensi</label>
						<div class="col-xs-9">
							<?= dynamic_dropdown([
								'name' => 'id_kategori',
								'table' => 'kategori_kewenangan_klinis',
								'key' => 'id',
								'label' => 'nama_kategori',
								'selected' => '',
								'empty_first' => FALSE,
								'empty_first_label' => '- Pilih -',
								'attr' => 'class="form-control select2-single"'
							]) ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">No. Urut Laporan</label>
						<div class="col-xs-9">
							<input name="urutan" type="text" class="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('kewenangan_klinis/kompetensi') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>