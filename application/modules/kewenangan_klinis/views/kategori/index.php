<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("kewenangan_klinis/kategori/add") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-plus-circle"></i> <span class="hidden-xs">Tambah Kategori</span></a> 
			<a href="javascript:void(0)" id="remove-selected" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i> <span class="hidden-xs">Hapus</span></a> 
			<a href="#modal-search" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-fw fa-search"></i> <span class="hidden-xs">Pencarian</span></a> 
		</div>
	</div>
	<div class="card-body">
		<table class="table table-nowrap table-bordered table-hover" id="table-kategori-kewenangan-klinis" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="25"></th>
					<th width="5">Urutan</th>
					<th>Kategori Kompetensi</th>
					<th width="5"></th>
					<th width="5"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>