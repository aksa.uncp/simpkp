<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('dashboard/m_dashboard');
	}

	public function logBookChart_get()
	{
		$idPerawat = ( $this->ion_auth->in_group('perawat') ) ? $this->userdata->id : '';
		
		$beginDate = $this->get('beginDate');
		$endDate   = $this->get('endDate');

		$presensiSummary = $this->m_dashboard->dashboardLogBookChart($beginDate, $endDate, $idPerawat);

		$result = [
			'labels' => [],
			'values' => []
		];

		foreach ($presensiSummary as $row) {
			// date format
			$moment = new \Moment\Moment($row->tgl);
			$tgl = $moment->format('d M Y');

			$result['labels'][] = $tgl;
			$result['values'][] = $row->jumlah_log_book;
		}

		$this->response([
			'metadata' => [
				'code' => 200,
				'message' => "OK"
			],
			'response' => $result
		]);
	}

	public function badgeInfo_get()
	{
		$badgeInfo = $this->m_dashboard->dashboardBadgeInfo();

		$result = (object) [
			'jumlahPerawatPK1' => $badgeInfo->jumlah_perawat_pk_1,
			'jumlahPerawatPK2' => $badgeInfo->jumlah_perawat_pk_2,
			'jumlahPerawatPK3' => $badgeInfo->jumlah_perawat_pk_3,
			'jumlahPerawatPK4' => $badgeInfo->jumlah_perawat_pk_4,
			'jumlahPerawatPK5' => $badgeInfo->jumlah_perawat_pk_5,
			'totalPerawat' => $badgeInfo->total_perawat
		];

		$this->response([
			'metadata' => [
				'code' => 200,
				'message' => "OK"
			],
			'response' => $result
		]);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */