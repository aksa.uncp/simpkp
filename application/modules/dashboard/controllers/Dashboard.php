<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['page_title'] = "Beranda";
		$this->data['page_description'] = "Rangkuman informasi";

		// js
		$this->template->set_partial('js', 'dashboard/js/index.js.php');

		$this->template->build('index', $this->data);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */