<script type="text/javascript">
	var logBookChartData = {
		labels: [],
		datasets: [{
			label: 'Tindakan Perawat',
			data: [],
			backgroundColor: 'rgba(255,99,132,0.2)',
			borderColor: 'rgba(255,99,132,1)',
			borderWidth: 1
		}]
	};

	function updatelogBookChartData(beginDate, endDate, obj) {
		$.ajax({
			url: '<?= site_url('api/dashboard/dashboard/logBookChart') ?>',
			method: 'get',
			dataType: 'json',
			data: {
				beginDate: beginDate,
				endDate: endDate
			},
			beforeSend: function() {
				$('#logBookChartContainer').waitMe({
					effect : 'rotation',
					text : '',
					bg : 'rgba(255,255,255,0.7)',
					color : '#000',
					maxSize : '',
					waitTime : -1,
					textPos : 'vertical',
					fontSize : '',
					source : '',
					onClose : function() {}
				});
			},
			success: function(xhr) {
				try {
					let data = xhr.response;
					logBookChartData.labels = data.labels;
					logBookChartData.datasets[0].data = data.values;
					obj.update();
				} catch(e) {
					$.messager.alert('Error', 'Terjadi kesalahan', 'error');
					console.log(e);
				}
			},
			error: function(xhr) {
				try {
					let error = JSON.parse(xhr.responseText);
					$.messager.alert('Error', error.metadata.message, 'error');
				} catch(e) {
					$.messager.alert('Error', 'Terjadi kesalahan', 'error');
					console.log(e);
				}
			},
			complete: function(xhr) {
				$('#logBookChartContainer').waitMe('hide');
			}
		});
	}

	function updateBadgeInfo() {
		$.ajax({
			url: '<?= site_url('api/dashboard/dashboard/badgeInfo') ?>',
			method: 'get',
			dataType: 'json',
			beforeSend: function() {
				
			},
			success: function(xhr) {
				try {
					new Promise(function(resolve, reject) {
						let data = xhr.response;
						$('#jumlah-perawat-pk-1').html(accounting.formatNumber(data.jumlahPerawatPK1));
						$('#jumlah-perawat-pk-2').html(accounting.formatNumber(data.jumlahPerawatPK2));
						$('#jumlah-perawat-pk-3').html(accounting.formatNumber(data.jumlahPerawatPK3));
						$('#jumlah-perawat-pk-4').html(accounting.formatNumber(data.jumlahPerawatPK4));
						$('#jumlah-perawat-pk-5').html(accounting.formatNumber(data.jumlahPerawatPK5));
						$('#total-perawat').html(accounting.formatNumber(data.totalPerawat));
						resolve(true);
					}).then(function(result) {
						$('.counter').counterUp({
							delay: 10,
							time: 1000
						});
					}).catch(function(result) {});
				} catch(e) {
					$.messager.alert('Error', 'Terjadi kesalahan', 'error');
					console.log(e);
				}
			},
			error: function(xhr) {
				try {
					let error = JSON.parse(xhr.responseText);
					$.messager.alert('Error', error.metadata.message, 'error');
				} catch(e) {
					$.messager.alert('Error', 'Terjadi kesalahan', 'error');
					console.log(e);
				}
			},
			complete: function(xhr) {
				
			}
		});
	}

	$(function() {
		var ctx = document.getElementById("logBookChart").getContext('2d');
		var logBookChart = new Chart(ctx, {
			type: 'line',
			data: logBookChartData,
			options: {
				scales: {
					xAxes: [{
						ticks: {
							autoSkip: false
						}
					}],
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});

		$('[name="logBookChartRange"]').on('change', function(e) {
			if ($(this).val() == 1) {
				updatelogBookChartData(
					moment().startOf('week').format('YYYY-MM-DD'),
					moment().endOf('week').format('YYYY-MM-DD'),
					logBookChart
				);
			} else {
				updatelogBookChartData(
					moment().startOf('month').format('YYYY-MM-DD'),
					moment().endOf('month').format('YYYY-MM-DD'),
					logBookChart
				);
			}
		});

		// update badge info
		updateBadgeInfo();

		// update chart
		$('[name="logBookChartRange"][value="2"]').trigger('click').trigger('change');
	});
</script>