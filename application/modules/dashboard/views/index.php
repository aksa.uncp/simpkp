<div class="row gutter-xs">
	<div class="col-md-2 col-lg-2">
		<div class="card bg-primary">
			<div class="card-body">
				<div class="media">
					<div class="media-middle media-left">
						<span class="bg-primary-inverse circle sq-48">
							<span class="icon icon-users"></span>
						</span>
					</div>
					<div class="media-middle media-body">
						<h6 class="media-heading">Perawat PK I</h6>
						<h3 class="media-heading">
							<span class="fw-l counter" id="jumlah-perawat-pk-1">0</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-lg-2">
		<div class="card bg-primary">
			<div class="card-body">
				<div class="media">
					<div class="media-middle media-left">
						<span class="bg-primary-inverse circle sq-48">
							<i class="fa fa-fw fa-users"></i>
						</span>
					</div>
					<div class="media-middle media-body">
						<h6 class="media-heading">Perawat PK II</h6>
						<h3 class="media-heading">
							<span class="fw-l counter" id="jumlah-perawat-pk-2">0</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-lg-2">
		<div class="card bg-primary">
			<div class="card-body">
				<div class="media">
					<div class="media-middle media-left">
						<span class="bg-primary-inverse circle sq-48">
							<span class="icon icon-users"></span>
						</span>
					</div>
					<div class="media-middle media-body">
						<h6 class="media-heading">Perawat PK III</h6>
						<h3 class="media-heading">
							<span class="fw-l"></span>
							<span class="fw-l counter" id="jumlah-perawat-pk-3">0</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-lg-2">
		<div class="card bg-primary">
			<div class="card-body">
				<div class="media">
					<div class="media-middle media-left">
						<span class="bg-primary-inverse circle sq-48">
							<span class="icon icon-users"></span>
						</span>
					</div>
					<div class="media-middle media-body">
						<h6 class="media-heading">Perawat PK IV</h6>
						<h3 class="media-heading">
							<span class="fw-l"></span>
							<span class="fw-l counter" id="jumlah-perawat-pk-4">0</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-lg-2">
		<div class="card bg-primary">
			<div class="card-body">
				<div class="media">
					<div class="media-middle media-left">
						<span class="bg-primary-inverse circle sq-48">
							<span class="icon icon-users"></span>
						</span>
					</div>
					<div class="media-middle media-body">
						<h6 class="media-heading">Perawat PK V</h6>
						<h3 class="media-heading">
							<span class="fw-l"></span>
							<span class="fw-l counter" id="jumlah-perawat-pk-5">0</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-lg-2">
		<div class="card bg-primary">
			<div class="card-body">
				<div class="media">
					<div class="media-middle media-left">
						<span class="bg-primary-inverse circle sq-48">
							<span class="icon icon-users"></span>
						</span>
					</div>
					<div class="media-middle media-body">
						<h6 class="media-heading">Total Perawat</h6>
						<h3 class="media-heading">
							<span class="fw-l"></span>
							<span class="fw-l counter" id="total-perawat">0</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row gutter-xs">
	<div class="col-xs-12 col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="pull-left">
					<h5 class="card-title">Grafik Tindakan Perawat</h5>
				</div>
				<div class="pull-right" data-toggle="buttons">
					<label class="btn btn-outline-primary btn-xs btn-pill active">
						<input type="radio" name="logBookChartRange" value="1" autocomplete="off"> Minggu Ini
					</label>
					<label class="btn btn-outline-primary btn-xs btn-pill">
						<input type="radio" name="logBookChartRange" value="2" autocomplete="off"> Bulan Ini
					</label>
				</div>
			</div>
			<div class="card-body">
				<div class="card-chart" id="logBookChartContainer">
					<canvas id="logBookChart" height="110">
				</div>
			</div>
		</div>
	</div>
</div>