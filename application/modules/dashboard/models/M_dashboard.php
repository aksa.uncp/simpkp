<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	public function dashboardLogBookChart($beginDate, $endDate, $idPerawat)
	{
		$query = $this->db->query("CALL px_dashboard_log_book_chart('{$beginDate}','{$endDate}','{$idPerawat}')");
		$result = $query->result();
		mysqli_next_result($this->db->conn_id);

		return $result;
	}

	public function dashboardBadgeInfo()
	{
		$query = $this->db->query("CALL px_dashboard_badge_info()");
		$result = $query->row();
		mysqli_next_result($this->db->conn_id);

		return $result;
	}

}

/* End of file M_dashboard.php */
/* Location: ./application/modules/dashboard/models/M_dashboard.php */