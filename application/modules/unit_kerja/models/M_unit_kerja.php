<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_unit_kerja extends MY_Model {

	public $table = 'unit_kerja';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

}

/* End of file m_unit_kerja.php */
/* Location: ./application/modules/jabatan/models/m_unit_kerja.php */