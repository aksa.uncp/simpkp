<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("unit_kerja") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<form id="form-edit" class="form-horizontal" role="form">

		<input type="hidden" name="id" value="<?= $data->id ?>" />

		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="group">
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Unit Kerja</label>
						<div class="col-xs-9">
							<input name="nama_unit_kerja" value="<?= $data->nama_unit_kerja ?>" type="text" class="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('unit_kerja') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>