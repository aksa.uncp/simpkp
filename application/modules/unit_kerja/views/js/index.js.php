<script type="text/javascript">
	$(function() {
		var selected = [];

		var tableUnitKerja = $('#table-unit-kerja').DataTable({
			language: {
				emptyTable: "<center>Tidak ada data ditemukan</center>"
			},
			ordering: true,
			order: [[1, 'asc']],
			processing: true,
			serverSide: true,
			responsive: true,
			ajax: '<?= site_url("api/unit_kerja/unit_kerja/datatables") ?>',
			columns: [
				{ 
					orderable: false,
					data: null
				},
				{ data: "nama_unit_kerja" },
				{
					orderable: false,
					data: null,
					mRender: function(data, type, row) {
						return "<a href='<?= site_url('unit_kerja/edit/') ?>" + row.id + "' class='edit text-primary'><i class='fa fa-fw fa-pencil'></i></a>";
					}
				},
				{
					orderable: false,
					data: null,
					mRender: function(data, type, row) {
						return "<a href='javascript:void(0)' class='remove text-danger'><i class='fa fa-fw fa-trash'></i></a>";
					}
				}
			],
			rowCallback: function( row, data ) {
				if ( $.inArray(data.id, selected) !== -1 ) {
					$(row).addClass('selected');
				}
			},
			columnDefs: [
				{ targets: 0, className: "text-center gray" },
				{ targets: [0], className: "text-center" },
				{ targets: 0, responsivePriority: 1 },
				{ targets: 1, responsivePriority: 2 },
				{ targets: -2, responsivePriority: 3 },
				{ targets: -1, responsivePriority: 4 }
			],
		});

		tableUnitKerja.on( 'draw', function (eve, obj) {
			var start = obj.oAjaxData.start;
			let no = start + 1;
			tableUnitKerja.column(0).nodes().each( function(cell, i) {
				cell.innerHTML = i + no;
			});
		});

		$('#table-unit-kerja tbody').on('click', 'tr', function () {
			let data = tableUnitKerja.row(this).data();
			let id = data.id;
			let index = $.inArray(id, selected);

			if ( index === -1 ) {
				selected.push( id );
			} else {
				selected.splice( index, 1 );
			}

			$(this).toggleClass('selected');
		} );

		$('#remove-selected').on('click', function() {
			swal({
				title: 'Hapus Data',
				text: 'Apakah anda yakin akan menghapus data yang dipilih?',
				icon: 'warning',
				dangerMode: true,
				buttons: {
					cancel: "Tidak",
					confirm: "Ya, hapus data!"
				}
			})
			.then((isConfirm) => {
				if (isConfirm) {
					$.ajax({
						url: '<?= site_url("api/unit_kerja/unit_kerja") ?>',
						method: 'DELETE',
						data: {
							id: selected
						},
						dataType: 'json',
						beforeSend: function() {
							$('#main-content').waitMe({
								effect: 'rotation',
								text: 'Tunggu...',
								bg: 'rgba(255,255,255,0.7)',
								color: '#000',
								waitTime: -1,
								textPos: 'vertical'
							});
						},
						success: function(xhr) {
							try {
								let data = xhr.response;
								swal('Berhasil', 'Berhasil menghapus data', 'success');
								tableUnitKerja.ajax.reload(null, false);
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						error: function(xhr) {
							try {
								let error = JSON.parse(xhr.responseText);
								swal({
									title: 'Oops..',
									icon: 'error',
									text: error.metadata.message
								});
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						complete: function(xhr) {
							$('#main-content').waitMe('hide');
						}
					});
				}
			});
		});

		$('#table-unit-kerja tbody').on('click', '.remove', function() {
			let data = tableUnitKerja.row( $(this).parents('tr') ).data();

			swal({
				title: 'Hapus Data',
				text: 'Apakah anda yakin akan menghapus data ini?',
				icon: 'warning',
				dangerMode: true,
				buttons: {
					cancel: "Tidak",
					confirm: "Ya, hapus data!"
				}
			})
			.then((isConfirm) => {
				if (isConfirm) {
					$.ajax({
						url: '<?= site_url("api/unit_kerja/unit_kerja") ?>',
						method: 'DELETE',
						data: {
							id: data.id
						},
						dataType: 'json',
						beforeSend: function() {
							$('#main-content').waitMe({
								effect: 'rotation',
								text: 'Tunggu...',
								bg: 'rgba(255,255,255,0.7)',
								color: '#000',
								waitTime: -1,
								textPos: 'vertical'
							});
						},
						success: function(xhr) {
							try {
								let data = xhr.response;
								swal('Berhasil', 'Berhasil menghapus data', 'success');
								tableUnitKerja.ajax.reload(null, false);
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						error: function(xhr) {
							try {
								let error = JSON.parse(xhr.responseText);
								swal({
									title: 'Oops..',
									icon: 'error',
									text: error.metadata.message
								});
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						complete: function(xhr) {
							$('#main-content').waitMe('hide');
						}
					});
				}
			});
		});

		$('#form-search').on('submit', function(e) {
			e.preventDefault();

			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				tableUnitKerja.column(i).search($(this).val());
			});

			tableUnitKerja.draw();

			$('#modal-search').modal('hide');
		});

		$('#form-search').on('reset', function() {
			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				tableUnitKerja.column(i).search('');
			});

			tableUnitKerja.draw();

			$('#modal-search').modal('hide');
		});
	});
</script>