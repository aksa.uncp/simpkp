<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unit_kerja extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('unit_kerja/m_unit_kerja');
	}

	/**
	 * response for jquery datatables
	 */
	public function datatables_get()
	{
		$query = [];

		// columns index
		$column_index = array(null, 'nama_unit_kerja');

		// filter column handler / search
		$columns = $this->get('columns');
		$search  = $this->get('search');
		$orders  = $this->get('order');
		$length  = $this->get('length');
		$start   = $this->get('start');

		if ( ! empty($search['value']) ) {

			$search_value = xss_clean($search['value']);

			$query['where'][$column_index[1].' LIKE'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 0 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];
					
					} else {

						$query['where'][$column_index[$column_key].' LIKE '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = $this->m_unit_kerja->count($query);

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $orders as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// limit
		$query['limit_offset'] = array($length, $start);

		$data = $this->m_unit_kerja->get($query);

		$this->response([
			'draw' => $this->get('draw'),
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_total,
			'data' => $data['data']
		]);
	}

	/**
	 * add new data
	 */
	public function index_post()
	{
		$validation_rules = [
			[
				'field' => 'nama_unit_kerja',
				'label' => 'Nama Unit Kerja',
				'rules' => 'required|unique_field[unit_kerja.nama_unit_kerja]|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'nama_unit_kerja' => $this->post('nama_unit_kerja')
			];

			if ( $this->m_unit_kerja->insert($data) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menambahkan data.'
					]
				], 500);

			}

		}
	}
 
	/** 
	 * update data 
	 */ 
	public function index_put() 
	{
		$this->form_validation->set_data($this->put());
		$id = $this->put('id', TRUE);
 
		$validation_rules = [
			[
				'field' => 'nama_unit_kerja',
				'label' => 'Nama unit_kerja',
				'rules' => 'required|unique_field[unit_kerja.nama_unit_kerja.id.'.$id.']|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'nama_unit_kerja' => $this->put('nama_unit_kerja')
			];

			if ( $this->m_unit_kerja->update($data, $id) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menyimpan perubahan.'
					]
				], 500);

			}

		}
	}

	/**
	 * delete data
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		if ( is_array($id) ) {
			$result = $this->m_unit_kerja->delete_batch($id);
		} else {
			$result = $this->m_unit_kerja->delete($id);
		}

		if ( $result == false ) {
			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => 'Server error. Gagal menghapus data.'
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => 'OK'
				]
			]);
		}
	}

}

/* End of file unit_kerja.php */
/* Location: ./application/modules/unit_kerja/controllers/rest/unit_kerja.php */