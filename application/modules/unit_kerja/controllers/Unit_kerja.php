<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_kerja extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('unit_kerja/m_unit_kerja');
	}

	public function index()
	{
		$this->data['page_title'] = 'Unit Kerja';
		$this->data['page_description'] = 'Daftar Unit Kerja';

		$this->template->set_partial('js', 'unit_kerja/js/index.js.php');
		$this->template->set_partial('modals', 'unit_kerja/modals/index.modal.php');

		$this->template->build('unit_kerja/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah unit_kerja';
		$this->data['page_description'] = 'Menambahkan Data unit_kerja';

		$this->template->set_partial('js', 'unit_kerja/js/add.js.php');

		$this->template->build('unit_kerja/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit unit_kerja';
		$this->data['page_description'] = 'Mengubah Data unit_kerja';

		$this->data['data'] = $this->m_unit_kerja->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'unit_kerja/js/edit.js.php');

		$this->template->build('unit_kerja/edit', $this->data);
	}

}

/* End of file unit_kerja.php */
/* Location: ./application/modules/unit_kerja/controllers/unit_kerja.php */