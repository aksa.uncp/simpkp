<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pangkat extends MY_Model {

	public $table = 'pangkat';
	public $view = 'v_pangkat';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

}

/* End of file M_pangkat.php */
/* Location: ./application/modules/absen/models/M_pangkat.php */