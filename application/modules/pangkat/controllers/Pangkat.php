<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pangkat extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pangkat/m_pangkat');
	}

	public function index()
	{
		$this->data['page_title'] = 'Pangkat';
		$this->data['page_description'] = 'Daftar Pangkat';

		$this->template->set_partial('js', 'pangkat/js/index.js.php');
		$this->template->set_partial('modals', 'pangkat/modals/index.modal.php');

		$this->template->build('pangkat/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah Pangkat';
		$this->data['page_description'] = 'Menambahkan Data Pangkat';

		$this->template->set_partial('js', 'pangkat/js/add.js.php');

		$this->template->build('pangkat/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Pangkat';
		$this->data['page_description'] = 'Mengubah Data Pangkat';

		$this->data['data'] = $this->m_pangkat->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'pangkat/js/edit.js.php');

		$this->template->build('pangkat/edit', $this->data);
	}

}

/* End of file Absen.php */
/* Location: ./application/modules/pangkat/controllers/Pangkat.php */