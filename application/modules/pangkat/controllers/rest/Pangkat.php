<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pangkat extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pangkat/m_pangkat');
	}

	/**
	 * response for jquery datatables
	 */
	public function datatables_get()
	{
		$query = [];

		// columns index
		$column_index = array(null, 'nama_pangkat', 'golongan', 'id_jabatan', 'nama_jabatan');

		// filter column handler / search
		$columns = $this->get('columns');
		$search  = $this->get('search');
		$orders  = $this->get('order');
		$length  = $this->get('length');
		$start   = $this->get('start');

		if ( ! empty($search['value']) ) {

			$search_value = xss_clean($search['value']);

			$query['where'][$column_index[1].' LIKE'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 3 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];
					
					} else {

						$query['where'][$column_index[$column_key].' LIKE '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = $this->m_pangkat->count($query);

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $orders as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// limit
		$query['limit_offset'] = array($length, $start);

		$data = $this->m_pangkat->get($query);

		$this->response([
			'draw' => $this->get('draw'),
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_total,
			'data' => $data['data']
		]);
	}

	/**
	 * add new data
	 */
	public function index_post()
	{
		$validation_rules = [
			[
				'field' => 'nama_pangkat',
				'label' => 'Nama Pangkat',
				'rules' => 'required|unique_field[v_pangkat.nama_pangkat]|trim|xss_clean'
			],
			[
				'field' => 'golongan',
				'label' => 'Golongan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_jabatan',
				'label' => 'Jabatan',
				'rules' => 'required|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'nama_pangkat' => $this->post('nama_pangkat'),
				'golongan'     => $this->post('golongan'),
				'id_jabatan'   => $this->post('id_jabatan')
			];

			if ( $this->m_pangkat->insert($data) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menambahkan data.'
					]
				], 500);

			}

		}
	}
 
	/** 
	 * update data 
	 */ 
	public function index_put() 
	{
		$this->form_validation->set_data($this->put());
		$id = $this->put('id', TRUE);
 
		$validation_rules = [
			[
				'field' => 'nama_pangkat',
				'label' => 'Nama Pangkat',
				'rules' => 'required|unique_field[v_pangkat.nama_pangkat.id.'.$id.']|trim|xss_clean'
			],
			[
				'field' => 'golongan',
				'label' => 'Golongan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_jabatan',
				'label' => 'Jabatan',
				'rules' => 'required|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'nama_pangkat' => $this->put('nama_pangkat'),
				'golongan'     => $this->put('golongan'),
				'id_jabatan'   => $this->put('id_jabatan')
			];

			if ( $this->m_pangkat->update($data, $id) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menyimpan perubahan.'
					]
				], 500);

			}

		}
	}

	/**
	 * delete data
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		if ( is_array($id) ) {
			$result = $this->m_pangkat->delete_batch($id);
		} else {
			$result = $this->m_pangkat->delete($id);
		}

		if ( $result == false ) {
			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => 'Server error. Gagal menghapus data.'
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => 'OK'
				]
			]);
		}
	}

}

/* End of file pangkat.php */
/* Location: ./application/controllers/pangkat.php */