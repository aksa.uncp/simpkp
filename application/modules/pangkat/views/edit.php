<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("pangkat") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<form id="form-edit" class="form-horizontal" role="form">

		<input type="hidden" name="id" value="<?= $data->id ?>" />

		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="group">
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Pangkat</label>
						<div class="col-xs-9">
							<input name="nama_pangkat" value="<?= $data->nama_pangkat ?>" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Golongan</label>
						<div class="col-xs-9">
							<input name="golongan" value="<?= $data->golongan ?>" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Jabatan</label>
						<div class="col-xs-9">
							<?= dynamic_dropdown([
								'name' => 'id_jabatan',
								'table' => 'jabatan',
								'key' => 'id',
								'label' => 'nama_jabatan',
								'selected' => $data->id_jabatan,
								'empty_first' => TRUE,
								'empty_first_label' => '- Pilih -',
								'attr' => 'class="form-control select2-single"'
							]) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('pangkat') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>