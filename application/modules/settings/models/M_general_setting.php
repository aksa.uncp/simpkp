<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_general_setting extends CI_Model {

	public function update($data)
	{
		$this->db->trans_begin();

		foreach ($data as $key => $value) {
			$this->db->where(['parameter' => $key])->set(['nilai' => $value])->update('pengaturan');
		}

		if ( $this->db->trans_status() == FALSE ) {
			$this->db->trans_rollback();

			return FALSE;
		} else {
			$this->db->trans_commit();

			return TRUE;
		}
	}

}

/* End of file M_setting.php */
/* Location: ./application/modules/settings/models/M_setting.php */