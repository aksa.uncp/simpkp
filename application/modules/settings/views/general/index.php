<div class="card">
	<form id="form-settings" class="form-horizontal">
		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="group">
					<div class="text-center m-b">
						<h4 class="m-b-0"><i class="fa fa-fw fa-institution"></i> Profil RS</h4>
						<small>Identitas Rumah Sakit</small>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Rumah Sakit</label>
						<div class="col-xs-9">
							<input name="nama_rs" type="text" class="form-control" value="<?= App::get_option('nama_rs') ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Alamat Rumah Sakit</label>
						<div class="col-xs-9">
							<textarea name="alamat_rs" class="form-control" rows="5"><?= App::get_option('alamat_rs') ?></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
			</div>
		</div>
	</form>
</div>