<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('settings/m_general_setting');
	}

	public function index_put()
	{
		$this->form_validation->set_data($this->put());
 
		$validation_rules = [
			[
				'field' => 'nama_rs',
				'label' => 'Nama Rumah Sakit',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'alamat_rs',
				'label' => 'Alamat Rumah Sakit',
				'rules' => 'trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => "400",
					'message' => strip_tags(validation_errors())
				]
			], 400);
		} else {
			$data['nama_rs']   = $this->put('nama_rs');
			$data['alamat_rs'] = $this->put('alamat_rs');

			if ( $this->m_general_setting->update($data) ) {
				$this->response([
					'metadata' => [
						'code' => "200",
						'message' => 'Berhasil menyimpan perubahan'
					]
				]);
			} else {
				$this->response([
					'metadata' => [
						'code' => "500",
						'message' => 'Gagal menyimpan perubahan'
					]
				], 500);
			}
		}
	}

}

/* End of file General.php */
/* Location: ./application/modules/settings/controllers/rest/General.php */