<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['page_title'] = 'Pengaturan Umum';
		$this->data['page_description'] = 'Pengaturan Umum Aplikasi';

		// js
		$this->template->set_partial('js', 'settings/general/includes/js/index.js.php');

		$this->template->build('settings/general/index', $this->data);
	}

}

/* End of file General.php */
/* Location: ./application/modules/settings/controllers/General.php */