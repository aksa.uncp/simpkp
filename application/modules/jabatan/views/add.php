<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("jabatan") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<form id="form-add" class="form-horizontal" role="form">
		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="group">
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Jabatan</label>
						<div class="col-xs-9">
							<input name="nama_jabatan" type="text" class="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('jabatan') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>