<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('jabatan/m_jabatan');
	}

	public function index()
	{
		$this->data['page_title'] = 'Jabatan';
		$this->data['page_description'] = 'Daftar Jabatan';

		$this->template->set_partial('js', 'jabatan/js/index.js.php');
		$this->template->set_partial('modals', 'jabatan/modals/index.modal.php');

		$this->template->build('jabatan/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah Jabatan';
		$this->data['page_description'] = 'Menambahkan Data Jabatan';

		$this->template->set_partial('js', 'jabatan/js/add.js.php');

		$this->template->build('jabatan/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Jabatan';
		$this->data['page_description'] = 'Mengubah Data Jabatan';

		$this->data['data'] = $this->m_jabatan->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'jabatan/js/edit.js.php');

		$this->template->build('jabatan/edit', $this->data);
	}

}

/* End of file jabatan.php */
/* Location: ./application/modules/jabatan/controllers/jabatan.php */