<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jabatan extends MY_Model {

	public $table = 'jabatan';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

}

/* End of file m_jabatan.php */
/* Location: ./application/modules/jabatan/models/m_jabatan.php */