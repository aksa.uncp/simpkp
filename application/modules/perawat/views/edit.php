<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("perawat") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<form id="form-edit" class="form-horizontal" role="form">

		<input type="hidden" name="id" value="<?= $data->id ?>" />

		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="group">
					<div class="text-center m-b">
						<h4 class="m-b-0"><i class="fa fa-fw fa-user"></i> Identitas</h4>
						<small>Identitas Perawat</small>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">NIP</label>
						<div class="col-xs-9">
							<input name="nip" value="<?= $data->nip ?>" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Lengkap</label>
						<div class="col-xs-9">
							<input name="nama_lengkap" value="<?= $data->nama_lengkap ?>" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Email</label>
						<div class="col-xs-9">
							<input name="email" value="<?= $data->email ?>" type="text" class="form-control" />
						</div>
					</div>
				</div>
				<div class="group">
					<div class="text-center m-b">
						<h4 class="m-b-0"><i class="fa fa-fw fa-suitcase"></i> Jabatan</h4>
						<small>Pangkat & Jabatan Perawat</small>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Unit Kerja</label>
						<div class="col-xs-9">
							<?= dynamic_dropdown([
								'name' => 'id_unit_kerja',
								'table' => 'unit_kerja',
								'key' => 'id',
								'label' => 'nama_unit_kerja',
								'selected' => $data->id_unit_kerja,
								'empty_first' => TRUE,
								'empty_first_label' => '- Pilih -',
								'attr' => 'class="form-control select2-single"'
							]) ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Jabatan</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input name="nama_jabatan" value="<?= $data->nama_jabatan ?>" type="text" class="form-control" readonly="true" />
								<div class="input-group-btn">
									<button class="btn btn-primary" type="button" id="btn-select-jabatan">
										<i class="fa fa-fw fa-list"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Pangkat</label>
						<div class="col-xs-9">
							<input name="id_pangkat" value="<?= $data->id_pangkat ?>" type="hidden" />
							<input name="nama_pangkat" value="<?= $data->nama_pangkat ?>" type="text" class="form-control" readonly="true" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Golongan</label>
						<div class="col-xs-9">
							<input name="golongan" value="<?= $data->golongan ?>" type="text" class="form-control" readonly="true" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Jenjang PK</label>
						<div class="col-xs-9">
							<?= form_dropdown([
								'name' => 'jenjang_pk',
								'options' => [
									'1' => 'PK I',
									'2' => 'PK II',
									'3' => 'PK III',
									'4' => 'PK IV',
									'5' => 'PK V'
								],
								'selected' => $data->jenjang_pk,
								'class' => 'form-control select2-single'
							]); ?>
						</div>
					</div>
				</div>

				<div class="group">
					<div class="text-center m-b">
						<h4 class="m-b-0"><i class="fa fa-fw fa-key"></i> Akses</h4>
						<small>Akses Login Pengguna Sistem</small>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Username</label>
						<div class="col-xs-9">
							<input name="username" value="<?= $data->username ?>" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Password</label>
						<div class="col-xs-9">
							<input name="password" type="password" class="form-control" placeholder="Kosongkan jika anda tidak ingin mengganti password" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Konfirmasi Password</label>
						<div class="col-xs-9">
							<input name="password_confirmation" type="password" class="form-control" placeholder="Kosongkan jika anda tidak ingin mengganti password" />
							<p class="help-block">
								<small>Ketik ulang password anda untuk mengkonfirmasi</small>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('perawat') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>