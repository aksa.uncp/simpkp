<script type="text/javascript">
	$(function() {
		let tableJabatan;

		$('#form-edit').ajaxForm({
			url: '<?= site_url("api/perawat/perawat") ?>',
			method: 'PUT',
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				$('#main-content').waitMe({
					effect: 'rotation',
					text: 'Tunggu...',
					bg: 'rgba(255,255,255,0.7)',
					color: '#000',
					waitTime: -1,
					textPos: 'vertical'
				});
			},
			success: function(xhr) {
				try {
					let data = xhr.response;
					$('#form-edit').find('[name="password"]').val('');
					$('#form-edit').find('[name="password_confirmation"]').val('');
					swal('Berhasil', 'Berhasil menyimpan perubahan', 'success');
				} catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
				}
			},
			error: function(xhr) {
				try {
					let error = JSON.parse(xhr.responseText);
					swal({
						title: 'Oops..',
						icon: 'error',
						text: error.metadata.message
					});
				} catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
				}
			},
			complete: function(xhr) {
				$('#main-content').waitMe('hide');
			}
		});

		$('#btn-select-jabatan').on('click', function(e) {
			$('#modal-select-jabatan').modal('show');
		});

		$('#modal-select-jabatan').on('show.bs.modal', function(e) {
			if ($.fn.DataTable.isDataTable("#table-select-jabatan") === false) {
				tableJabatan = $('#table-select-jabatan').DataTable({
					language: {
						emptyTable: "<center>Tidak ada data ditemukan</center>"
					},
					destroy: true,
					order: [[1,'asc']],
					processing: true,
					serverSide: true,
					responsive: true,
					searching: true,
					pagingType: "simple",
					ajax: '<?= site_url("api/pangkat/pangkat/datatables") ?>',
					columns : [
						{ 
							orderable: false,
							data: null
						},
						{ data: "nama_pangkat" },
						{ data: "golongan" },
						{ data: "id_jabatan", visible: false },
						{ data: "nama_jabatan" },
						{
							data: null,
							orderable: false,
							mRender: function(data, type, row) {
								return '<a href="javascript:void(0)" class="select"><i class="fa fa-fw fa-hand-pointer-o"></i></a>';
							}
						}
					],
					columnDefs: [
						{ targets: 0, className: "text-center gray" },
						{ targets: [3], visible: false },
						{ targets: [2], className: "text-center" },
						{ targets: 0, responsivePriority: 1 },
						{ targets: 1, responsivePriority: 2 },
						{ targets: -1, responsivePriority: 3 }
					],
				});

				tableJabatan.on( 'draw', function () {
					tableJabatan.column(0).nodes().each( function(cell, i) {
						cell.innerHTML = i + 1;
					} );
				} ).draw();
			} else {
				$('#table-select-jabatan').DataTable().ajax.reload(null, true);
			}
		});

		$('#modal-select-jabatan').on('click', '#table-select-jabatan .select', function(e) {
			let data = tableJabatan.row( $(this).parents('tr') ).data();

			$('#modal-select-jabatan').modal('hide');

			$('#form-edit').find('[name="nama_jabatan"]').val(data.nama_jabatan);
			$('#form-edit').find('[name="id_pangkat"]').val(data.id);
			$('#form-edit').find('[name="nama_pangkat"]').val(data.nama_pangkat);
			$('#form-edit').find('[name="golongan"]').val(data.golongan);
		});
	});
</script>