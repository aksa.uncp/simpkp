<div id="modal-select-jabatan" tabindex="-1" role="dialog" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>

				<div class="text-center">
					<span class="icon icon-suitcase icon-5x m-y-lg"></span>
					<h3 class="modal-title">Jabatan</h3>
					<p>Pilih Jabatan Perawat</p>
				</div>
			</div>
			<div class="modal-body">
				<table class="table table-nowrap table-bordered table-hover" id="table-select-jabatan" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th></th>
							<th>Pangkat</th>
							<th>Golongan</th>
							<th>ID Jabatan</th>
							<th>Jabatan</th>
							<th width="10"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>