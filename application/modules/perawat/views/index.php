<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("perawat/add") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-plus-circle"></i> <span class="hidden-xs">Tambah Perawat</span></a> 
			<a href="#modal-search" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-fw fa-search"></i> <span class="hidden-xs">Pencarian</span></a> 
		</div>
	</div>
	<div class="card-body">
		<table class="table table-nowrap table-bordered table-hover" id="table-perawat" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="25"></th>
					<th>NIP</th>
					<th>Nama Lengkap</th>
					<th>Email</th>
					<th>ID Jabatan</th>
					<th>Jabatan</th>
					<th>ID Pangkat</th>
					<th>Pangkat</th>
					<th>Golongan</th>
					<th>PK</th>
					<th width="5"></th>
					<th width="5"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>