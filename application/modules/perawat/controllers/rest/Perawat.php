<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perawat extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('perawat/m_perawat');
	}

	/**
	 * response for jquery datatables
	 */
	public function datatables_get()
	{
		$query = [];

		// columns index
		$column_index = array(null, 'nip', 'nama_lengkap', 'email', 'id_jabatan', 'nama_jabatan', 'id_pangkat', 'nama_pangkat', 'golongan', 'jenjang_pk');

		// filter column handler / search
		$columns = $this->get('columns');
		$search  = $this->get('search');
		$orders  = $this->get('order');
		$length  = $this->get('length');
		$start   = $this->get('start');

		if ( ! empty($search['value']) ) {

			$search_value = xss_clean($search['value']);

			$query['where'][$column_index[2].' LIKE'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 6 || $column_key == 7 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];
					
					} else {

						$query['where'][$column_index[$column_key].' LIKE '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = $this->m_perawat->count($query);

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $orders as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// limit
		$query['limit_offset'] = array($length, $start);

		$data = $this->m_perawat->get($query);

		$this->response([
			'draw' => $this->get('draw'),
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_total,
			'data' => $data['data']
		]);
	}

	/**
	 * add new data
	 */
	public function index_post()
	{
		$validation_rules = [
			[
				'field' => 'nip',
				'label' => 'NIP',
				'rules' => 'required|unique_field[v_perawat.nip]|trim|xss_clean'
			],
			[
				'field' => 'nama_lengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'valid_email|unique_field[v_perawat.email]|trim|xss_clean'
			],
			[
				'field' => 'id_unit_kerja',
				'label' => 'Unit Kerja',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_pangkat',
				'label' => 'Pangkat',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'jenjang_pk',
				'label' => 'Jenjang PK',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|unique_field[v_perawat.username]|trim|xss_clean'
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|matches[password_confirmation]|trim|xss_clean'
			],
			[
				'field' => 'password_confirmation',
				'label' => 'Konfirmasi Password',
				'rules' => 'required|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$username = $this->post('username', TRUE);
			$password = $this->post('password', TRUE);
			$email    = $this->post('email', TRUE);

			$data = [
				'nip'           => $this->post('nip', TRUE),
				'nama_lengkap'  => $this->post('nama_lengkap', TRUE),
				'id_pangkat'    => $this->post('id_pangkat', TRUE),
				'jenjang_pk'    => $this->post('jenjang_pk', TRUE),
				'id_unit_kerja' => $this->post('id_unit_kerja', TRUE)
			];

			if ( $this->ion_auth->register($username, $password, $email, $data, [2]) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menyimpan data.'
					]
				], 500);

			}

		}
	}
 
	/** 
	 * update data 
	 */ 
	public function index_put() 
	{
		$this->form_validation->set_data($this->put());
		$id = $this->put('id', TRUE);
 
		$validation_rules = [
			[
				'field' => 'nip',
				'label' => 'NIP',
				'rules' => 'required|unique_field[v_perawat.nip.id.'.$id.']|trim|xss_clean'
			],
			[
				'field' => 'nama_lengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'valid_email|unique_field[v_perawat.email.id.'.$id.']|trim|xss_clean'
			],
			[
				'field' => 'id_unit_kerja',
				'label' => 'Unit Kerja',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_pangkat',
				'label' => 'Pangkat',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'jenjang_pk',
				'label' => 'Jenjang PK',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|unique_field[v_perawat.username.id.'.$id.']|trim|xss_clean'
			]
		];

		$password = $this->put('password', TRUE);

		if ( !empty($password) ) {

			$validation_rules[] = [
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|matches[password_confirmation]|trim|xss_clean'
			];

			$validation_rules[] = [
				'field' => 'password_confirmation',
				'label' => 'Konfirmasi Password',
				'rules' => 'required|trim|xss_clean'
			];

		}
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'username'      => $this->put('username', TRUE),
				'email'         => $this->put('email', TRUE),
				'nip'           => $this->put('nip', TRUE),
				'nama_lengkap'  => $this->put('nama_lengkap', TRUE),
				'id_pangkat'    => $this->put('id_pangkat', TRUE),
				'jenjang_pk'    => $this->put('jenjang_pk', TRUE),
				'id_unit_kerja' => $this->put('id_unit_kerja', TRUE)
			];

			if ( !empty($password) ) {

				$data['password'] = $this->put('password', TRUE);

			}

			if ( $this->ion_auth->update($id, $data) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menyimpan data.'
					]
				], 500);

			}

		}
	}

	/**
	 * delete data
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->ion_auth->delete_user($id);

		if ( $result == false ) {

			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => 'Server error. Gagal menghapus data.'
				]
			], 500);

		} else {

			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => 'OK'
				]
			]);

		}
	}

}

/* End of file Perawat.php */
/* Location: ./application/controllers/Perawat.php */