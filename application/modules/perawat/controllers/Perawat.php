<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawat extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('perawat/m_perawat');
	}

	public function index()
	{
		$this->data['page_title'] = 'Perawat';
		$this->data['page_description'] = 'Daftar Perawat';

		$this->template->set_partial('js', 'perawat/js/index.js.php');
		$this->template->set_partial('modals', 'perawat/modals/index.modal.php');

		$this->template->build('perawat/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah Perawat';
		$this->data['page_description'] = 'Menambahkan Data Perawat';

		$this->template->set_partial('js', 'perawat/js/add.js.php');
		$this->template->set_partial('modals', 'perawat/modals/add.modal.php');

		$this->template->build('perawat/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Perawat';
		$this->data['page_description'] = 'Mengubah Data Perawat';

		$this->data['data'] = $this->m_perawat->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'perawat/js/edit.js.php');
		$this->template->set_partial('modals', 'perawat/modals/edit.modal.php');

		$this->template->build('perawat/edit', $this->data);
	}

}

/* End of file Perawat.php */
/* Location: ./application/modules/perawat/controllers/Perawat.php */