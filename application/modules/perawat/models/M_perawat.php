<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_perawat extends MY_Model {

	public $table = 'users';
	public $view = 'v_perawat';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

}

/* End of file M_perawat.php */
/* Location: ./application/modules/absen/models/M_perawat.php */