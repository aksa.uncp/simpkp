<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasien extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pasien/m_pasien');
	}

	/**
	 * response for jquery datatables
	 */
	public function datatables_get()
	{
		$query = [];

		// columns index
		$column_index = array(null, 'no_rm', 'nama_pasien', 'sapaan', 'nama_lengkap_pasien', 'jenis_kelamin', 'tgl_lahir');

		// filter column handler / search
		$columns = $this->get('columns');
		$search  = $this->get('search');
		$orders  = $this->get('order');
		$length  = $this->get('length');
		$start   = $this->get('start');

		if ( ! empty($search['value']) ) {

			$search_value = xss_clean($search['value']);

			$query['where'][$column_index[2].' LIKE'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 3 || $column_key == 5 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];
					
					} else {

						$query['where'][$column_index[$column_key].' LIKE '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = $this->m_pasien->count($query);

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $orders as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// limit
		$query['limit_offset'] = array($length, $start);

		$data = $this->m_pasien->get($query);

		$this->response([
			'draw' => $this->get('draw'),
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_total,
			'data' => $data['data']
		]);
	}

	/**
	 * add new data
	 */
	public function index_post()
	{
		$validation_rules = [
			[
				'field' => 'no_rm',
				'label' => 'No. RM',
				'rules' => 'required|unique_field[v_pasien.no_rm]|trim|xss_clean'
			],
			[
				'field' => 'nama_pasien',
				'label' => 'Nama Pasien',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'sapaan',
				'label' => 'Sapaan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'jenis_kelamin',
				'label' => 'Jenis Kelamin',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'tgl_lahir',
				'label' => 'Tgl. Lahir',
				'rules' => 'required|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'no_rm'         => $this->post('no_rm'),
				'nama_pasien'   => $this->post('nama_pasien'),
				'sapaan'        => $this->post('sapaan'),
				'jenis_kelamin' => $this->post('jenis_kelamin'),
				'tgl_lahir'     => $this->post('tgl_lahir')
			];

			if ( $this->m_pasien->insert($data) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menambahkan data.'
					]
				], 500);

			}

		}
	}
 
	/** 
	 * update data 
	 */ 
	public function index_put() 
	{
		$this->form_validation->set_data($this->put());
		$id = $this->put('id', TRUE);
 
		$validation_rules = [
			[
				'field' => 'no_rm',
				'label' => 'No. RM',
				'rules' => 'required|unique_field[v_pasien.no_rm.id.'.$id.']|trim|xss_clean'
			],
			[
				'field' => 'nama_pasien',
				'label' => 'Nama Pasien',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'sapaan',
				'label' => 'Sapaan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'jenis_kelamin',
				'label' => 'Jenis Kelamin',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'tgl_lahir',
				'label' => 'Tgl. Lahir',
				'rules' => 'required|trim|xss_clean'
			]
		];
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'no_rm'         => $this->put('no_rm'),
				'nama_pasien'   => $this->put('nama_pasien'),
				'sapaan'        => $this->put('sapaan'),
				'jenis_kelamin' => $this->put('jenis_kelamin'),
				'tgl_lahir'     => $this->put('tgl_lahir')
			];

			if ( $this->m_pasien->update($data, $id) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menyimpan perubahan.'
					]
				], 500);

			}

		}
	}

	/**
	 * delete data
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		if ( is_array($id) ) {
			$result = $this->m_pasien->delete_batch($id);
		} else {
			$result = $this->m_pasien->delete($id);
		}

		if ( $result == false ) {
			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => 'Server error. Gagal menghapus data.'
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => 'OK'
				]
			]);
		}
	}

}

/* End of file pasien.php */
/* Location: ./application/controllers/pasien.php */