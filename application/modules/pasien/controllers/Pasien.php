<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pasien/m_pasien');
	}

	public function index()
	{
		$this->data['page_title'] = 'Pasien';
		$this->data['page_description'] = 'Daftar Pasien';

		$this->template->set_partial('js', 'pasien/js/index.js.php');
		$this->template->set_partial('modals', 'pasien/modals/index.modal.php');

		$this->template->build('pasien/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah pasien';
		$this->data['page_description'] = 'Menambahkan Data pasien';

		$this->template->set_partial('js', 'pasien/js/add.js.php');

		$this->template->build('pasien/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Pasien';
		$this->data['page_description'] = 'Mengubah Data Pasien';

		$this->data['data'] = $this->m_pasien->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'pasien/js/edit.js.php');

		$this->template->build('pasien/edit', $this->data);
	}

}

/* End of file Absen.php */
/* Location: ./application/modules/pasien/controllers/pasien.php */