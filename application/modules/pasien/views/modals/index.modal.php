<div id="modal-search" tabindex="-1" role="dialog" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-search" class="form-horizontal" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
						<span class="sr-only">Close</span>
					</button>

					<div class="text-center">
						<span class="text-info icon icon-search icon-5x m-y-lg"></span>
						<h3 class="text-info modal-title">Pencarian</h3>
						<p>Pencarian Data Pasien</p>
					</div>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-xs-4">No. RM</label>
						<div class="col-xs-8">
							<input type="text" class="form-control" data-column="1" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-4">Nama Pasien</label>
						<div class="col-xs-8">
							<input type="text" class="form-control" data-column="4" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-4">Jenis Kelamin</label>
						<div class="col-xs-8">
							<?= form_dropdown([
								'name' => 'jenis_kelamin',
								'options' => [
									'' => '- Semua -',
									'L' => 'Laki-Laki',
									'P' => 'Perempuan'
								],
								'selected' => '',
								'class' => 'form-control',
								'data-column' => 5
							]) ?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="text-center">
						<div class="m-t-lg">
							<button class="btn btn-info" type="submit">Cari</button>
							<button class="btn btn-default" type="reset">Bersihkan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>