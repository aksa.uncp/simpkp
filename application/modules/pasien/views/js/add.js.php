<script type="text/javascript">
	$(function() {
		$('#form-add').ajaxForm({
			url: '<?= site_url("api/pasien/pasien") ?>',
			method: 'POST',
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				$('#main-content').waitMe({
					effect: 'rotation',
					text: 'Tunggu...',
					bg: 'rgba(255,255,255,0.7)',
					color: '#000',
					waitTime: -1,
					textPos: 'vertical'
				});
			},
			success: function(xhr) {
				try {
					let data = xhr.response;
					swal('Berhasil', 'Berhasil menambahkan data', 'success');
					$('#form-add').resetForm();
					$('#form-add').find('[name="sapaan"]').trigger('change');
					$('#form-add').find('[name="jenis_kelamin"]').trigger('change');
					$('#form-add').find('[name="tgl_lahir"]').trigger('change');
				} catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
				}
			},
			error: function(xhr) {
				try {
					let error = JSON.parse(xhr.responseText);
					swal({
						title: 'Oops..',
						icon: 'error',
						text: error.metadata.message
					});
				} catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
				}
			},
			complete: function(xhr) {
				$('#main-content').waitMe('hide');
			}
		});
	});
</script>