<script type="text/javascript">
	$(function() {
		var selected = [];

		var tablePasien = $('#table-pasien').DataTable({
			language: {
				emptyTable: "<center>Tidak ada data ditemukan</center>"
			},
			ordering: true,
			order: [[1, 'asc']],
			processing: true,
			serverSide: true,
			responsive: true,
			ajax: '<?= site_url("api/pasien/pasien/datatables") ?>',
			columns: [
				{ 
					orderable: false,
					data: null
				},
				{ data: "no_rm" },
				{ data: "nama_pasien", visible: false },
				{ data: "sapaan", visible: false },
				{ data: "nama_lengkap_pasien" },
				{ data: "jenis_kelamin" },
				{
					data: "tgl_lahir",
					mRender: function(data, type, row) {
						return moment(data).format('DD/MM/YYYY');
					}
				},
				{
					orderable: false,
					data: null,
					mRender: function(data, type, row) {
						return "<a href='<?= site_url('pasien/edit/') ?>" + row.id + "' class='edit text-primary'><i class='fa fa-fw fa-pencil'></i></a>";
					}
				},
				{
					orderable: false,
					data: null,
					mRender: function(data, type, row) {
						return "<a href='javascript:void(0)' class='remove text-danger'><i class='fa fa-fw fa-trash'></i></a>";
					}
				}
			],
			rowCallback: function( row, data ) {
				if ( $.inArray(data.id, selected) !== -1 ) {
					$(row).addClass('selected');
				}
			},
			columnDefs: [
				{ targets: 0, className: "text-center gray" },
				{ targets: [0,1,5,6], className: "text-center" },
				{ targets: 0, responsivePriority: 1 },
				{ targets: 1, responsivePriority: 2 },
				{ targets: 2, responsivePriority: 3 },
				{ targets: -2, responsivePriority: 4 },
				{ targets: -1, responsivePriority: 5 }
			],
		});

		tablePasien.on( 'draw', function (eve, obj) {
			var start = obj.oAjaxData.start;
			let no = start + 1;
			tablePasien.column(0).nodes().each( function(cell, i) {
				cell.innerHTML = i + no;
			});
		});

		$('#table-pasien tbody').on('click', 'tr', function () {
			let data = tablePasien.row(this).data();
			let id = data.id;
			let index = $.inArray(id, selected);

			if ( index === -1 ) {
				selected.push( id );
			} else {
				selected.splice( index, 1 );
			}

			$(this).toggleClass('selected');
		} );

		$('#remove-selected').on('click', function() {
			swal({
				title: 'Hapus Data',
				text: 'Apakah anda yakin akan menghapus data yang dipilih?',
				icon: 'warning',
				dangerMode: true,
				buttons: {
					cancel: "Tidak",
					confirm: "Ya, hapus data!"
				}
			})
			.then((isConfirm) => {
				if (isConfirm) {
					$.ajax({
						url: '<?= site_url("api/pasien/pasien") ?>',
						method: 'DELETE',
						data: {
							id: selected
						},
						dataType: 'json',
						beforeSend: function() {
							$('#main-content').waitMe({
								effect: 'rotation',
								text: 'Tunggu...',
								bg: 'rgba(255,255,255,0.7)',
								color: '#000',
								waitTime: -1,
								textPos: 'vertical'
							});
						},
						success: function(xhr) {
							try {
								let data = xhr.response;
								swal('Berhasil', 'Berhasil menghapus data', 'success');
								tablePasien.ajax.reload(null, false);
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						error: function(xhr) {
							try {
								let error = JSON.parse(xhr.responseText);
								swal({
									title: 'Oops..',
									icon: 'error',
									text: error.metadata.message
								});
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						complete: function(xhr) {
							$('#main-content').waitMe('hide');
						}
					});
				}
			});
		});

		$('#table-pasien tbody').on('click', '.remove', function() {
			let data = tablePasien.row( $(this).parents('tr') ).data();

			swal({
				title: 'Hapus Data',
				text: "Apakah anda yakin akan menghapus data ini?",
				icon: 'warning',
				dangerMode: true,
				buttons: {
					cancel: "Tidak",
					confirm: "Ya, hapus data!"
				}
			})
			.then((isConfirm) => {
				if (isConfirm) {
					$.ajax({
						url: '<?= site_url("api/pasien/pasien") ?>',
						method: 'DELETE',
						data: {
							id: data.id
						},
						dataType: 'json',
						beforeSend: function() {
							$('#main-content').waitMe({
								effect: 'rotation',
								text: 'Tunggu...',
								bg: 'rgba(255,255,255,0.7)',
								color: '#000',
								waitTime: -1,
								textPos: 'vertical'
							});
						},
						success: function(xhr) {
							try {
								let data = xhr.response;
								swal('Berhasil', 'Berhasil menghapus data', 'success');
								tablePasien.ajax.reload(null, false);
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						error: function(xhr) {
							try {
								let error = JSON.parse(xhr.responseText);
								swal({
									title: 'Oops..',
									icon: 'error',
									text: error.metadata.message
								});
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						complete: function(xhr) {
							$('#main-content').waitMe('hide');
						}
					});
				}
			});
		});

		$('#form-search').on('submit', function(e) {
			e.preventDefault();

			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				tablePasien.column(i).search($(this).val());
			});

			tablePasien.draw();

			$('#modal-search').modal('hide');
		});

		$('#form-search').on('reset', function() {
			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				tablePasien.column(i).search('');
			});

			tablePasien.draw();

			$('#modal-search').modal('hide');
		});
	});
</script>