<script type="text/javascript">
	$(function() {
		$('#form-edit').ajaxForm({
			url: '<?= site_url("api/pasien/pasien") ?>',
			method: 'PUT',
			dataType: 'json',
			beforeSubmit: function(arr, $form, options) {
				$('#main-content').waitMe({
					effect: 'rotation',
					text: 'Tunggu...',
					bg: 'rgba(255,255,255,0.7)',
					color: '#000',
					waitTime: -1,
					textPos: 'vertical'
				});
			},
			success: function(xhr) {
				try {
					let data = xhr.response;
					swal('Berhasil', 'Berhasil menyimpan perubahan', 'success');
				} catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
				}
			},
			error: function(xhr) {
				try {
					let error = JSON.parse(xhr.responseText);
					swal({
						title: 'Oops..',
						icon: 'error',
						text: error.metadata.message
					});
				} catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
				}
			},
			complete: function(xhr) {
				$('#main-content').waitMe('hide');
			}
		});
	});
</script>