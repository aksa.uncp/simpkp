<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("pasien") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<form id="form-add" class="form-horizontal" role="form">
		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="group">
					<div class="form-group">
						<label class="control-label col-xs-3">No. RM</label>
						<div class="col-xs-9">
							<input name="no_rm" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Pasien</label>
						<div class="col-xs-9">
							<input name="nama_pasien" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Sapaan</label>
						<div class="col-xs-9">
							<?= form_dropdown([
								'name' => 'sapaan',
								'options' => [
									'Tn' => 'Tn',
									'Ny' => 'Ny',
									'Nn' => 'Nn',
									'An' => 'An',
									'By' => 'By'
								],
								'selected' => 'Tn',
								'class' => 'form-control select2-single'
							]) ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">L/P</label>
						<div class="col-xs-9">
							<?= form_dropdown([
								'name' => 'jenis_kelamin',
								'options' => [
									'L' => 'Laki-Laki',
									'P' => 'Perempuan'
								],
								'selected' => 'L',
								'class' => 'form-control select2-single'
							]) ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Tgl. Lahir</label>
						<div class="col-xs-9">
							<input name="tgl_lahir" type="text" class="form-control datepicker" autocomplete="off" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('pasien') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>