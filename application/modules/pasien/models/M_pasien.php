<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pasien extends MY_Model {

	public $table = 'pasien';
	public $view = 'v_pasien';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

}

/* End of file M_pangkat.php */
/* Location: ./application/modules/absen/models/M_pangkat.php */