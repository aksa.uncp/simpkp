<!DOCTYPE html>
<html>
	<head>
		<title>Penilaian Kinerja Perawat</title>
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/report.css') ?>">
		<style type="text/css">
			body {
				margin-top: 1cm;
				margin-left: 1cm;
				margin-right: 1.5cm;
				margin-bottom: 1.5cm;
			}

			header {
				position: fixed;
				top: 0cm;
				left: 1cm;
				right: 1.5cm;
				height: 1cm;
			}

			footer {
				position: fixed; 
				bottom: 0cm; 
				left: 1cm; 
				right: 1.5cm;
				height: 1cm;
			}

			tr.gray td {
				background: #DEDEDE;
			}
		</style>
	</head>
	<body>
		<footer>
			<?= base_url() ?>
		</footer>
		<div class="text-center">
			<h3 class="uppercase">Penilaian Kinerja Perawat</h3>
		</div>
		<div class="group">
			<table width="100%">
				<tr>
					<td width="55%">
						<table>
							<tr>
								<td>Nama</td>
								<td width="10" class="text-center">:</td>
								<td><?= $perawat->nama_lengkap ?></td>
							</tr>
							<tr>
								<td>NIP</td>
								<td class="text-center">:</td>
								<td><?= $perawat->nip ?></td>
							</tr>
							<tr>
								<td>Pangkat/Gol/Ruang</td>
								<td class="text-center">:</td>
								<td><?= $perawat->nama_pangkat ?> / <?= $perawat->golongan ?></td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td>Jabatan</td>
								<td width="10" class="text-center">:</td>
								<td><?= $perawat->nama_jabatan ?></td>
							</tr>
							<tr>
								<td>Unit Kerja</td>
								<td class="text-center">:</td>
								<td><?= $perawat->nama_unit_kerja ?></td>
							</tr>
							<tr>
								<td>Periode Penilaian</td>
								<td class="text-center">:</td>
								<td><?= $moment->format('F Y') ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
		</div>
		<main>
			<table class="table">
				<thead>
					<tr>
						<th colspan="2">INDIKATOR PENILAIAN</th>
						<th>TARGET</th>
						<th>SATUAN</th>
						<th>CAPAIAN</th>
						<th>BOBOT</th>
						<th>NILAI</th>
					</tr>
					<tr>
						<th colspan="2">1</th>
						<th>2</th>
						<th>3</th>
						<th>4</th>
						<th>5</th>
						<th>(4/2)x5</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$alpha = range('A', 'Z');

					$index = 0;
					$index_penilaian = 0;
					$index_kategori = 0;
					$index_indikator = 1;
					$id_kategori = $penilaian['data'][0]->id_kategori;

					$jumlah_bobot = 0;
					$jumlah_nilai = 0;
					$total_nilai = 0;
					?>
					<?php foreach ( $penilaian['data'] as $row ) : ?>

						<?php if ( $index == 0 ) : ?>
							<tr class="gray">
								<td class="text-center" style="font-weight:bold" width="5"><?= $alpha[$index_kategori] ?></td>
								<td style="font-weight:bold" width="250"><?= strtoupper($row->nama_kategori) ?></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
							</tr>

							<?php $index_kategori++; ?>
						<?php endif; ?>
						
						<?php if ( $id_kategori != $row->id_kategori ) : ?>
							<?php $id_kategori = $row->id_kategori; ?>
							<?php $index_indikator = 1; ?>

							<tr class="gray">
								<td colspan="2" class="text-center" style="font-weight:bold">Jumlah</td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center">
									<span id="jumlah-bobot-kategori-<?= $penilaian['data'][$index - 1]->id_kategori ?>"><?= $jumlah_bobot ?></span>%
								</td>
								<td class="text-center">
									<span class="jumlah-nilai-kategori" id="jumlah-nilai-kategori-<?= $penilaian['data'][$index - 1]->id_kategori ?>"><?= $jumlah_nilai ?></span>%
								</td>
							</tr>
							<tr class="gray">
								<td class="text-center" style="font-weight:bold"><?= $alpha[$index_kategori] ?></td>
								<td style="font-weight:bold"><?= strtoupper($row->nama_kategori) ?></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td class="text-center">
									<?= ($row->parent == 0) ? $index_indikator : '' ?>
								</td>
								<td>
									<?= $row->nama_indikator ?>
								</td>
								<td class="text-center">
									<?= $row->target ?>
								</td>
								<td class="text-center">
									<?= $row->satuan ?>
								</td>
								<td class="text-center" width="50">
									<?php if ( $row->has_child == 0 ) : ?>
										<?= $row->capaian ?>
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?>
										<?= $row->bobot ?>%
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?>
										<span class="nilai-kategori-<?= $row->id_kategori ?>" id="nilai-<?= $row->id ?>"><?= $row->nilai ?></span>%
									<?php endif; ?>
								</td>
							</tr>

							<?php if ( $row->id == 6 ) : ?>
								<?php if ( $row->tindakan['count'] > 0 ) : ?>
									<?php foreach ( $row->tindakan['data'] as $tindakan ) : ?>
										<tr>
											<td></td>
											<td><?= $tindakan->nama_kategori ?></td>
											<td></td>
											<td></td>
											<td class="text-center"><?= $tindakan->jumlah_tindakan ?></td>
											<td></td>
											<td></td>
										</tr>
									<?php endforeach; ?>
								<?php endif; ?>
							<?php endif; ?>

							<?php $jumlah_bobot = 0; ?>
							<?php $jumlah_nilai = 0; ?>
							<?php $index_kategori++; ?>
						<?php else : ?>
							<tr>
								<td class="text-center">
									<?= ($row->parent == 0) ? $index_indikator : '' ?>
								</td>
								<td>
									<?= $row->nama_indikator ?>
								</td>
								<td class="text-center">
									<?= $row->target ?>
								</td>
								<td class="text-center">
									<?= $row->satuan ?>
								</td>
								<td class="text-center" width="50">
									<?php if ( $row->has_child == 0 ) : ?>
										<?= $row->capaian ?>
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?>
										<?= $row->bobot ?>%
									<?php endif; ?>
								</td>
								<td class="text-center">
									<?php if ( $row->has_child == 0 ) : ?>
										<span class="nilai-kategori-<?= $row->id_kategori ?>" id="nilai-<?= $row->id ?>"><?= $row->nilai ?></span>%
									<?php endif; ?>
								</td>
							</tr>

							<?php if ( $row->id == 6 ) : ?>
								<?php if ( $row->tindakan['count'] > 0 ) : ?>
									<?php foreach ( $row->tindakan['data'] as $tindakan ) : ?>
										<tr>
											<td></td>
											<td style="font-style:italic"><?= $tindakan->nama_kategori ?></td>
											<td></td>
											<td></td>
											<td class="text-center"><?= $tindakan->jumlah_tindakan ?></td>
											<td></td>
											<td></td>
										</tr>
									<?php endforeach; ?>
								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>

						<?php $jumlah_bobot += $row->bobot; ?>
						<?php $jumlah_nilai += $row->nilai; ?>
						<?php $total_nilai += $row->nilai; ?>

						<?php if ( $index == ($penilaian['count'] - 1) ) : ?>
							<tr class="gray">
								<td colspan="2" class="text-center" style="font-weight:bold">Jumlah</td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center"></td>
								<td class="text-center">
									<span id="jumlah-bobot-kategori-<?= $row->id_kategori ?>"><?= $jumlah_bobot ?></span>%
								</td>
								<td class="text-center">
									<span class="jumlah-nilai-kategori" id="jumlah-nilai-kategori-<?= $row->id_kategori ?>"><?= $jumlah_nilai ?></span>%
								</td>
							</tr>
							<tr>
								<td colspan="6" class="text-center" style="font-weight:bold">TOTAL NILAI KRITERIA INDIVIDU</td>
								<td class="text-center" style="font-weight:bold"><span id="total-nilai"><?= $total_nilai ?></span>%</td>
							</tr>
						<?php endif; ?>

						<?php $index_indikator += ($row->parent == 0) ? 1 : 0; ?>
						<?php $index++; ?>
						<?php $index_penilaian += ($row->has_child == 0) ? 1 : 0; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
		</main>
	</body>
</html>