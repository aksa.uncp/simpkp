<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;
use Moment\Moment;
use Dompdf\Dompdf;
use Dompdf\Options;

class Penilaian_kinerja extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('penilaian_kinerja/m_nilai');
		$this->load->model('perawat/m_perawat');
	}

	public function download($type)
	{
		switch ($type) {
			case 'pdf':
				$this->__downloadPDF();
				break;
			
			default:
				show_404();
				break;
		}
	}

	private function __downloadPDF()
	{
		$this->data['perawat'] = $this->m_perawat->row([
			'where' => [
				'id' => $this->input->get('id_perawat', TRUE)
			]
		]);

		if ($this->data['perawat'] === NULL) {

			show_error('Perawat tidak ditemukan');

		} else {

			$this->data['periode'] = $this->input->get('periode', TRUE);
			$this->data['moment'] = new Moment($this->input->get('periode', TRUE));
			$this->data['penilaian'] = $this->m_nilai->getPenilaianPerPerawat($this->input->get('id_perawat', TRUE), $this->input->get('periode', TRUE));

			$html = $this->load->view('report/penilaian_kinerja/penilaian_kinerja', $this->data, TRUE);

			$options = new Options();
			$options->set('defaultFont', 'Helvetica');

			$dompdf = new Dompdf($options);
			$dompdf->loadHtml($html);
			$dompdf->setPaper('Legal', 'portrait');

			$dompdf->render();
			$dompdf->stream('Penilaian Kinerja Perawat ' . $this->data['moment']->format('F Y') . ' ' . $this->data['perawat']->nama_lengkap . '.pdf');

		}
	}

}

/* End of file Rekap_presensi_bulanan.php */
/* Location: ./application/modules/report/controllers/Rekap_presensi_bulanan.php */