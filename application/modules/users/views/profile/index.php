<div class="card">
	<form id="form-profile" class="form-horizontal" role="form">
		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<div class="group">	
					<div class="text-center m-b">
						<h4 class="m-b-0"><i class="fa fa-fw fa-user"></i> Identitas</h4>
						<small>Indetitas Pengguna Sistem</small>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Lengkap</label>
						<div class="col-xs-9">
							<input name="nama_lengkap" value="<?= $user->nama_lengkap ?>" type="text" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Email</label>
						<div class="col-xs-9">
							<input name="email" value="<?= $user->email ?>" type="text" class="form-control" />
						</div>
					</div>
				</div>
				<div class="group">
					<div class="text-center m-b">
						<h4 class="m-b-0"><i class="fa fa-fw fa-key"></i> Akses</h4>
						<small>Akses Login Pengguna Sistem</small>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Username</label>
						<div class="col-xs-9">
							<input name="username" value="<?= $user->username ?>" type="text" readonly="true" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Password</label>
						<div class="col-xs-9">
							<input name="password" type="password" class="form-control" />
							<p class="help-block">
								<small>Kosongkan kolom jika tidak ingin mengubah password</small>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Konfirmasi Password</label>
						<div class="col-xs-9">
							<input name="password_confirmation" type="password" class="form-control" />
							<p class="help-block">
								<small>Ketik ulang password anda untuk mengkonfirmasi</small>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<button class="btn btn-default" type="button"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
			</div>
		</div>
	</form>
</div>