<div class="card">
	<div class="card-header">
		<div class="card-actions">
			<button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
			<button type="button" class="card-action card-reload" title="Reload"></button>
			<button type="button" class="card-action card-remove" title="Remove"></button>
		</div>
		<div>
			<a href="<?= site_url("users") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<div class="card-body">
		<div class="col-md-8 col-md-offset-2">
			<form id="form-add" class="form-horizontal" role="form">
				<div class="text-center m-b">
					<h3 class="m-b-0">Biodata</h3>
					<small>Indetitas pengguna sistem</small>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Nama Lengkap</label>
					<div class="col-xs-9">
						<input name="name" type="text" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Email</label>
					<div class="col-xs-9">
						<input name="email" type="text" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Level</label>
					<div class="col-xs-9">
						<?= dynamic_dropdown(array(
							'name' => 'group_id',
							'table' => 'groups',
							'key' => 'id',
							'label' => 'description',
							'order_by' => 'description ASC',
							'selected' => '',
							'empty_first' => TRUE,
							'empty_first_label' => '- Pilih -',
							'attr' => 'class="form-control"'
						)) ?>
					</div>
				</div>
				<div class="text-center m-b">
					<h3 class="m-b-0">Akses Login</h3>
					<small>Akses login pengguna sistem</small>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Username</label>
					<div class="col-xs-9">
						<input name="username" type="text" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Password</label>
					<div class="col-xs-9">
						<input name="password" type="password" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Konfirmasi Password</label>
					<div class="col-xs-9">
						<input name="password_confirmation" type="password" class="form-control" />
						<p class="help-block">
							<small>Ketik ulang password anda untuk mengkonfirmasi</small>
						</p>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Kirim Email</label>
					<div class="col-xs-9">
						<div class="custom-controls-stacked m-t">
							<label class="custom-control custom-control-primary custom-checkbox">
								<input class="custom-control-input" type="checkbox" value="true" name="send_email" />
								<span class="custom-control-indicator"></span>
								<span class="custom-control-label"><i>Kirim informasi pendaftaran ke email pengguna</i></span>
							</label>
						</div>
					</div>
				</div>
				<div class="text-center">
					<div class="m-t-lg">
						<button class="btn btn-primary" type="submit">Simpan</button>
						<button class="btn btn-default" type="button">Batal</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>