<script type="text/javascript">
	function removeUser(obj) {
		var data = $('#table-users').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Pilih data yang ingin dihapus', 'warning');
		} else {
			$.messager.confirm('Hapus Data', 'Anda yakin ingin menghapus data pengguna <b>'+ data.name +'</b>?', function(r) {
				if ( r ) {
					$('#table-users').datagrid('loading');

					$.ajax({
						url: '<?= site_url('api/users/manage_user') ?>',
						method: 'delete',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									$('#table-users').datagrid('reload');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							$('#table-users').datagrid('loaded');
						}
					})
				}
			});
		}
	}

	function doLoginAs(obj) {
		let data = $('#table-users').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Anda belum memilih pengguna', 'warning');
		} else {
			$.messager.confirm('Login Sebagai', 'Anda yakin akan keluar dan login sebagai <b>'+data.name+'</b>?', function(r) {
				if ( r ) {
					$.ajax({
						url: '<?= site_url('api/users/manage_user/loginas') ?>',
						method: 'get',
						data: {
							identify: data.username
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									window.location.replace('<?= site_url('web') ?>');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						}
					});
				}
			});
		}
	}

	function doResetPassword(obj) {
		let data = $('#table-users').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Anda belum memilih pengguna', 'warning');
		} else {
			$.messager.confirm('Reset Password', 'Anda yakin ingin membuat password baru untuk <b>'+data.name+'</b>?', function(r) {
				if ( r ) {
					$.ajax({
						url: '<?= site_url('api/users/manage_user/resetpassword') ?>',
						method: 'PUT',
						data: {
							id: data.id
						},
						dataType: 'json',
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									let new_password = xhr.response;
									$.messager.alert('Reset Password', '<div class="text-center">Password baru anda adalah <h4 class="text-danger">'+ new_password +'</h4></div>');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						}
					});
				}
			});
		}
	}

	function doClearLoginAttempts(obj) {
		let data = $('#table-users').datagrid('getSelected');

		if ( data == null ) {
			$.messager.alert('Warning', 'Anda belum memilih pengguna', 'warning');
		} else {
			$.messager.confirm('Clear Login Attempts', 'Anda yakin ingin mereset batas login untuk pengguna <b>'+data.name+'</b>?', function(r) {
				if ( r ) {
					$.ajax({
						url: '<?= site_url('api/users/manage_user/clearloginattempts') ?>',
						method: 'PUT',
						data: {
							id: data.id,
							username: data.username
						},
						dataType: 'json',
						beforeSend: function() {
							jQuery('body').waitMe({
								effect: 'rotation',
								bg: 'rgba(255,255,255,0.5)'
							});
						},
						success: function(xhr) {
							if ( typeof xhr === "object" ) {
								if ( xhr.metadata.code != 200 ) {
									$.messager.alert('Error', xhr.metadata.message, 'error');
								} else {
									let new_password = xhr.response;
									$.messager.alert('Clear Login Attempts', 'Batas login pengguna telah direset', 'info');
								}
							} else {
								$.messager.alert('Error', 'Server error', 'error');
								console.log(xhr);
							}
						},
						error: function(xhr) {
							let error = JSON.parse(xhr.responseText);
							if ( typeof error == "object" ) {
								var message = error.metadata.message;
							} else {
								var message = "Tidak dapat menghubungi server";
							}
							$.messager.alert('Error', message, 'error');
							console.log(error);
						},
						complete: function() {
							jQuery('body').waitMe('hide');
						}
					});
				}
			});
		}
	}

	$(function() {
		var selected = [];

		var table = $('#table-users').DataTable({
			language: {
				emptyTable: "<center>Tidak ada data ditemukan</center>"
			},
			ordering : true,
			order : [],
			processing : true,
			serverSide : true,
			responsive: true,
			ajax : '<?= site_url("api/users/users/datatables") ?>',
			columns : [
				{ 
					orderable: false,
					data: null
				},
				{ data: "nama_lengkap" },
				{ data: "email" },
				{
					data: "group_id",
					mRender: function(data, type, row) {
						return row.group_description;
					}
				},
				{
					data: "created_on",
					mRender: function(data, type, row) {
						return moment(data).format("<b>DD/MM/YYYY</b> hh:mm:ss");
					}
				},
				{
					data: "last_login",
					mRender: function(data, type, row) {
						return moment(data).format("<b>DD/MM/YYYY</b> hh:mm:ss");
					}
				},
				{
					data: "active",
					mRender: function(data, type, row) {
						return (data) ? '<i class="fa fa-fw fa-check-circle text-success"></i>' : '<i class="fa fa-fw fa-times-circle text-danger"></i>';
					}
				},
				{
					orderable: false,
					data: null,
					mRender: function(data, type, row) {
						return "<a href='<?= site_url('users/edit/"+row.id+"') ?>'><i class='fa fa-fw fa-pencil'></i></a>";
					}
				},
				{
					orderable: false,
					data: null,
					mRender: function(data, type, row) {
						return "<a href='javascript:void(0)' class='remove text-danger'><i class='fa fa-fw fa-trash'></i></a>";
					}
				}
			],
			fixedColumns: {
				leftColumns: 1
			},
			rowCallback: function( row, data ) {
				if ( $.inArray(data.id, selected) !== -1 ) {
					$(row).addClass('selected');
				}


			},
			columnDefs: [
				{ targets: 0, className: "text-center gray" },
				{ targets: [3,4,5,6], className: "text-center" },
				{ targets: 0, responsivePriority: 1 },
				{ targets: 1, responsivePriority: 2 },
				{ targets: -2, responsivePriority: 3 },
				{ targets: -1, responsivePriority: 4 },
			],
		});

		table.on( 'draw', function () {
			table.column(0).nodes().each( function(cell, i) {
				cell.innerHTML = i + 1;
			} );
		} ).draw();


		$('#table-users tbody').on('click', 'tr', function () {
			let data = table.row(this).data();
			let id = data.id;
			let index = $.inArray(id, selected);

			if ( index === -1 ) {
				selected.push( id );
			} else {
				selected.splice( index, 1 );
			}

			$(this).toggleClass('selected');
		} );


		$('#table-users tbody').on('click', '.remove', function() {
			let data = table.row( $(this).parents('tr') ).data();

			swal({
				title: 'Hapus Data',
				text: "Apakah anda yakin akan menghapus data ini?",
				icon: 'warning',
				dangerMode: true,
				buttons: {
					cancel: "Tidak",
					confirm: "Ya, hapus data!"
				}
			})
			.then((isConfirm) => {
				if (isConfirm) {
					$.ajax({
						url: '<?= site_url("api/users/users") ?>',
						method: 'DELETE',
						dataType: 'json',
						data: {
							id: data.id
						},
						beforeSend: function() {
							$('#main-content').waitMe({
								effect: 'rotation',
								text: 'Tunggu...',
								bg: 'rgba(255,255,255,0.7)',
								color: '#000',
								waitTime: -1,
								textPos: 'vertical'
							});
						},
						success: function(xhr) {
							try {
								let data = xhr.response;
								swal('Berhasil', 'Berhasil menghapus data', 'success');
								table.ajax.reload(null, false);
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						error: function(xhr) {
							try {
								let error = JSON.parse(xhr.responseText);
								swal({
									title: 'Oops..',
									icon: 'error',
									text: error.metadata.message
								});
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						complete: function(xhr) {
							$('#main-content').waitMe('hide');
						}
					});
				}
			});
		});


		$('#form-search').on('submit', function(e) {
			e.preventDefault();

			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				table.column(i).search($(this).val());
			});

			table.draw();

			$('#modal-search').modal('hide');
		});


		$('#form-search').on('reset', function() {
			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				table.column(i).search('');
			});

			table.draw();

			$('#modal-search').modal('hide');
		});
	});
</script>