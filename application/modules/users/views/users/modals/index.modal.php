<div id="modal-search" tabindex="-1" role="dialog" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-search" class="form-horizontal" role="form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
						<span class="sr-only">Close</span>
					</button>

					<div class="text-center">
						<span class="text-info icon icon-search icon-5x m-y-lg"></span>
						<h3 class="modal-title text-success">Pencarian</h3>
						<p>Pencarian Data Pengguna</p>
					</div>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Lengkap</label>
						<div class="col-xs-9">
							<input name="name" type="text" class="form-control" data-column="1" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Email</label>
						<div class="col-xs-9">
							<input name="email" type="text" class="form-control" data-column="2" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Level</label>
						<div class="col-xs-9">
							<?= dynamic_dropdown(array(
								'name' => 'group_id',
								'table' => 'groups',
								'key' => 'id',
								'label' => 'description',
								'selected' => '',
								'empty_first' => TRUE,
								'empty_first_label' => '- Semua -',
								'attr' => 'class="form-control" data-column="3"'
							)) ?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="text-center">
						<div class="m-t-lg">
							<button class="btn btn-info" type="submit">Cari</button>
							<button class="btn btn-default" data-dismiss="modal" type="button">Tutup</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>