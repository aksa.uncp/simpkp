<div class="card">
	<div class="card-header">
		<div class="card-actions">
			<button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
			<button type="button" class="card-action card-reload" title="Reload"></button>
			<button type="button" class="card-action card-remove" title="Remove"></button>
		</div>
		<div>
			<a href="<?= site_url("users") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<div class="card-body">
		<div class="col-md-8 col-md-offset-2">
			<form id="form-edit" class="form-horizontal" role="form">

				<input type="hidden" name="id" value="<?= $user->id ?>" />

				<div class="text-center m-b">
					<h3 class="m-b-0">Biodata</h3>
					<small>Indetitas pengguna sistem</small>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Nama Lengkap</label>
					<div class="col-xs-9">
						<input name="name" value="<?= $user->name ?>" type="text" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Email</label>
					<div class="col-xs-9">
						<input name="email" value="<?= $user->email ?>" type="text" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">Level</label>
					<div class="col-xs-9">
						<?= dynamic_dropdown(array(
							'name' => 'group_id',
							'table' => 'groups',
							'key' => 'id',
							'label' => 'description',
							'selected' => $user->group_id,
							'empty_first' => TRUE,
							'empty_first_label' => '- Pilih -',
							'attr' => 'class="form-control"'
						)) ?>
					</div>
				</div>
				<div class="text-center">
					<div class="m-t-lg">
						<button class="btn btn-primary" type="submit">Simpan</button>
						<button class="btn btn-default" type="button">Batal</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>