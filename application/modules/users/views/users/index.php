<div class="card">
	<div class="card-header">
		<div class="card-actions hidden-xs">
			<button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
			<button type="button" class="card-action card-reload" title="Reload"></button>
			<button type="button" class="card-action card-remove" title="Remove"></button>
		</div>
		<div>
			<a href="<?= site_url("users/add") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-plus-circle"></i> <span class="hidden-xs">Tambah Pengguna</span></a> 
			<a href="#modal-search" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-fw fa-search"></i> <span class="hidden-xs">Pencarian</span></a> 
		</div>
	</div>
	<div class="card-body">
		<table class="table table-nowrap table-bordered table-hover" id="table-users" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="25"></th>
					<th>Nama Lengkap</th>
					<th>Email</th>
					<th>Level</th>
					<th>Tgl. Daftar</th>
					<th>Login Terakhir</th>
					<th width="10">Aktif</th>
					<th width="5"></th>
					<th width="5"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>