<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_user');
	}

	public function index()
	{
		$this->data['page_title'] = 'Pengguna';
		$this->data['page_description'] = 'Pengaturan Akses Pengguna';

		$this->template->set_partial('js', 'users/users/js/index.js.php');
		$this->template->set_partial('modals', 'users/users/modals/index.modal.php');

		$this->template->build('users/users/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Tambah Pengguna';
		$this->data['page_description'] = 'Menambahkan Akses Pengguna';

		$this->template->set_partial('js', 'users/users/js/add.js.php');

		$this->template->build('users/users/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Edit Pengguna';
		$this->data['page_description'] = 'Mengubah Data Akses Pengguna';

		$this->template->set_partial('js', 'users/users/js/edit.js.php');

		if ( $this->m_user->count([ 'where' => [ 'id' => $id ] ]) == 0 ) {

			show_404();
			
		}

		// get user's data
		$this->data['user'] = $this->m_user->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->build('users/users/edit', $this->data);
	}

}

/* End of file Users.php */
/* Location: ./application/modules/users/controllers/Users.php */