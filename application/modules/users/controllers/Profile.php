<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['page_title'] = 'Profil';
		$this->data['page_description'] = 'Profil Pengguna';

		$this->template->set_partial('js', 'users/profile/js/index.js.php');

		// user's data
		$this->data['user'] = $this->ion_auth->user()->row();

		$this->template->build('profile/index', $this->data);
	}

}

/* End of file Profile.php */
/* Location: ./application/modules/users/controllers/Profile.php */