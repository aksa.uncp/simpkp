<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->config->load('ion_auth');
	}

	/**
	 * update user information
	 */
	public function index_put()
	{
		$this->form_validation->set_data($this->put());

		$user = $this->ion_auth->user()->row();

		$rules = [
			[
				'field' => 'nama_lengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|trim|valid_email|xss_clean|conflict_user_email['.$user->id.']'
			]
		];

		$password = $this->put('password', TRUE);

		if ( ! empty($password) ) {

			$rules[] = [
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|trim|min_length['. $this->config->item('min_password_length', 'ion_auth') .']|max_length['. $this->config->item('max_password_length', 'ion_auth') .']|xss_clean'
			];
			$rules[] = [
				'field' => 'password_confirmation',
				'label' => 'Konfirmasi Password',
				'rules' => 'required|trim|matches[password]|xss_clean'
			];

		}

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			// user data
			$nama_lengkap = $this->put('nama_lengkap', TRUE);
			$email = $this->put('email', TRUE);

			$data = [
				'nama_lengkap' => $nama_lengkap,
				'email' => $email,
			];

			// update password
			if ( ! empty($password) ) {

				$data['password'] = $this->put('password', TRUE);

			}

			if ( $this->ion_auth->update($user->id, $data) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => $this->ion_auth->messages()
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => $this->ion_auth->errors()
					]
				], 500);

			}

		}
	}

}