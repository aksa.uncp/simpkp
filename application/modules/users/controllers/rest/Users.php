<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->config->load('ion_auth');
		$this->load->model('m_user');
		$this->load->model('ion_auth_model');
	}

	/**
	 * response for jquery datatables
	 */
	public function datatables_get()
	{
		$query = [];

		// columns index
		$column_index = array(null, 'nama_lengkap', 'email', 'group_id', 'created_on', 'last_login', 'active');

		// filter column handler / search
		$columns = $this->get('columns');
		$search  = $this->get('search');
		$orders  = $this->get('order');
		$length  = $this->get('length');
		$start   = $this->get('start');

		if ( ! empty($search['value']) ) {

			$search_value = xss_clean($search['value']);

			// global search
			$query['where'][$column_index[1].' LIKE'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 3 || $column_key == 4 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];

					} else {

						$query['where'][$column_index[$column_key].' LIKE '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = $this->m_user->count($query);

		// orders
		if ( isset($_GET['order']) ) {

			foreach ( $orders as $order ) {

				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;

			}

		}

		// limit
		$query['limit_offset'] = array($length, $start);

		$data = $this->m_user->get($query);

		$this->response([
			'draw' => $this->get('draw'),
			'recordsTotal' => $records_total,
			'recordsFiltered' => $data['count'],
			'data' => $data['data']
		]);
	}

	/**
	 * get user data
	 */
	public function index_get()
	{
		$uid = $this->get('id', TRUE);

		if ( $uid != NULL ) {

			$result = $this->m_user->row([
				'where' => [
					'id' => $uid
				]
			]);

			if ($result == FALSE) {

				$this->response([
					'metadata' => [
						'code' => 400,
						'message' => "User tidak ditemukan"
					]
				], 400);

			} else {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => "OK"
					],
					'response' => $result
				]);

			}

		} else {

			$query = [];

			$count = $this->m_user->count($query);

			// set limit and offset
			$offset = isset($_GET['page']) ? $this->get('page', TRUE) : 1;
			$limit  = isset($_GET['rows']) ? $this->get('rows', TRUE) : 10;
			$offset = ($offset - 1) * $limit;
			$query['limit_offset'] = [$limit, $offset];

			// sorting
			$sort = $this->get('sort', TRUE);
			$order = $this->get('order', TRUE);
			if ( isset($_GET['sort']) && isset($_GET['order']) ) $query['order_by'] = "{$sort} {$order}";

			$result = $this->m_user->get($query);

			if ($result == FALSE) {

				$this->response([
					'metadata' => [
						'code' => 400,
						'message' => "User tidak ditemukan"
					]
				], 400);

			} else {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => "OK"
					],
					'response' => [
						'total' => $count,
						'rows' => $result['data']
					]
				]);

			}

		}
	}

	/**
	 * register new user
	 */
	public function index_post()
	{
		$validation_rules = [
			[
				'field' => 'nama_lengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|trim|valid_email|unique_field[users.email]|xss_clean'
			],
			[
				'field' => 'group_id',
				'label' => 'Level',
				'rules' => 'required|trim|integer|xss_clean'
			],
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|trim|unique_field[users.username]|xss_clean'
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|trim|min_length['. $this->config->item('min_password_length', 'ion_auth') .']|max_length['. $this->config->item('max_password_length', 'ion_auth') .']|xss_clean'
			],
			[
				'field' => 'password_confirmation',
				'label' => 'Konfirmasi Password',
				'rules' => 'required|trim|matches[password]|xss_clean'
			]
		];

		$group_id = $this->input->post('group_id', TRUE);

		$this->form_validation->set_rules($validation_rules);

		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$email    = $this->input->post('email', TRUE);
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);
			
			// groups
			$groups = [$group_id];

			// additional data
			$nama_lengkap = $this->input->post('nama_lengkap', TRUE);
			$active = $this->input->post('active', TRUE);

			$additional_data = [
				'active' => $active,
				'nama_lengkap' => $nama_lengkap
			];

			if ( $this->ion_auth->register($username, $password, $email, $additional_data, $groups) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => $this->ion_auth->messages()
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => $this->ion_auth->errors()
					]
				], 500);

			}

		}
	}

	/**
	 * update user information
	 */
	public function index_put()
	{
		$this->form_validation->set_data($this->put());

		$id = $this->put('id', TRUE);

		$rules = [
			[
				'field' => 'nama_lengkap',
				'label' => 'Nama Lengkap',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|trim|valid_email|xss_clean|conflict_user_email['.$id.']'
			],
			[
				'field' => 'group_id',
				'label' => 'Grup',
				'ruels' => 'required|trim|xss_clean'
			]
		];

		$group_id = $this->put('group_id', TRUE);

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			// groups
			$groups = [$group_id];

			// user data
			$id    = $this->put('id', TRUE);
			$nama_lengkap  = $this->put('nama_lengkap', TRUE);
			$email = $this->put('email', TRUE);

			$data = [
				'nama_lengkap'  => $nama_lengkap,
				'email' => $email,
			];

			if ( $this->ion_auth->update($id, $data) ) {

				// remove from groups
				$this->ion_auth->remove_from_group(NULL, $id);

				// add to groups
				$this->ion_auth->add_to_group($groups, $id);

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => $this->ion_auth->messages()
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => $this->ion_auth->errors()
					]
				], 500);

			}

		}
	}

	/**
	 * remove user
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		$result = $this->m_user->where('id', $id)->delete();

		if ( $result == FALSE ) {
			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => "Server error, gagal menghapus data."
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => "OK"
				]
			]);
		}
	}

	/**
	 * login as specific user
	 * without password
	 */
	public function loginAs_get()
	{
		$identify = $this->get('identify');

		if ( $this->ion_auth_model->impersonate($identify) ) {

			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => "OK"
				]
			]);

		} else {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => "User tidak ditemukan"
				]
			], 400);

		}
	}

	public function resetPassword_put()
	{
		$id = $this->put('id');

		if ( $this->ion_auth->user($id) !== FALSE ) {

			$new_password = $this->m_user->generateNewPassword();

			$data = ['password' => $new_password];

			if ( $this->ion_auth->update($id, $data) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => "OK"
					],
					'response' => $new_password
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 400,
						'message' => strip_tags($this->ion_auth->errors())
					]
				], 400);

			}

		} else {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags($this->ion_auth->errors())
				]
			], 400);

		}
	}

	public function clearLoginAttempts_put()
	{
		$id       = $this->put('id');
		$username = $this->put('username');

		if ( $this->ion_auth->user($id) !== FALSE ) {

			if ( $this->ion_auth->clear_login_attempts($username) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => "OK"
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 400,
						'message' => strip_tags($this->ion_auth->errors())
					]
				], 400);
			}

		} else {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags($this->ion_auth->errors())
				]
			], 400);

		}
	}

	/**
	 * authenticate user
	 */
	public function auth_post()
	{
		$rules = [
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|xss_clean'
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|xss_clean'
			]
		];

		$this->form_validation->set_rules($rules);

		if ( $this->form_validation->run($rules) == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			]);

		} else {

			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);
			$remember = $this->input->post('remember', TRUE);

			if ( $this->ion_auth->login($username, $password, $remember) == FALSE ) {

				$this->response([
					'metadata' => [
						'code' => 401,
						'message' => strip_tags($this->ion_auth->errors())
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => strip_tags($this->ion_auth->messages())
					]
				]);

			}

		}
	}

}

/* End of file Users.php */
/* Location: ./application/modules/users/controllers/rest/Users.php */