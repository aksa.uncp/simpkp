<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_logbook extends MY_Model {

	public $table = 'logbook';
	public $view = 'v_logbook';
	public $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		
	}

}

/* End of file M_logbook.php */
/* Location: ./application/modules/logbook/models/M_logbook.php */