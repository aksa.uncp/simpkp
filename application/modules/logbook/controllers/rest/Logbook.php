<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logbook extends SP_Rest_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('logbook/m_logbook');
	}

	/**
	 * response for jquery datatables
	 */
	public function datatables_get()
	{
		$query = [];

		if ( $this->ion_auth->in_group('perawat') ) {

			$query['where']['id_perawat'] = $this->userdata->id;

		}

		// columns index
		$column_index = array(null, 'waktu', 'id_tindakan', 'nama_tindakan', 'id_kategori', 'nama_kategori', 'jenjang_pk', 'no_rm', 'nama_pasien', 'nip', 'nama_perawat');

		// filter column handler / search
		$columns = $this->get('columns');
		$search  = $this->get('search');
		$orders  = $this->get('order');
		$length  = $this->get('length');
		$start   = $this->get('start');

		if ( ! empty($search['value']) ) {

			$search_value = xss_clean($search['value']);

			$query['where'][$column_index[3].' LIKE'] = "%$search_value%";

		} else {

			foreach ( $columns as $column_key => $column_val ) {

				if ( ($column_val['searchable']) && (! empty($column_val['search']['value'])) ) {

					if ( $column_key == 2 || $column_key == 4 || $column_key == 6 ) {

						$query['where'][$column_index[$column_key]] = $column_val['search']['value'];
					
					} else {

						$query['where'][$column_index[$column_key].' LIKE '] = '%'.$column_val['search']['value'].'%';

					}

				}

			}

		}

		$records_total = $this->m_logbook->count($query);

		// orders
		if ( isset($_GET['order']) ) {
			foreach ( $orders as $order ) {
				$order_column = $order['column'];
				$order_dir    = $order['dir'];

				$query['order_by'][$column_index[$order_column]] = $order_dir;
			}
		}

		// limit
		$query['limit_offset'] = array($length, $start);

		$data = $this->m_logbook->get($query);

		$this->response([
			'draw' => $this->get('draw'),
			'recordsTotal' => $records_total,
			'recordsFiltered' => $records_total,
			'data' => $data['data']
		]);
	}

	/**
	 * add new data
	 */
	public function index_post()
	{
		$validation_rules = [
			[
				'field' => 'id_pasien',
				'label' => 'ID Pasien',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'no_rm',
				'label' => 'No. RM',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'nama_lengkap_pasien',
				'label' => 'Nama Pasien',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'tindakan[]',
				'label' => 'Tindakan',
				'rules' => 'required|trim|xss_clean'
			]
		];

		if ( $this->ion_auth->is_admin() ) {

			$validation_rules[] = [
				'field' => 'id_perawat',
				'label' => 'ID Perawat',
				'rules' => 'required|trim|xss_clean'
			];

			$validation_rules[] = [
				'field' => 'nip',
				'label' => 'NIP',
				'rules' => 'required|trim|xss_clean'
			];

			$validation_rules[] = [
				'field' => 'nama_perawat',
				'label' => 'Nama Perawat',
				'rules' => 'required|trim|xss_clean'
			];

			$validation_rules[] = [
				'field' => 'nama_jenjang_pk',
				'label' => 'Jenjang PK',
				'rules' => 'required|trim|xss_clean'
			];

		}
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$this->db->trans_begin();

			
			$id_perawat    = ($this->ion_auth->in_group('perawat')) ? $this->userdata->id : $this->post('id_perawat', true);
			$id_pasien     = $this->post('id_pasien', true);
			$list_tindakan = $this->post('tindakan', true);

			if ( count($list_tindakan) > 0 ) {

				foreach ( $list_tindakan as $tindakan ) {

					$id_kewenangan_klinis = $tindakan['id'];
					$waktu = $tindakan['waktu'];

					$this->m_logbook->insert([
						'id_pasien' => $id_pasien,
						'id_perawat' => $id_perawat,
						'id_kewenangan_klinis' => $id_kewenangan_klinis,
						'waktu' => $waktu
					]);

				}

			}

			if ( $this->db->trans_status() == FALSE ) {

				$this->db->trans_rollback();

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menambahkan data.'
					]
				], 500);

			} else {

				$this->db->trans_commit();

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			}

		}
	}

	/**
	 * update data
	 */
	public function index_put()
	{
		$this->form_validation->set_data($this->put());
		$id = $this->put('id', TRUE);

		$validation_rules = [
			[
				'field' => 'id_pasien',
				'label' => 'ID Pasien',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'no_rm',
				'label' => 'No. RM',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'nama_lengkap_pasien',
				'label' => 'Nama Pasien',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'id_kewenangan_klinis',
				'label' => 'Nama Tindakan',
				'rules' => 'required|trim|xss_clean'
			],
			[
				'field' => 'waktu',
				'label' => 'Waktu',
				'rules' => 'required|trim|xss_clean'
			]
		];

		if ( $this->ion_auth->is_admin() ) {

			$validation_rules[] = [
				'field' => 'id_perawat',
				'label' => 'ID Perawat',
				'rules' => 'required|trim|xss_clean'
			];

			$validation_rules[] = [
				'field' => 'nip',
				'label' => 'NIP',
				'rules' => 'required|trim|xss_clean'
			];

			$validation_rules[] = [
				'field' => 'nama_perawat',
				'label' => 'Nama Perawat',
				'rules' => 'required|trim|xss_clean'
			];

			$validation_rules[] = [
				'field' => 'nama_jenjang_pk',
				'label' => 'Jenjang PK',
				'rules' => 'required|trim|xss_clean'
			];

		}
 
		$this->form_validation->set_rules($validation_rules);
 
		if ( $this->form_validation->run() == FALSE ) {

			$this->response([
				'metadata' => [
					'code' => 400,
					'message' => strip_tags(validation_errors())
				]
			], 400);

		} else {

			$data = [
				'id_perawat'           => ($this->ion_auth->in_group('perawat')) ? $this->userdata->id : $this->put('id_perawat', true),
				'id_pasien'            => $this->put('id_pasien', true),
				'id_kewenangan_klinis' => $this->put('id_kewenangan_klinis', true),
				'waktu'                => $this->put('waktu', true)
			];

			if ( $this->m_logbook->update($data, $id) ) {

				$this->response([
					'metadata' => [
						'code' => 200,
						'message' => 'OK'
					]
				]);

			} else {

				$this->response([
					'metadata' => [
						'code' => 500,
						'message' => 'Server error. Gagal menambahkan data.'
					]
				], 500);

			}

		}
	}

	/**
	 * delete data
	 */
	public function index_delete()
	{
		$id = $this->delete('id');

		if ( is_array($id) ) {
			$result = $this->m_logbook->delete_batch($id);
		} else {
			$result = $this->m_logbook->delete($id);
		}

		if ( $result == false ) {
			$this->response([
				'metadata' => [
					'code' => 500,
					'message' => 'Server error. Gagal menghapus data.'
				]
			], 500);
		} else {
			$this->response([
				'metadata' => [
					'code' => 200,
					'message' => 'OK'
				]
			]);
		}
	}

}

/* End of file jabatan.php */
/* Location: ./application/modules/jabatan/controllers/rest/jabatan.php */