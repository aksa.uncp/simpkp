<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logbook extends SP_Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('logbook/m_logbook');
	}

	public function index()
	{
		$this->data['page_title'] = 'Log Book';
		$this->data['page_description'] = 'Riwayat Tindakan Keperawatan';

		$this->template->set_partial('js', 'logbook/js/index.js.php');
		$this->template->set_partial('modals', 'logbook/modals/index.modal.php');

		$this->template->build('logbook/index', $this->data);
	}

	public function add()
	{
		$this->data['page_title'] = 'Input Log Book';
		$this->data['page_description'] = 'Menambahkan Tindakan Keperawatan';

		$this->template->set_partial('js', 'logbook/js/add.js.php');
		$this->template->set_partial('modals', 'logbook/modals/add.modal.php');

		$this->template->build('logbook/add', $this->data);
	}

	public function edit($id)
	{
		$this->data['page_title'] = 'Tindakan';
		$this->data['page_description'] = 'Mengubah Data Tindakan';

		$this->data['data'] = $this->m_logbook->row([
			'where' => [
				'id' => $id
			]
		]);

		$this->template->set_partial('js', 'logbook/js/edit.js.php');
		$this->template->set_partial('modals', 'logbook/modals/edit.modal.php');

		$this->template->build('logbook/edit', $this->data);
	}

}

/* End of file Logbook.php */
/* Location: ./application/modules/logbook/controllers/Logbook.php */