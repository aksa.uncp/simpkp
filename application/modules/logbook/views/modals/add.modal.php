<?php if ( $this->ion_auth->is_admin() ) : ?>
	<div id="modal-select-perawat" tabindex="-1" role="dialog" class="modal fade in">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
						<span class="sr-only">Close</span>
					</button>

					<div class="text-center">
						<span class="icon icon-wheelchair icon-5x m-y-lg"></span>
						<h3 class="modal-title">Perawat</h3>
						<p>Pilih Data Perawat</p>
					</div>
				</div>
				<div class="modal-body">
					<table class="table table-nowrap table-bordered table-hover" id="table-select-perawat" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="25"></th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Email</th>
								<th>ID Jabatan</th>
								<th>Jabatan</th>
								<th>ID Pangkat</th>
								<th>Pangkat</th>
								<th>Golongan</th>
								<th>Jenjang PK</th>
								<th width="10"></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<div id="modal-select-pasien" tabindex="-1" role="dialog" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>

				<div class="text-center">
					<span class="icon icon-wheelchair icon-5x m-y-lg"></span>
					<h3 class="modal-title">Pasien</h3>
					<p>Pilih Data Pasien</p>
				</div>
			</div>
			<div class="modal-body">
				<table class="table table-nowrap table-bordered table-hover" id="table-select-pasien" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="25"></th>
							<th>No. RM</th>
							<th>Nama</th>
							<th>Sapaan</th>
							<th>Nama Lengkap</th>
							<th>JK</th>
							<th>Tgl. Lahir</th>
							<th width="10"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="modal-tindakan" tabindex="-1" role="dialog" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>

				<div class="text-center">
					<span class="icon icon-stethoscope icon-5x m-y-lg"></span>
					<h3 class="modal-title">Tindakan</h3>
					<p>Tindakan Keperawatan</p>
				</div>
			</div>
			<div class="modal-body">
				<table class="table table-nowrap table-bordered table-hover" id="table-tindakan" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="25"></th>
							<th class="none">ID Kategori</th>
							<th class="none">Kategori</th>
							<th>Urutan</th>
							<th>Kompetensi</th>
							<th>ID Jenjang PK</th>
							<th>PK</th>
							<th width="10"></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="modal-add-tindakan" tabindex="-1" role="dialog" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-add-tindakan" class="form-horizontal" role="form">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
						<span class="sr-only">Close</span>
					</button>

					<div class="text-center">
						<span class="icon icon-stethoscope icon-5x m-y-lg"></span>
						<h3 class="modal-title">Tambah Tindakan</h3>
						<p>Menambahkan Tindakan Keperawatan</p>
					</div>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-xs-3">Waktu</label>
						<div class="col-xs-9">
							<input name="waktu" type="text" class="form-control datetimepicker" autocomplete="off" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Tindakan</label>
						<div class="col-xs-9">
							<input type="hidden" name="id_kewenangan_klinis" />
							<p class="form-control-static nama_tindakan"></p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="text-center">
						<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
						<button class="btn btn-default" type="button" data-dismiss="modal"><i class="fa fa-fw fa-times-circle"></i> Batal</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>