<script type="text/javascript">
	$(function() {
		var selected = [];

		var tablejabatan = $('#table-logbook').DataTable({
			language: {
				emptyTable: "<center>Tidak ada data ditemukan</center>"
			},
			ordering: true,
			order: [[1, 'desc']],
			processing: true,
			serverSide: true,
			responsive: true,
			ajax: '<?= site_url("api/logbook/logbook/datatables") ?>',
			columns: [
				{ 
					orderable: false,
					data: null
				},
				{
					data: "waktu",
					mRender: function(data, type, row) {
						return moment(data).format("<b>DD/MM/YYYY</b> HH:mm");
					}
				},
				{ data: "id_tindakan", visible: false },
				{ data: "nama_tindakan", className: 'pre-wrap' },
				{ data: "id_kategori", visible: false },
				{ data: "nama_kategori", className: 'pre-wrap' },
				{
					data: "jenjang_pk",
					mRender: function(data, type, row) {
						return row.nama_jenjang_pk;
					}
				},
				{ data: "no_rm" },
				{ data: "nama_pasien" },

				<?php if ( $this->ion_auth->is_admin() ) : ?>
				{ data: "nip" },
				{ data: "nama_perawat" },
				<?php endif; ?>

				{
					orderable: false,
					data: null,
					mRender: function(data, type, row) {
						return "<a href='<?= site_url('logbook/edit/') ?>" + row.id + "' class='edit text-primary'><i class='fa fa-fw fa-pencil'></i></a>";
					}
				},
				{
					orderable: false,
					data: null,
					mRender: function(data, type, row) {
						return "<a href='javascript:void(0)' class='remove text-danger'><i class='fa fa-fw fa-trash'></i></a>";
					}
				}
			],
			rowCallback: function( row, data ) {
				if ( $.inArray(data.id, selected) !== -1 ) {
					$(row).addClass('selected');
				}
			},
			columnDefs: [
				{ targets: 0, className: "text-center gray" },
				{ targets: [0,1,6,7,-1,-2], className: "text-center" },
				{ targets: [7,8], orderable: false },
				{ targets: 0, responsivePriority: 1 },
				{ targets: 1, responsivePriority: 2 },
				{ targets: -2, responsivePriority: 3 },
				{ targets: -1, responsivePriority: 4 }
			],
		});

		tablejabatan.on( 'draw', function (eve, obj) {
			var start = obj.oAjaxData.start;
			let no = start + 1;
			tablejabatan.column(0).nodes().each( function(cell, i) {
				cell.innerHTML = i + no;
			});
		});

		$('#table-logbook tbody').on('click', 'tr', function () {
			let data = tablejabatan.row(this).data();
			let id = data.id;
			let index = $.inArray(id, selected);

			if ( index === -1 ) {
				selected.push( id );
			} else {
				selected.splice( index, 1 );
			}

			$(this).toggleClass('selected');
		} );

		$('#remove-selected').on('click', function() {
			swal({
				title: 'Hapus Data',
				text: 'Apakah anda yakin akan menghapus data yang dipilih?',
				icon: 'warning',
				dangerMode: true,
				buttons: {
					cancel: "Tidak",
					confirm: "Ya, hapus data!"
				}
			})
			.then((isConfirm) => {
				if (isConfirm) {
					$.ajax({
						url: '<?= site_url("api/logbook/logbook") ?>',
						method: 'DELETE',
						data: {
							id: selected
						},
						dataType: 'json',
						beforeSend: function() {
							$('#main-content').waitMe({
								effect: 'rotation',
								text: 'Tunggu...',
								bg: 'rgba(255,255,255,0.7)',
								color: '#000',
								waitTime: -1,
								textPos: 'vertical'
							});
						},
						success: function(xhr) {
							try {
								let data = xhr.response;
								swal('Berhasil', 'Berhasil menghapus data', 'success');
								tablejabatan.ajax.reload(null, false);
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						error: function(xhr) {
							try {
								let error = JSON.parse(xhr.responseText);
								swal({
									title: 'Oops..',
									icon: 'error',
									text: error.metadata.message
								});
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						complete: function(xhr) {
							$('#main-content').waitMe('hide');
						}
					});
				}
			});
		});

		$('#table-logbook tbody').on('click', '.remove', function() {
			let data = tablejabatan.row( $(this).parents('tr') ).data();

			swal({
				title: 'Hapus Data',
				text: 'Apakah anda yakin akan menghapus data ini?',
				icon: 'warning',
				dangerMode: true,
				buttons: {
					cancel: "Tidak",
					confirm: "Ya, hapus data!"
				}
			})
			.then((isConfirm) => {
				if (isConfirm) {
					$.ajax({
						url: '<?= site_url("api/logbook/logbook") ?>',
						method: 'DELETE',
						data: {
							id: data.id
						},
						dataType: 'json',
						beforeSend: function() {
							$('#main-content').waitMe({
								effect: 'rotation',
								text: 'Tunggu...',
								bg: 'rgba(255,255,255,0.7)',
								color: '#000',
								waitTime: -1,
								textPos: 'vertical'
							});
						},
						success: function(xhr) {
							try {
								let data = xhr.response;
								swal('Berhasil', 'Berhasil menghapus data', 'success');
								tablejabatan.ajax.reload(null, false);
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						error: function(xhr) {
							try {
								let error = JSON.parse(xhr.responseText);
								swal({
									title: 'Oops..',
									icon: 'error',
									text: error.metadata.message
								});
							} catch(e) {
								swal('Oops..', 'Terjadi kesalahan', 'error');
								console.log(e);
							}
						},
						complete: function(xhr) {
							$('#main-content').waitMe('hide');
						}
					});
				}
			});
		});

		$('#form-search').on('submit', function(e) {
			e.preventDefault();

			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				tablejabatan.column(i).search($(this).val());
			});

			tablejabatan.draw();

			$('#modal-search').modal('hide');
		});

		$('#form-search').on('reset', function() {
			$(this).find('.form-control').each(function() {
				var i = $(this).attr('data-column');

				tablejabatan.column(i).search('');
			});

			tablejabatan.draw();

			$('#modal-search').modal('hide');
		});
	});
</script>