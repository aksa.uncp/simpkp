<script type="text/javascript">
	$(function() {
		let tablePerawat;
		let tablePasien;
		let tableTindakan;

		$('#form-add').on('submit', function(e) {
			e.preventDefault();

			let data = $(this).serializeObject();

			$.ajax({
				url: '<?= site_url("api/logbook/logbook") ?>',
				method: 'POST',
				dataType: 'json',
				data: data,
				beforeSend: function() {
					$('#main-content').waitMe({
						effect: 'rotation',
						text: 'Tunggu...',
						bg: 'rgba(255,255,255,0.7)',
						color: '#000',
						waitTime: -1,
						textPos: 'vertical'
					});
				},
				success: function(xhr) {
					try {
						let data = xhr.response;
						swal('Berhasil', 'Berhasil menambahkan data', 'success');
						$('#form-add').resetForm();
						$('#form-add').find('#list-tindakan').html('');
					} catch(e) {
						swal('Oops..', 'Terjadi kesalahan', 'error');
					}
				},
				error: function(xhr) {
					try {
						let error = JSON.parse(xhr.responseText);
						swal({
							title: 'Oops..',
							icon: 'error',
							text: error.metadata.message
						});
					} catch(e) {
						swal('Oops..', 'Terjadi kesalahan', 'error');
					}
				},
				complete: function(xhr) {
					$('#main-content').waitMe('hide');
				}
			});
		});

		$('#modal-select-perawat').on('shown.bs.modal', function() {
			if ($.fn.DataTable.isDataTable("#table-select-perawat") === false) {
				tablePerawat = $('#table-select-perawat').DataTable({
					language: {
						emptyTable: "<center>Tidak ada perawat ditemukan</center>"
					},
					destroy: true,
					order: [[1,'asc']],
					processing: true,
					serverSide: true,
					responsive: true,
					searching: true,
					pagingType: "simple",
					ajax: '<?= site_url("api/perawat/perawat/datatables") ?>',
					columns : [
						{ 
							orderable: false,
							data: null
						},
						{ data: "nip" },
						{ data: "nama_lengkap" },
						{ data: "email", visible: false },
						{ data: "id_jabatan", visible: false },
						{ data: "nama_jabatan", visible: false },
						{ data: "id_pangkat", visible: false },
						{ data: "nama_pangkat", visible: false },
						{ data: "golongan", visible: false },
						{
							data: "jenjang_pk",
							mRender: function(data, type, row) {
								return row.nama_jenjang_pk;
							}
						},
						{
							data: null,
							orderable: false,
							mRender: function(data, type, row) {
								return '<a href="javascript:void(0)" class="select"><i class="fa fa-fw fa-hand-pointer-o"></i></a>';
							}
						}
					],
					columnDefs: [
						{ targets: 0, className: "text-center gray" },
						{ targets: [1,9], className: "text-center" },
						{ targets: 0, responsivePriority: 1 },
						{ targets: 1, responsivePriority: 2 },
						{ targets: -1, responsivePriority: 3 }
					],
				});

				tablePerawat.on( 'draw', function () {
					tablePerawat.column(0).nodes().each( function(cell, i) {
						cell.innerHTML = i + 1;
					} );
				} ).draw();
			} else {
				$('#table-select-perawat').DataTable().ajax.reload(null, true);
			}
		});

		$('#modal-select-perawat').on('click', '#table-select-perawat .select', function(e) {
			let data = tablePerawat.row( $(this).parents('tr') ).data();

			$('#modal-select-perawat').modal('hide');

			$('#form-add').find('[name="id_perawat"]').val(data.id);
			$('#form-add').find('[name="nip"]').val(data.nip);
			$('#form-add').find('[name="nama_perawat"]').val(data.nama_lengkap);
			$('#form-add').find('[name="nama_jenjang_pk"]').val(data.nama_jenjang_pk);
		});

		$('#modal-select-pasien').on('shown.bs.modal', function() {
			if ($.fn.DataTable.isDataTable("#table-select-pasien") === false) {
				tablePasien = $('#table-select-pasien').DataTable({
					language: {
						emptyTable: "<center>Tidak ada pasien ditemukan</center>"
					},
					destroy: true,
					order: [[1,'asc']],
					processing: true,
					serverSide: true,
					responsive: true,
					searching: true,
					pagingType: "simple",
					ajax: '<?= site_url("api/pasien/pasien/datatables") ?>',
					columns : [
						{ 
							orderable: false,
							data: null
						},
						{ data: "no_rm" },
						{ data: "nama_pasien", visible: false },
						{ data: "sapaan", visible: false },
						{ data: "nama_lengkap_pasien" },
						{ data: "jenis_kelamin", visible: false },
						{
							data: "tgl_lahir",
							mRender: function(data, type, row) {
								return moment(data).format('DD/MM/YYYY');
							}
						},
						{
							data: null,
							orderable: false,
							mRender: function(data, type, row) {
								return '<a href="javascript:void(0)" class="select"><i class="fa fa-fw fa-hand-pointer-o"></i></a>';
							}
						}
					],
					columnDefs: [
						{ targets: 0, className: "text-center gray" },
						{ targets: [1,5,6], className: "text-center" },
						{ targets: 0, responsivePriority: 1 },
						{ targets: 4, responsivePriority: 2 },
						{ targets: -1, responsivePriority: 3 }
					],
				});

				tablePasien.on( 'draw', function () {
					tablePasien.column(0).nodes().each( function(cell, i) {
						cell.innerHTML = i + 1;
					} );
				} ).draw();
			} else {
				$('#table-select-pasien').DataTable().ajax.reload(null, true);
			}
		});

		$('#modal-select-pasien').on('click', '#table-select-pasien .select', function(e) {
			let data = tablePasien.row( $(this).parents('tr') ).data();

			$('#modal-select-pasien').modal('hide');

			$('#form-add').find('[name="id_pasien"]').val(data.id);
			$('#form-add').find('[name="no_rm"]').val(data.no_rm);
			$('#form-add').find('[name="nama_lengkap_pasien"]').val(data.nama_lengkap_pasien);
		});

		$('#modal-tindakan').on('shown.bs.modal', function() {
			if ($.fn.DataTable.isDataTable("#table-tindakan") === false) {
				tableTindakan = $('#table-tindakan').DataTable({
					language: {
						emptyTable: "<center>Tidak ada tindakan ditemukan</center>"
					},
					destroy: true,
					order: [[1,'asc'],[3,'asc']],
					processing: true,
					serverSide: true,
					responsive: true,
					searching: true,
					pagingType: "simple",
					ajax: '<?= site_url("api/kewenangan_klinis/tindakan/datatables") ?>',
					columns : [
						{ 
							orderable: false,
							data: null
						},
						{ data: "id_kategori", visible: false },
						{ data: "nama_kategori", className: "pre-wrap" },
						{ data: "urutan", visible: false },
						{ data: "nama_kompetensi", className: "pre-wrap" },
						{ data: "jenjang_pk", visible: false },
						{ data: "nama_jenjang_pk" },
						{
							data: null,
							orderable: false,
							mRender: function(data, type, row) {
								return '<a href="javascript:void(0)" class="select"><i class="fa fa-fw fa-hand-pointer-o"></i></a>';
							}
						}
					],
					columnDefs: [
						{ targets: 0, className: "text-center gray" },
						{ targets: [6], className: "text-center" },
						{ targets: 0, responsivePriority: 1 },
						{ targets: 4, responsivePriority: 2 },
						{ targets: -1, responsivePriority: 3 }
					],
				});

				tableTindakan.on( 'draw', function () {
					tableTindakan.column(0).nodes().each( function(cell, i) {
						cell.innerHTML = i + 1;
					} );
				} ).draw();
			} else {
				$('#table-tindakan').DataTable().ajax.reload(null, true);
			}
		});

		$('#modal-tindakan').on('click', '#table-tindakan .select', function(e) {
			let data = tableTindakan.row( $(this).parents('tr') ).data();

			$('#modal-tindakan').modal('hide');
			$('#modal-add-tindakan').modal('show');

			$('#form-add-tindakan').find('[name="waktu"]').val(data.no_rm);
			$('#form-add-tindakan').find('[name="id_kewenangan_klinis"]').val(data.id);
			$('#form-add-tindakan').find('.nama_tindakan').html(data.nama_kompetensi);
			
		});

		$('#modal-add-tindakan').on('shown.bs.modal', function() {
			$('#form-add-tindakan [name="waktu"]').datetimepicker({
				format: 'YYYY-MM-DD HH:mm:ss'
			});
		});

		$('#form-add-tindakan').on('submit', function(e) {
			e.preventDefault();

			let waktu = $('#form-add-tindakan').find('[name="waktu"]').val();
			let id_kewenangan_klinis = $('#form-add-tindakan').find('[name="id_kewenangan_klinis"]').val();
			let nama_tindakan = $('#form-add-tindakan').find('.nama_tindakan').html();

			if (waktu == "") {
				swal('Oops..', 'Waktu tidak boleh kosong', 'error');
			} else {
				$('#form-add').find('#list-tindakan').append(function() {
					$('#modal-add-tindakan').modal('hide');

					return '<a href="#" class="list-group-item">'+
							   '<input type="hidden" name="tindakan[][id]" value="'+ id_kewenangan_klinis +'" />'+
							   '<input type="hidden" name="tindakan[][waktu]" value="'+ waktu +'" />'+
							   '<div style="font-weight:bold">'+ nama_tindakan +'</div>'+
							   '<div><i>'+ moment(waktu).format('DD/MM/YYYY HH:mm:ss') +'</i></div>'+
						   '</a>';
				});
			}
		});
	});
</script>