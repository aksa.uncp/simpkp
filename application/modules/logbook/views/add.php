<div class="card">
	<div class="card-header">
		<div>
			<a href="<?= site_url("logbook") ?>" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-circle-left"></i> Kembali</a>
		</div>
	</div>
	<form id="form-add" class="form-horizontal" role="form">
		<div class="card-body">
			<div class="col-md-8 col-md-offset-2">
				<?php if ( $this->ion_auth->is_admin() ) : ?>
					<div class="group">
						<div class="text-center m-b">
							<h4 class="m-b-0"><i class="fa fa-fw fa-user-md"></i> Perawat</h4>
							<small>Identitas Perawat</small>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3">NIP</label>
							<div class="col-xs-9">
								<div class="input-group">
									<input name="id_perawat" type="hidden" />
									<input name="nip" type="text" class="form-control" readonly="" />
									<div class="input-group-btn">
										<button class="btn btn-primary" type="button" id="btn-select-perawat" data-toggle="modal" href="#modal-select-perawat">
											<i class="fa fa-fw fa-list"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3">Nama Perawat</label>
							<div class="col-xs-9">
								<input name="nama_perawat" type="text" class="form-control" readonly="" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-xs-3">Jenjang PK</label>
							<div class="col-xs-9">
								<input name="nama_jenjang_pk" type="text" class="form-control" readonly="" />
							</div>
						</div>
					</div>
				<?php endif; ?>

				<div class="group">
					<div class="text-center m-b">
						<h4 class="m-b-0"><i class="fa fa-fw fa-user"></i> Pasien</h4>
						<small>Identitas Pasien</small>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">No. RM</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input name="id_pasien" type="hidden" />
								<input name="no_rm" type="text" class="form-control" readonly="" />
								<div class="input-group-btn">
									<button class="btn btn-primary" type="button" id="btn-select-pasien" data-toggle="modal" href="#modal-select-pasien">
										<i class="fa fa-fw fa-list"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">Nama Pasien</label>
						<div class="col-xs-9">
							<input name="nama_lengkap_pasien" type="text" class="form-control" readonly="" />
						</div>
					</div>
				</div>
				<div class="group">
					<div class="text-center m-b">
						<h4 class="m-b-0"><i class="fa fa-fw fa-stethoscope"></i> Tindakan</h4>
						<small>Tindakan Keperawatan</small>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="list-group" id="list-tindakan"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<a href="#modal-tindakan" data-toggle="modal" class="tambah-tindakan-btn">
								<i class="fa fa-fw fa-plus-circle"></i> Tambah Tindakan
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="text-center">
				<button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-check-circle"></i> Simpan</button>
				<a class="btn btn-default" href="<?= site_url('jabatan') ?>"><i class="fa fa-fw fa-times-circle"></i> Batal</a>
			</div>
		</div>
	</form>
</div>