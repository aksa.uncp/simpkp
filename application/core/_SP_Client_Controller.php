<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SP_Client_Controller extends MX_Controller {

	public $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->setTheme();

		$this->data['app_title'] = $this->config->item('app_title');
		$this->data['app_description'] = $this->config->item('app_description');
		$this->data['app_version'] = $this->config->item('app_version');
	}

	private function setTheme()
	{
		$this->template->set_theme($this->config->item('client_theme'));
	}

}

/* End of file _SP_Client_Controller.php */
/* Location: ./application/controllers/_SP_Client_Controller.php */