<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__ . DIRECTORY_SEPARATOR . '_Custom_Model.php';

class MY_Model extends Custom_Model {

	public $table;
	public $view;
	public $primary_key;

	public function __construct()
	{
		parent::__construct();
	}

	protected function query($query=array())
	{
		if ( isset($query['select']) ) $this->db->select($query['select']);
		if ( isset($query['from']) ) $this->db->from($query['from']);
		if ( isset($query['where']) ) $this->db->where($query['where']);
		if ( isset($query['where_no_escape']) ) $this->db->where($query['where_no_escape'], null, false);
		if ( isset($query['or_where']) ) $this->db->or_where($query['or_where']);
		if ( isset($query['or_where_no_escape']) ) $this->db->or_where($query['or_where_no_escape'], null, false);
		if ( isset($query['where_in']) ) $this->db->where_in($query['where_in'][0], $query['where_in'][1]);
		if ( isset($query['limit']) ) $this->db->limit($query['limit']);
		if ( isset($query['limit_offset']) ) $this->db->limit($query['limit_offset'][0], $query['limit_offset'][1]);
		if ( isset($query['group_by']) ) $this->db->group_by($query['group_by']);

		if ( isset($query['join']) ) $this->db->join($query['join'][0], $query['join'][1]);
		if ( isset($query['left_join']) ) $this->db->join($query['left_join'][0], $query['left_join'][1], 'left');
		if ( isset($query['right_join']) ) $this->db->right($query['right_join'][0], $query['right_join'][1], 'right');

		if ( isset($query['order_by']) ) {
			if ( is_array($query['order_by']) ) {
				foreach ( $query['order_by'] as $column => $dir ) {
					$this->db->order_by($column." ".$dir, FALSE);
				}
			} else {
				$this->db->order_by($query['order_by']);
			}
		}
	}

	protected function _get($query=array())
	{
		$this->query($query);

		if ( ! empty($this->view) ) {
			$this->db->from($this->view);
		} else {
			$this->db->from($this->table);
		}

		$result = $this->db->get();

		return $result;
	}

	public function get($query=array())
	{
		$result = $this->_get($query);

		return [
			'count' => $result->num_rows(),
			'data' => $result->result()
		];
	}

	public function row($query=array())
	{
		return $this->_get($query)->row();
	}

	public function count($query=array())
	{
		if ( isset($query['select']) ) {
			$query['select'] .= "COUNT(*) AS count";
		} else {
			$query['select'] = "COUNT(*) AS count";
		}

		$result = $this->row($query);

		return $result->count;
	}

	/**
	 * Delete batch of data
	 * @param  Array $id array of primary key
	 * @return boolean
	 */
	public function delete_batch($id)
	{
		$result = $this->db
					   ->where_in($this->primary_key, $id)
					   ->delete($this->table);

		return $result;
	}

}

/* End of file _MY_Model.php */
/* Location: ./application/models/_MY_Model.php */