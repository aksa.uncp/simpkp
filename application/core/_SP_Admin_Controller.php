<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SP_Admin_Controller extends MX_Controller {

	public $data = [];

	protected $userdata;

	public function __construct()
	{
		parent::__construct();

		$this->setTheme();
		$this->authenticate();

		$this->userdata = $this->ion_auth->user()->row();

		$this->data['app_title']        = $this->config->item('app_title');
		$this->data['app_description']  = $this->config->item('app_description');
		$this->data['app_version']      = $this->config->item('app_version');
		$this->data['page_title']       = $this->config->item('app_title');
		$this->data['page_description'] = $this->config->item('app_description');
		$this->data['user']             = $this->userdata;
	}

	private function setTheme()
	{
		$this->template->set_theme($this->config->item('admin_theme'));
	}

	private function authenticate()
	{
		if ( ! $this->ion_auth->logged_in() ) {
			redirect('login');
		}
	}

}

/* End of file _SP_Admin_Controller.php */
/* Location: ./application/controllers/_SP_Admin_Controller.php */