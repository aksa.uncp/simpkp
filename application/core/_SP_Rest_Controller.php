<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once __DIR__ .  DIRECTORY_SEPARATOR . 'REST_Controller.php';

class SP_Rest_Controller extends REST_Controller {

	public $data = [];

	protected $userdata;

	public function __construct()
	{
		parent::__construct();

		$this->authenticate();

		$this->userdata = $this->ion_auth->user()->row();

		$this->data['app_title']       = $this->config->item('app_title');
		$this->data['app_description'] = $this->config->item('app_description');
		$this->data['app_version']     = $this->config->item('app_version');
		$this->data['user']            = $this->userdata;
	}

	private function authenticate()
	{
		if ( 
			( $this->uri->uri_string() != 'api/users/users/auth' && $this->input->method() != 'post' )
		) {
			if ( ! $this->ion_auth->logged_in() ) {
				$this->response([
					'metadata' => [
						'code' => 403,
						'message' => 'Access forbidden'
					]
				], 403);
			}
		}
	}

}

/* End of file _SP_Rest_Controller.php */
/* Location: ./application/core/_SP_Rest_Controller.php */