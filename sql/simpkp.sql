/*
 Navicat Premium Data Transfer

 Source Server         : Laragon (MySQL)
 Source Server Type    : MySQL
 Source Server Version : 100312
 Source Host           : localhost:3306
 Source Schema         : simpkp

 Target Server Type    : MySQL
 Target Server Version : 100312
 File Encoding         : 65001

 Date: 16/09/2020 09:25:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'perawat', 'Perawat');

-- ----------------------------
-- Table structure for indikator
-- ----------------------------
DROP TABLE IF EXISTS `indikator`;
CREATE TABLE `indikator`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_kategori` int(10) UNSIGNED NULL DEFAULT NULL,
  `urutan` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `nama_indikator` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `parent` int(10) UNSIGNED NULL DEFAULT NULL,
  `target` float UNSIGNED NULL DEFAULT NULL,
  `satuan` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bobot` float UNSIGNED NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_indikator_sub_kategori_indikator`(`id_kategori`) USING BTREE,
  INDEX `FK_indikator_indikator`(`parent`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 36 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Indikator penilaian' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of indikator
-- ----------------------------
INSERT INTO `indikator` VALUES (1, 1, 1, 'Melakukan pengkajian', NULL, NULL, '', NULL, '2018-09-15 13:30:24', '2018-10-12 20:51:56', NULL);
INSERT INTO `indikator` VALUES (2, 1, 2, 'Pengkajian awal keperawatan/kebidanan', 1, 9, 'RM/Pasien', 5, '2018-09-15 13:31:45', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (3, 1, 3, 'Melakukan pengkajian harian/lanjutan', 1, 180, 'RM/Pasien', 3, '2018-09-15 13:32:12', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (4, 1, 4, 'Merumuskan diagnosa keperawatan/kebidanan', NULL, 378, 'RM/Pasien', 2.5, '2018-09-15 13:32:33', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (5, 1, 5, 'Merumuskan rencana keperawatan/kebidanan', NULL, 756, 'RM/Pasien', 2.5, '2018-09-15 13:33:01', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (6, 1, 6, 'Melaksanakan tindakan keperawatan/kebidanan', NULL, 605, 'Tindakan', 5, '2018-09-15 13:33:27', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (7, 1, 7, 'Melakukan evaluasi keperawatan/kebidanan', NULL, 378, 'RM/Pasien', 2, '2018-09-15 13:34:17', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (8, 1, 8, 'Melakukan edukasi keperawatan/kebidanan', NULL, 36, 'RM/Pasien', 5, '2018-09-15 13:34:37', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (9, 1, 9, 'Merencanakan pasien pulang/pindah (Discharge planning)', NULL, 9, 'RM/Pasien', 5, '2018-09-15 13:35:06', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (10, 2, 1, 'Pengkajian awal keperawatan/kebidanan terdokumentasi lengkap dalam waktu < 24 jam', NULL, 100, '%', 4, '2018-10-11 17:17:20', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (11, 2, 2, 'Pengkajian harian/lanjutan terdokumentasi lengkap dalam waktu < 24 jam', NULL, 100, '%', 4, '2018-10-11 17:17:47', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (12, 2, 3, 'Diagnosa keperawatan/kebidanan terdokumentasi lengkap', NULL, 100, '%', 3, '2018-10-11 17:18:06', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (13, 2, 4, 'Melaksanakan tindakan keperawatan secara tepat', NULL, 100, '%', 5, '2018-10-11 17:18:30', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (14, 2, 5, 'Evaluasi keberhasilan tindakan keperawatan/kebidanan dan terdokumentasi secara lengkap', NULL, 100, '%', 3, '2018-10-11 17:18:56', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (15, 2, 6, 'Kelengkapan dokumentasi', NULL, 100, '%', 5, '2018-10-11 17:19:23', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (16, 2, 7, 'Laporan KPC/KNC/KTD', NULL, 100, '%', 2, '2018-10-11 17:19:35', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (17, 2, 8, 'Mengumpulkan data indikator mutu', NULL, 100, '%', 2, '2018-10-11 17:20:34', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (18, 2, 9, 'Kepuasan konsumen', NULL, 90, '%', 2, '2018-10-11 17:21:35', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (19, 3, 1, 'Keberadaan', NULL, 100, '%', 5, '2018-10-11 17:22:03', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (20, 3, 2, 'Inisiatif', NULL, 100, '%', 4, '2018-10-11 17:22:10', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (21, 3, 3, 'Kehandalan', NULL, 100, '%', 4, '2018-10-11 17:22:18', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (22, 3, 4, 'Kepatuhan', NULL, NULL, NULL, NULL, '2018-10-11 17:22:25', '2018-10-11 20:21:06', NULL);
INSERT INTO `indikator` VALUES (23, 3, 5, 'Hadir tepat waktu setiap shift', 22, 100, '%', 3, '2018-10-11 17:23:04', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (24, 3, 6, 'Kesesuaian waktu dinas dengan jadwal yang telah dibuat', 22, 100, '%', 2, '2018-10-11 17:25:15', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (25, 3, 7, 'Pengembangan diri profesional', NULL, NULL, NULL, NULL, '2018-10-11 17:26:34', NULL, NULL);
INSERT INTO `indikator` VALUES (26, 3, 8, 'Mengikuti kegiatan DRK', 25, 100, '%', 1, '2018-10-11 17:27:02', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (27, 3, 9, 'Mengikuti pre & post conference', 25, 100, '%', 1, '2018-10-11 17:27:36', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (28, 3, 10, 'Operan jaga setiap shift', 25, 100, '%', 1, '2018-10-11 17:27:49', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (29, 3, 11, 'Pertemuan rutin bulanan', 25, 100, '%', 1, '2018-10-11 17:28:03', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (30, 3, 12, 'Kerjasama', NULL, 100, '%', 4, '2018-10-11 17:28:14', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (31, 3, 13, 'Sikap Perilaku', NULL, 100, '%', 4, '2018-10-11 17:28:24', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (32, 4, 1, 'On call', NULL, 3, '', 2, '2018-10-11 17:28:44', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (33, 4, 2, 'Siaga code blue', NULL, 3, '', 2, '2018-10-11 17:28:58', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (34, 4, 3, 'Home care', NULL, 3, '', 2, '2018-10-11 17:29:13', '2019-01-04 21:00:36', NULL);
INSERT INTO `indikator` VALUES (35, 4, 4, 'Upacara', NULL, 1, '', 1, '2018-10-11 17:29:20', '2019-01-04 21:00:36', NULL);

-- ----------------------------
-- Table structure for jabatan
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Jabatan perawat' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES (1, 'Perawat Terampil', '2018-09-02 07:21:02', '2018-09-02 07:24:06', NULL);

-- ----------------------------
-- Table structure for kategori_indikator
-- ----------------------------
DROP TABLE IF EXISTS `kategori_indikator`;
CREATE TABLE `kategori_indikator`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `urutan` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `nama_kategori` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Kategori indikator penilaian' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kategori_indikator
-- ----------------------------
INSERT INTO `kategori_indikator` VALUES (1, 1, 'Indikator Kuantitas', '2018-09-15 09:57:32', '2018-09-15 13:48:42', NULL);
INSERT INTO `kategori_indikator` VALUES (2, 2, 'Indikator Kualitas', '2018-09-15 10:04:45', NULL, NULL);
INSERT INTO `kategori_indikator` VALUES (3, 3, 'Perilaku Kerja', '2018-09-15 10:04:56', NULL, NULL);
INSERT INTO `kategori_indikator` VALUES (4, 4, 'Kegiatan Tambahan', '2018-09-15 10:05:08', NULL, NULL);

-- ----------------------------
-- Table structure for kategori_kewenangan_klinis
-- ----------------------------
DROP TABLE IF EXISTS `kategori_kewenangan_klinis`;
CREATE TABLE `kategori_kewenangan_klinis`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `urutan` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `nama_kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 23 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Kategori kewenangan klinis' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kategori_kewenangan_klinis
-- ----------------------------
INSERT INTO `kategori_kewenangan_klinis` VALUES (1, 1, 'Penerapan Prinsip Etik dan Legal', '2018-09-06 23:23:54', '2018-09-15 09:26:40', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (2, 2, 'Komunikasi Interpersonal', '2018-09-06 23:24:02', '2018-09-15 09:17:35', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (3, 3, 'Patient Safety', '2018-09-06 23:24:09', '2018-09-15 09:17:48', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (4, 4, 'Prinsip Pengendalian dan Pencegahan Infeksi', '2018-09-06 23:24:23', '2018-09-15 09:17:53', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (5, 5, 'Pencegahan Cedera', '2018-09-06 23:24:29', '2018-09-15 09:17:56', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (6, 6, 'Pemenuhan Kebutuhan Oksigen', '2018-09-06 23:24:44', '2018-09-15 09:18:01', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (7, 7, 'Pemenuhan Kebutuhan Cairan dan Elektrolit', '2018-09-06 23:24:59', '2018-09-15 09:18:03', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (8, 8, 'Pengukuran Tanda Tanda Vital', '2018-09-06 23:25:35', '2018-09-15 09:18:05', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (9, 9, 'Analisis Interpretasi, dan Dokumentasi Data Secara Akurat', '2018-09-06 23:25:49', '2018-09-15 09:18:07', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (10, 10, 'Perawatan Luka', '2018-09-06 23:25:53', '2018-09-15 09:18:13', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (11, 11, 'Pemberian Obat Dengan Aman dan Benar', '2018-09-06 23:26:09', '2018-09-15 09:18:15', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (12, 12, 'Pengelolaan Pemberian Darah', '2018-09-06 23:26:15', '2018-09-15 09:18:17', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (13, 13, 'Pemenuhan Kebutuhan Nutrisi dan Eliminasi', '2018-09-06 23:26:25', '2018-09-15 09:18:24', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (14, 14, 'Pemenuhan Kebutuhan Belajar', '2018-09-06 23:26:36', '2018-09-15 09:18:25', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (15, 15, 'Pemenuhan Kebutuhan Psikososial dan Spiritual', '2018-09-06 23:26:48', '2018-09-15 09:18:27', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (16, 16, 'Pemenuhan Kebutuhan Istirahat Tidur', '2018-09-06 23:27:01', '2018-09-15 09:18:30', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (17, 17, 'Pemenuhan Kebutuhan Aktivitas dan Mobilisasi', '2018-09-15 09:20:51', NULL, NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (18, 18, 'Pemenuhan Kebutuhan Seksualitas', '2018-09-06 23:27:20', '2018-10-24 13:07:21', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (19, 19, 'Pemeriksaan Penunjang', '2018-09-06 23:27:32', '2018-10-24 13:07:44', NULL);
INSERT INTO `kategori_kewenangan_klinis` VALUES (20, 19, 'Pemenuhan Kebutuhan Kenyamanan', '2018-09-06 23:27:40', '2018-09-15 09:18:36', NULL);

-- ----------------------------
-- Table structure for kewenangan_klinis
-- ----------------------------
DROP TABLE IF EXISTS `kewenangan_klinis`;
CREATE TABLE `kewenangan_klinis`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_kategori` int(10) UNSIGNED NULL DEFAULT NULL,
  `jenjang_pk` tinyint(4) NULL DEFAULT NULL,
  `urutan` tinyint(4) NOT NULL DEFAULT 0,
  `nama_kompetensi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_kewenangan_klinis_kategori_kewenangan_klinis`(`id_kategori`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 225 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Kewenangan klinis perawat' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kewenangan_klinis
-- ----------------------------
INSERT INTO `kewenangan_klinis` VALUES (1, 1, 1, 1, 'Melakukan asesmen pasien (dengan senyum, salam, sapa, sopan dan santun (5S))', '2018-09-06 23:29:11', '2018-09-07 09:49:36', NULL);
INSERT INTO `kewenangan_klinis` VALUES (2, 1, 1, 2, 'Mengorientasikan pasien dan keluarga tentang ruang rawat yang ditempati', '2018-09-06 23:29:40', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (3, 1, 1, 3, 'Meminta persetujuan setiap tindakan yang dilakukan', '2018-09-06 23:29:52', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (4, 1, 1, 4, 'Menghargai keputusan pasien dan keluarga dalam proses perawatan', '2018-09-06 23:30:01', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (5, 1, 1, 5, 'Memberikan informasi yang akurat tentang proses perawatan', '2018-09-06 23:30:20', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (6, 1, 1, 6, 'Menjaga privasi pasien ', '2018-09-06 23:30:28', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (7, 1, 1, 7, 'Melakukan pelayanan dengan prinsip caring', '2018-09-06 23:30:37', '2018-10-18 23:58:07', NULL);
INSERT INTO `kewenangan_klinis` VALUES (9, 1, 3, 8, 'Melakukan perawatan pasien dengan penyakit terminal', '2018-10-22 21:51:31', '2018-10-22 21:53:52', NULL);
INSERT INTO `kewenangan_klinis` VALUES (10, 2, 1, 1, 'Memperkenalkan diri ', '2018-10-22 21:56:03', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (11, 2, 1, 2, 'Menjadi pendengar yang baik', '2018-10-22 21:56:37', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (12, 2, 1, 3, 'Membina hubungan saling percaya ', '2018-10-22 21:57:08', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (13, 2, 1, 4, 'Menerapkan prinsip komunikasi terapiutik', '2018-10-22 21:58:38', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (14, 2, 1, 5, 'Menunjukkan sikap empati', '2018-10-22 22:00:05', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (15, 1, 1, 6, 'Menggunakan bahasa yang mudah dimengerti pasien', '2018-10-22 22:00:32', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (16, 2, 3, 6, 'Melaporkan segala perubahan dan kondisi pasien pada PK IV atau DPJP', '2018-10-22 22:01:06', '2018-10-22 22:01:29', NULL);
INSERT INTO `kewenangan_klinis` VALUES (17, 3, 1, 1, 'Menerapkan perinsip 6 goals patient safety', '2018-10-22 22:04:16', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (18, 3, 1, 2, 'Melaporkan insiden KTC/KTD/KPC/KNC/sentinel.', '2018-10-22 22:04:34', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (19, 3, 1, 3, 'Menggunakan spilkit sesuai standar', '2018-10-22 22:05:19', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (20, 3, 1, 4, 'Melakukan interpretasi data indikator mutu', '2018-10-22 22:05:40', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (21, 4, 1, 1, 'Mengajarkan perinsip hand hygiene', '2018-10-22 22:06:45', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (22, 4, 1, 2, 'Menggunakan APD sesuai indikasi', '2018-10-22 22:07:18', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (23, 4, 1, 3, 'Mengajarkan etika batuk', '2018-10-22 22:07:56', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (24, 4, 1, 4, 'Membuang sampah sesuai SOP', '2018-10-22 22:10:34', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (25, 4, 1, 5, 'Memahami tehnik isolasi', '2018-10-22 22:13:27', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (26, 4, 1, 6, 'Memahami tehnik septik dan aseptik', '2018-10-22 22:13:55', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (27, 4, 1, 7, 'Meletakkan linen kotor pada tempatnya', '2018-10-22 22:14:24', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (28, 5, 1, 1, 'Memasang pengaman tempat tidur ', '2018-10-22 22:14:53', '2018-10-22 22:37:55', NULL);
INSERT INTO `kewenangan_klinis` VALUES (29, 5, 1, 3, 'Memasang gelang identitas', '2018-10-22 22:15:31', '2018-10-22 22:17:06', NULL);
INSERT INTO `kewenangan_klinis` VALUES (30, 5, 1, 2, 'Melakukan pengkajian resiko jatuh', '2018-10-22 22:16:08', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (31, 5, 1, 4, 'Memasang label resiko jatuh pada scala sedang-tinggi', '2018-10-22 22:19:34', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (32, 5, 3, 5, 'Melakukan analisa grading pasien resiko jatuh dan merencanakan tindakan', '2018-10-22 22:20:10', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (33, 5, 3, 6, 'Melakukan manajemen hiperglikemia', '2018-10-22 22:20:44', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (34, 5, 3, 7, 'Melakukan manajemen hipoglikemia', '2018-10-22 22:21:50', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (35, 5, 3, 8, 'Menyiapkan penatalaksanaan pemasangan central venous chateter (CVC) /Perpherally Inseted Central Venous Chateter (PICC)', '2018-10-22 22:22:15', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (36, 6, 1, 1, 'Memasang alat oksigenasi binasal kanule ', '2018-10-22 22:22:44', '2018-10-22 22:38:52', NULL);
INSERT INTO `kewenangan_klinis` VALUES (37, 6, 1, 2, 'Memasang alat oksigenasi simple mask', '2018-10-22 22:23:13', '2018-10-22 22:39:38', NULL);
INSERT INTO `kewenangan_klinis` VALUES (51, 6, 3, 15, 'Melakukan algoritma ACLS', '2018-10-22 22:40:42', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (38, 6, 1, 3, 'Melakukan postural drainage', '2018-10-22 22:23:47', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (39, 6, 1, 4, 'Melakukan bantuan hidup dasar (BHD)', '2018-10-22 22:24:12', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (40, 6, 1, 5, 'Memasang saturasi oksigen', '2018-10-22 22:27:39', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (41, 6, 1, 6, 'Memonitor saturasi oksigen ', '2018-10-22 22:28:18', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (42, 6, 2, 7, 'Melakukan manajemen jalan nafas tanpa alat', '2018-10-22 22:31:57', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (43, 6, 1, 8, 'Melakukan manajemen jalan nafas dengan alat (Suction oropharing)', '2018-10-22 22:32:19', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (44, 6, 2, 9, 'Menganalisa kebutuhan pemberian oksigenasi', '2018-10-22 22:32:46', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (45, 6, 2, 10, 'Melakukan bantuan pernafasan dengan bag velve mask', '2018-10-22 22:33:25', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (46, 6, 2, 11, 'Melakukan RJP', '2018-10-22 22:33:51', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (47, 6, 2, 12, 'Monitoring pasien perawatan water seal drainage (WSD) ', '2018-10-22 22:34:55', '2018-10-22 22:37:04', NULL);
INSERT INTO `kewenangan_klinis` VALUES (48, 6, 2, 13, 'Menganalisa hasil saturasi oksigen', '2018-10-22 22:35:17', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (49, 6, 2, 13, 'Melakukan chest fisioterapi  (Melakukan Fisioterapi dada)', '2018-10-22 22:35:43', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (50, 6, 2, 14, 'Melatih batuk efektif ', '2018-10-22 22:36:30', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (52, 6, 3, 16, 'Melakukan resusitasi jantung paru (RJP)', '2018-10-22 22:41:21', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (53, 7, 1, 1, 'Mengganti cairan infus', '2018-10-22 22:42:09', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (54, 7, 1, 2, 'Melepas infus', '2018-10-22 22:42:48', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (55, 7, 1, 3, 'Memberi minum melalui sonde', '2018-10-22 22:43:20', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (56, 7, 1, 4, 'Melepas NGT', '2018-10-22 22:43:48', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (57, 7, 1, 5, 'Menghitung tetesan infus', '2018-10-22 22:45:38', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (58, 7, 1, 6, 'Melakukan perawatan infus', '2018-10-22 22:46:05', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (59, 7, 1, 7, 'Melakukan bladder training', '2018-10-22 22:46:31', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (60, 7, 1, 8, 'Menghitung balance cairan', '2018-10-22 22:47:16', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (61, 7, 1, 9, 'mempertahankan kepatenan selang infus', '2018-10-22 22:47:44', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (62, 7, 1, 10, 'Pemasangan infus pada pasien dewasa tanpa penyulit', '2018-10-22 22:50:14', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (63, 7, 2, 11, 'Menganalisis kebutuhan cairan', '2018-10-22 22:51:00', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (64, 6, 2, 12, 'Mengobservasi dan memonitoring resiko perdarahan', '2018-10-22 22:52:25', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (65, 7, 2, 13, 'Memasang infus dengan penyulit ', '2018-10-22 22:53:06', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (66, 7, 2, 14, 'Manajemen cairan ', '2018-10-22 22:53:36', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (67, 7, 2, 15, 'Melakukan aff drain', '2018-10-22 22:54:14', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (68, 7, 2, 16, 'Melakukan kumbah lambung ', '2018-10-22 22:54:41', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (69, 7, 3, 17, 'Monitoring hemodinamik pada pasien dalam kondisi khusus dan kompleks ', '2018-10-22 22:55:42', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (70, 7, 3, 18, 'Manajemen pasien syok', '2018-10-22 22:56:11', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (71, 7, 3, 19, 'Manajemen elektrolit dan asam basa', '2018-10-22 22:57:13', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (72, 7, 3, 20, 'Penatalaksanaan koreksi imbalance elektrolit', '2018-10-22 22:57:49', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (73, 8, 1, 1, 'Mengukur tekanan darah ', '2018-10-22 22:58:14', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (74, 8, 1, 2, 'Mengukur frekwensi nadi (Menghitung denyut nadi)', '2018-10-22 22:58:50', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (75, 8, 1, 3, 'Mengukur frekwensi pernafasan (memantau pernafasan)', '2018-10-22 22:59:41', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (76, 8, 1, 4, 'Mengukur frekwensi jantung', '2018-10-22 23:00:22', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (77, 8, 1, 5, 'Mengukur suhu tubuh ', '2018-10-22 23:22:05', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (78, 8, 1, 6, 'Melakukan auskultasi bunyi nafas', '2018-10-22 23:22:29', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (79, 8, 1, 7, 'Mengukur temperatur', '2018-10-22 23:22:58', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (80, 8, 1, 8, 'Mengkaji tingkat nyeri (PQRST) ', '2018-10-22 23:26:05', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (81, 8, 1, 9, 'Mengoprasikan monitor bedside', '2018-10-22 23:26:34', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (82, 8, 2, 10, 'Melakukan pengkajian pola fungsional keperawatan dan pemeriksaan fisik', '2018-10-22 23:27:13', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (83, 8, 2, 11, 'Melakukan observasi dan monitoring hemodinamik ', '2018-10-22 23:29:25', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (84, 8, 1, 12, 'Mengobservasi dan menilai tanda-tanda pasien syok dan melapor ke PK III atau DPJP', '2018-10-22 23:30:02', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (85, 8, 3, 13, 'Memonitoring gangguan irama jantung', '2018-10-22 23:30:52', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (86, 8, 3, 14, 'Melakukan algoritma ACLS', '2018-10-22 23:32:18', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (87, 8, 3, 15, 'Penatalaksanaan Direct Current (DC) Shock', '2018-10-22 23:33:10', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (88, 9, 1, 1, 'Melakukan perekaman EKG dengan benar ', '2018-10-22 23:35:03', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (89, 9, 1, 3, 'Mendokumentasikan segala tindakan yang telah dilakukan ', '2018-10-22 23:35:30', '2018-10-22 23:37:59', NULL);
INSERT INTO `kewenangan_klinis` VALUES (90, 9, 2, 2, 'Melakukan Analisis data hasil pengkajian ', '2018-10-22 23:35:55', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (91, 9, 2, 4, 'Menentukan masalah atau diagnosa keperawatan pasien', '2018-10-22 23:37:05', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (92, 9, 2, 5, 'Menyusun rencana asuhan keperawatan berdasarkan diagnosa keperawatan yang ada', '2018-10-22 23:38:40', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (93, 9, 2, 6, 'Memantau status neurologis/ glaslow coma scale (GCS) ', '2018-10-22 23:39:33', '2018-10-22 23:39:58', NULL);
INSERT INTO `kewenangan_klinis` VALUES (94, 9, 2, 7, 'Melakukan pencatatan dan pelaporan program TB, MDR', '2018-10-22 23:40:53', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (95, 9, 2, 8, 'Melakukan analisa EKG aritmia/lethal', '2018-10-22 23:41:38', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (96, 9, 2, 9, 'Merevisi rencana asuhan keperawatan ', '2018-10-22 23:49:57', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (97, 9, 3, 10, 'Pengkajian keperawatan mandiri pada pasien resiko komplikasi', '2018-10-22 23:50:30', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (98, 9, 3, 11, 'Melakukan analisis data keperawatan', '2018-10-22 23:52:48', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (99, 9, 3, 12, 'Merumuskan masalah/diagnosa keperawatan khusus dan kompleks', '2018-10-22 23:53:18', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (100, 9, 3, 13, 'Menyusun rencana asuhan keperawatan yang menggambarkan intervensi pada klien dengan resiko komplikasi', '2018-10-22 23:53:53', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (101, 9, 3, 14, 'Pemeriksaan fisik neurologis spesifik', '2018-10-22 23:54:58', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (102, 9, 3, 15, 'Penilaian dan monitoring status neurologi', '2018-10-22 23:55:22', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (103, 9, 3, 16, 'Menilai kekuatan otot pasien imobilisasi', '2018-10-22 23:56:34', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (106, 9, 3, 17, 'Pengkajian nyeri pasien tidak sadar/tidak bisa mengungkapkan diri', '2018-10-23 00:02:18', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (108, 10, 1, 1, 'Melakukan pengkajian luka ', '2018-10-23 06:52:36', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (107, 9, 3, 18, 'Pengambilan dan analisis darah arteri (AGD)2', '2018-10-23 00:02:45', '2018-10-23 00:22:10', NULL);
INSERT INTO `kewenangan_klinis` VALUES (109, 10, 1, 2, 'Perawatan luka akut', '2018-10-23 06:53:08', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (110, 10, 1, 3, 'Perawatan luka post operasi ringan ', '2018-10-23 06:53:35', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (111, 1, 2, 4, 'Melakukan perawatan luka/ulkus', '2018-10-23 06:54:05', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (112, 10, 2, 5, 'Melakukan perawatan luka bakar derajat I & II  Melakukan perawatan luka bakar II & III)', '2018-10-23 06:54:30', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (113, 10, 3, 6, 'Perawatan luka dengan komplikasi', '2018-10-23 06:55:01', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (114, 11, 1, 1, 'Memberikan obat melalui oral', '2018-10-23 06:55:26', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (115, 11, 1, 2, 'Memberikan obat melalui intra vena', '2018-10-23 06:55:46', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (116, 10, 1, 3, 'Memberikan obat melalui injeksi sub kutan (insulin)', '2018-10-23 06:56:11', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (117, 11, 1, 4, 'Memberikan obat melalui intra kutan', '2018-10-23 06:56:40', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (118, 11, 1, 5, 'Memberikan obat melalui intra muskuler', '2018-10-23 06:57:30', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (119, 11, 1, 6, 'Memberikan obat melalui drips', '2018-10-23 06:57:43', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (120, 11, 1, 7, 'Memberikan obat melalui kulit topical/transdermal', '2018-10-23 06:58:18', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (121, 11, 1, 8, 'Memberikan obat melalui supositoria', '2018-10-23 06:58:59', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (122, 11, 1, 9, 'Memberikan obat melalui inhalasi (Nebulizer) ', '2018-10-23 06:59:25', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (123, 11, 1, 10, 'Memberikan obat melalui naso gastric tube (NGT)', '2018-10-23 06:59:47', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (124, 11, 1, 10, 'Memberikan obat tetes hidung', '2018-10-23 07:01:42', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (125, 11, 1, 12, 'Memberikan obat tetes telinga', '2018-10-23 07:02:12', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (126, 11, 1, 13, 'Memberikan obat tetes mata', '2018-10-23 07:02:42', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (127, 11, 1, 14, 'Memberikan obat per vaginam', '2018-10-23 07:03:02', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (128, 11, 1, 15, 'Mengikuti prinsip 6 benar dalam pemberian obat', '2018-10-23 07:03:56', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (129, 11, 1, 16, 'Memonitor efek samping obat yang telah diberikan ', '2018-10-23 07:04:43', '2018-10-23 07:07:28', NULL);
INSERT INTO `kewenangan_klinis` VALUES (130, 11, 2, 17, 'Memberikan terapi obat sedatif via parenteral', '2018-10-23 07:05:13', '2018-10-23 07:15:35', NULL);
INSERT INTO `kewenangan_klinis` VALUES (131, 11, 2, 18, 'Memberikan obat menggunakan infus/syring pump (Memasang Infus pump)', '2018-10-23 07:05:59', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (132, 11, 3, 19, 'Monitoring efek samping pengobatan', '2018-10-23 07:16:58', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (133, 11, 3, 20, 'Penatalaksanaan pemberian obat-obatan trombolitik', '2018-10-23 07:17:30', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (134, 11, 3, 21, 'Observasi dan monitoring pemberian obat high alert', '2018-10-23 07:18:11', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (135, 11, 3, 22, 'Pemberian obat melalui infus pump', '2018-10-23 07:24:55', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (136, 11, 3, 23, 'Pemberian obat melalui syringe  pump', '2018-10-23 07:25:21', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (137, 11, 3, 24, 'Penatalaksanaan pemberian obat-obat suppoortif', '2018-10-23 07:28:21', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (138, 12, 3, 1, 'Merawat kepatenen selang transfusi darah', '2018-10-23 07:29:15', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (139, 12, 1, 2, 'Melakukan spooling post transfusi', '2018-10-23 07:29:42', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (140, 12, 1, 3, 'Memonitoring efek samping pemberian produk darah ', '2018-10-23 07:30:08', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (141, 12, 3, 4, 'Perawatan daerah akses vaskular transfusi darah', '2018-10-23 07:30:37', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (142, 12, 2, 5, 'Memberikan dan monitor pemberian transfusi darah dan komponen darah (FPP, albumin) (Melakukan transfusi albumin) ', '2018-10-23 07:31:05', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (143, 12, 2, 6, 'Mengkaji respon pasien terhadap terapi/pengobatan ', '2018-10-23 07:31:54', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (144, 12, 3, 7, 'Pengambilan darah mix vein', '2018-10-23 07:32:54', '2018-10-23 07:34:34', NULL);
INSERT INTO `kewenangan_klinis` VALUES (145, 13, 1, 1, 'Melakukan pengkajian status nutrisi  (Mengukur berat badan, Tinggi badan, Lingkar lengan atas)', '2018-10-23 07:36:03', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (146, 13, 1, 2, 'Membantu pasien eliminasi (BAB dan BAK) (Membantu eliminasi BAB dengan suppositoria)', '2018-10-23 07:36:27', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (147, 13, 1, 3, 'Melakukan fleet enema', '2018-10-23 07:38:02', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (148, 13, 1, 4, 'Memberikan makan dan minum oral tanpa penyulit (Memberikan nutrisi secara oral)', '2018-10-23 07:38:40', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (149, 13, 1, 5, 'Memasang kateter urine pada pasien tanpa penyulit', '2018-10-23 07:41:24', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (150, 13, 1, 6, 'Melepas kateter urine pada pasien tanpa penyulit ', '2018-10-23 07:51:18', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (151, 13, 1, 7, 'Merawat kateter urine', '2018-10-23 07:52:06', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (152, 13, 1, 8, 'memberikan makan  melalui sonde', '2018-10-23 07:54:03', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (153, 13, 1, 9, 'Melakukan perawatan naso gastic tube (NGT)', '2018-10-23 07:54:25', '2018-10-23 07:55:10', NULL);
INSERT INTO `kewenangan_klinis` VALUES (154, 13, 1, 10, 'Melepas  naso gastric tube (NGT)', '2018-10-23 07:55:59', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (155, 13, 1, 11, 'Melakukan klisma (Melakukan huknah rendah dan huknah tinggi)', '2018-10-23 07:58:32', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (156, 13, 1, 12, 'Irigasi kateter urine', '2018-10-23 07:59:12', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (157, 13, 1, 13, 'Membantu pasien berkemih ditempat tidur dengan menggunakan pispot', '2018-10-23 07:59:32', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (158, 13, 2, 14, 'Memasang kateter urine dengan penyulit', '2018-10-23 08:00:20', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (159, 13, 2, 15, 'Memasang tube feeding tanpa penyulit ', '2018-10-23 08:00:55', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (160, 13, 3, 16, 'Skrening disfagia', '2018-10-23 08:01:33', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (161, 13, 3, 17, 'Melakukan intervensi  eating management disorders', '2018-10-23 08:04:02', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (162, 14, 1, 1, 'Melakukan pengkajian kebutuhan edukasi pasien ', '2018-10-23 08:04:50', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (163, 14, 1, 1, 'Melakukan pengkajian kebutuhan edukasi pasien ', '2018-10-23 08:04:50', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (164, 14, 1, 2, 'Mempersiapkan pasien pulang', '2018-10-23 08:05:35', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (165, 14, 1, 3, 'Pemberian edukasi tentang prinsip hand hygiene', '2018-10-23 08:06:07', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (166, 14, 2, 4, 'Mengevaluasi pemahaman  pasien terhadap asuhan keperawatan ', '2018-10-23 08:07:03', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (167, 14, 2, 5, 'Melakukan penelitian keperawatan sebagai peneliti pendamping', '2018-10-23 08:09:18', '2018-10-23 08:10:32', NULL);
INSERT INTO `kewenangan_klinis` VALUES (168, 14, 3, 6, 'Melakukan VCT (volunter counseling testing)', '2018-10-23 08:11:21', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (169, 14, 3, 7, 'CI bagi mahasiswa keperawatan dan perawat magang', '2018-10-23 08:11:48', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (170, 14, 3, 8, 'Melakukan riset keperawatan sebagai peneliti utama', '2018-10-23 08:12:12', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (171, 14, 3, 9, 'Konseling untuk masalah keperawatan  lebih kompleks', '2018-10-23 08:12:42', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (172, 14, 3, 10, 'Health education; penggunaan inhaler dosis terukur', '2018-10-23 08:13:24', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (173, 14, 3, 12, 'Health education ; toilet training (penkes; toilet training)', '2018-10-23 08:14:02', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (174, 15, 1, 1, 'Memfasilitasi pemenuhan kebutuhan psikologis, sosial, dan spritual', '2018-10-24 08:47:08', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (175, 16, 1, 1, 'Mengatur posisi tidur pasien', '2018-10-24 08:47:39', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (176, 16, 1, 2, 'Mempersiapkan tempat tidur pasien baru', '2018-10-24 08:48:04', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (177, 16, 1, 3, 'Memfasilitasi lingkungan yang mendukung kebutuhan istirahat', '2018-10-24 08:48:36', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (178, 17, 1, 1, 'Mobilisasi pasien tanpa penyulit (Melakukan mobilisasi secara bertahap)', '2018-10-24 08:49:03', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (179, 17, 1, 2, 'Mendampingi pasien dengan hambatan mobilitas fisik dari tempat tidur  ke kamar mandi  dan dari kamar mandi kembali ke kamar', '2018-10-24 08:49:27', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (180, 17, 1, 3, 'Mengajarkan range of motion aktif', '2018-10-24 08:49:47', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (181, 17, 1, 4, 'Melakukan range of motion pasif', '2018-10-24 08:50:02', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (182, 17, 1, 5, 'Melakukan ambulasi pasien pre dan post tindakan pemeriksaan penunjang ', '2018-10-24 08:50:21', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (183, 17, 1, 6, 'Melakukan ambulasi pasien pre dan post tindakan operasi', '2018-10-24 08:50:42', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (184, 17, 1, 7, 'Mengganti posisi pasien tiap 2 jam dengan miring 30  derajat (Mengatur posisi miring kiri, miring kanan, semifowler dll)', '2018-10-24 08:51:04', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (185, 17, 2, 8, 'Melatih range of motion (ROM) pasif/aktif', '2018-10-24 08:51:41', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (186, 17, 2, 9, 'Melakukan transfer pada pasien dengan penyulit ', '2018-10-24 08:52:03', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (187, 17, 3, 10, 'Ambulasi pasien dalam kondisi tidak stabil (kompleks)', '2018-10-24 08:53:42', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (188, 17, 3, 11, 'Melakukan penilaian kekuatan otot pasien ', '2018-10-24 08:54:09', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (189, 17, 3, 12, 'Monitoring pemindahan pasien (Antar unit yang memerlukan monitoring khusus)', '2018-10-24 08:54:33', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (190, 18, 1, 1, 'Melakukan pengkajian data dasar tentang seksualitas', '2018-10-24 08:54:59', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (191, 19, 1, 1, 'Mempersiapkan pasien untuk beberapa pemeriksaan radiologi (melepaskan semua perhiasan yang melekat terutama pada bagian leher dan kepala', '2018-10-24 08:55:44', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (192, 19, 1, 2, 'Membantu pasien mengosongkan kandung kemih sebelum lumbal punksi', '2018-10-24 08:56:48', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (193, 19, 1, 3, 'Persiapan pasien preoperasi ; puasa', '2018-10-24 08:57:04', '2018-10-24 08:57:32', NULL);
INSERT INTO `kewenangan_klinis` VALUES (194, 19, 1, 4, 'Persiapan prosedur PAP SMEAR ', '2018-10-24 08:58:07', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (195, 19, 1, 5, 'Persiapan prosedur USG Abdomen ', '2018-10-24 08:58:27', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (196, 19, 1, 6, 'Persiapan prosedur CT.Scan Abdomen', '2018-10-24 08:58:57', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (197, 19, 1, 7, 'Persiapan prosedur Colonscopy', '2018-10-24 08:59:22', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (198, 19, 1, 8, 'Pengambilan sampel sputum', '2018-10-24 08:59:45', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (199, 19, 1, 9, 'Pengambilan sampel darah ', '2018-10-24 09:00:13', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (200, 19, 1, 10, 'Pengambilan sampel urine', '2018-10-24 09:00:35', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (201, 19, 2, 11, 'Menyiapkan alat dan pasien untuk pleura punctie', '2018-10-24 09:00:57', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (202, 19, 3, 12, 'Asistensi untuk tindakan lumbal puntie', '2018-10-24 09:01:35', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (203, 20, 1, 1, 'membantu menggunakan atau mengganti pakaian pasien', '2018-10-24 09:02:19', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (204, 20, 1, 2, 'Melakukan oral hygiene', '2018-10-24 09:02:34', '2018-10-24 09:03:12', NULL);
INSERT INTO `kewenangan_klinis` VALUES (205, 20, 1, 3, 'Memfasilitasi kenyamanan fisik', '2018-10-24 09:03:58', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (206, 20, 1, 4, 'Melakukan  kompres hangat/dingin', '2018-10-24 09:04:26', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (207, 20, 1, 5, 'Melakukan bed making (Mengganti alat tenun)', '2018-10-24 09:04:47', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (208, 20, 1, 6, 'menciptakan lingkungan yang bersih, rapi , dan nyaman untuk pasien', '2018-10-24 09:05:12', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (209, 20, 1, 7, 'Memandikan pasien', '2018-10-24 09:05:39', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (210, 20, 1, 8, 'menyikat gigi/ membersihkan mulut', '2018-10-24 09:06:00', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (211, 20, 1, 9, 'mencuci rambut ', '2018-10-24 09:06:30', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (212, 20, 1, 10, 'menggunting kuku (Merawat kuku pasien)', '2018-10-24 09:07:17', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (213, 20, 1, 11, 'mengajarkan teknik relaksasi untuk kenyamanan ; napas dalam ', '2018-10-24 09:07:37', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (214, 20, 1, 12, 'mengajarkan teknik relaksasi untuk kenyamanan ; masase ', '2018-10-24 09:08:06', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (215, 20, 1, 13, 'latihan distraksi sederhana; pengalihan perhatian', '2018-10-24 09:08:31', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (216, 20, 1, 14, 'Melakukan guide imagery (Memberikan terapi komplementer)', '2018-10-24 09:09:05', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (217, 20, 2, 15, 'Manajemen kolaborasi penatalaksanaan nyeri skala berat', '2018-10-24 09:09:29', NULL, NULL);
INSERT INTO `kewenangan_klinis` VALUES (218, NULL, NULL, 1, 'Pengkajian awal keperawatan/kebidanan', '2018-11-08 22:30:51', '2018-11-09 11:20:53', NULL);
INSERT INTO `kewenangan_klinis` VALUES (219, NULL, NULL, 2, 'Melakukan pengkajian harian/lanjutan', '2018-11-08 22:31:04', '2018-11-09 11:20:55', NULL);
INSERT INTO `kewenangan_klinis` VALUES (220, NULL, NULL, 3, 'Merumuskan diagnosa keperawatan/kebidanan', '2018-11-08 22:31:41', '2018-11-09 11:20:57', NULL);
INSERT INTO `kewenangan_klinis` VALUES (221, NULL, NULL, 4, 'Merumuskan rencana keperawatan/kebidanan', '2018-11-08 22:31:54', '2018-11-09 11:20:59', NULL);
INSERT INTO `kewenangan_klinis` VALUES (222, NULL, NULL, 5, 'Melakukan evaluasi keperawatan/kebidanan', '2018-11-08 22:32:05', '2018-11-09 11:21:00', NULL);
INSERT INTO `kewenangan_klinis` VALUES (223, NULL, NULL, 6, 'Melakukan edukasi keperawatan/kebidanan', '2018-11-08 22:32:14', '2018-11-09 11:21:03', NULL);
INSERT INTO `kewenangan_klinis` VALUES (224, NULL, NULL, 7, 'Merencanakan pasien pulang/pindah (Discharge planning)', '2018-11-08 22:32:30', '2018-11-09 11:21:07', NULL);

-- ----------------------------
-- Table structure for logbook
-- ----------------------------
DROP TABLE IF EXISTS `logbook`;
CREATE TABLE `logbook`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `waktu` datetime(0) NULL DEFAULT NULL,
  `id_kewenangan_klinis` int(10) UNSIGNED NULL DEFAULT NULL,
  `id_pasien` int(10) UNSIGNED NULL DEFAULT NULL,
  `id_perawat` int(10) UNSIGNED NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tgl_id_kewenangan_klinis_id_pasien_id_perawat`(`waktu`, `id_kewenangan_klinis`, `id_pasien`, `id_perawat`) USING BTREE,
  INDEX `FK_logbook_kewenangan_klinis`(`id_kewenangan_klinis`) USING BTREE,
  INDEX `FK_logbook_pasien`(`id_pasien`) USING BTREE,
  INDEX `FK_logbook_users`(`id_perawat`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 55 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Logbook Perawat' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of logbook
-- ----------------------------
INSERT INTO `logbook` VALUES (1, '2019-01-01 15:10:17', 218, 1, 5, '2018-11-09 15:10:42', '2019-01-02 09:16:13', NULL);
INSERT INTO `logbook` VALUES (2, '2019-01-01 15:10:17', 220, 1, 5, '2018-11-09 15:10:42', '2019-01-02 09:16:17', NULL);
INSERT INTO `logbook` VALUES (3, '2019-01-01 15:10:17', 221, 1, 5, '2018-11-09 15:10:42', '2019-01-02 09:16:20', NULL);
INSERT INTO `logbook` VALUES (4, '2019-01-01 10:18:21', 36, 1, 5, '2018-11-23 11:40:52', '2019-01-02 09:16:43', NULL);
INSERT INTO `logbook` VALUES (5, '2019-01-01 10:18:21', 56, 1, 5, '2018-11-23 11:40:52', '2019-01-02 09:16:53', NULL);
INSERT INTO `logbook` VALUES (6, '2019-01-01 10:18:21', 178, 1, 5, '2018-11-23 11:40:52', '2019-01-02 09:16:57', NULL);
INSERT INTO `logbook` VALUES (7, '2019-01-01 13:28:40', 199, 1, 5, '2018-11-23 14:50:55', '2019-01-02 09:17:04', NULL);
INSERT INTO `logbook` VALUES (8, '2019-01-01 13:28:40', 73, 1, 5, '2018-11-23 14:50:55', '2019-01-02 09:17:08', NULL);
INSERT INTO `logbook` VALUES (9, '2019-01-01 13:28:40', 165, 1, 5, '2018-11-23 14:50:55', '2019-01-02 09:17:12', NULL);
INSERT INTO `logbook` VALUES (10, '2019-01-01 13:37:29', 62, 1, 5, '2018-11-23 14:59:31', '2019-01-02 09:17:16', NULL);
INSERT INTO `logbook` VALUES (11, '2019-01-01 10:00:00', 30, 1, 5, '2018-11-23 14:59:31', '2019-01-02 09:17:19', NULL);
INSERT INTO `logbook` VALUES (12, '2019-01-01 10:00:00', 206, 1, 5, '2018-11-23 14:59:31', '2019-01-02 09:17:24', NULL);
INSERT INTO `logbook` VALUES (13, '2019-01-01 16:02:36', 53, 1, 8, '2018-11-23 17:23:59', '2019-01-02 09:17:29', NULL);
INSERT INTO `logbook` VALUES (14, '2019-01-01 16:02:36', 116, 1, 8, '2018-11-23 17:23:59', '2019-01-02 09:17:33', NULL);
INSERT INTO `logbook` VALUES (15, '2019-01-01 14:45:26', 54, 2, 8, '2018-11-23 18:08:07', '2019-01-02 09:17:37', NULL);
INSERT INTO `logbook` VALUES (16, '2019-01-01 01:45:26', 53, 2, 8, '2018-11-23 18:08:07', '2019-01-02 09:17:42', NULL);
INSERT INTO `logbook` VALUES (17, '2019-01-01 19:56:16', 62, 2, 9, '2018-11-23 21:18:34', '2019-01-02 09:17:46', NULL);
INSERT INTO `logbook` VALUES (18, '2019-01-01 19:56:16', 73, 2, 9, '2018-11-23 21:18:34', '2019-01-02 09:17:50', NULL);
INSERT INTO `logbook` VALUES (19, '2019-01-01 20:26:39', 116, 2, 5, '2018-11-23 21:48:31', '2019-01-02 09:17:55', NULL);
INSERT INTO `logbook` VALUES (20, '2019-01-01 20:26:39', 209, 2, 5, '2018-11-23 21:48:31', '2019-01-02 09:18:06', NULL);
INSERT INTO `logbook` VALUES (21, '2019-01-01 20:26:39', 59, 2, 5, '2018-11-23 21:48:31', '2019-01-02 09:18:10', NULL);
INSERT INTO `logbook` VALUES (22, '2019-01-02 09:09:45', 209, 1, 5, '2018-11-24 13:03:18', '2019-01-02 09:18:17', NULL);
INSERT INTO `logbook` VALUES (23, '2019-01-02 12:20:47', 53, 2, 5, '2018-11-24 13:41:02', '2019-01-02 09:18:20', NULL);
INSERT INTO `logbook` VALUES (24, '2019-01-02 12:29:43', 216, 4, 5, '2018-11-24 13:50:50', '2019-01-02 09:18:25', NULL);
INSERT INTO `logbook` VALUES (25, '2019-01-02 12:29:43', 30, 4, 5, '2018-11-24 13:50:50', '2019-01-02 09:18:29', NULL);
INSERT INTO `logbook` VALUES (26, '2019-01-02 12:37:13', 162, 4, 5, '2018-11-24 13:57:38', '2019-01-02 09:18:33', NULL);
INSERT INTO `logbook` VALUES (27, '2019-01-02 08:41:42', 162, 1, 5, '2018-11-24 15:05:48', '2019-01-02 09:18:38', NULL);
INSERT INTO `logbook` VALUES (28, '2019-01-02 09:41:42', 145, 1, 5, '2018-11-24 15:05:48', '2019-01-02 09:18:43', NULL);
INSERT INTO `logbook` VALUES (29, '2019-01-02 10:41:42', 162, 1, 5, '2018-11-24 15:05:48', '2019-01-02 09:18:47', NULL);
INSERT INTO `logbook` VALUES (30, '2019-01-02 11:41:42', 157, 2, 5, '2018-11-24 15:09:59', '2019-01-02 09:18:52', NULL);
INSERT INTO `logbook` VALUES (31, '2019-01-02 11:41:42', 151, 2, 5, '2018-11-24 15:09:59', '2019-01-02 09:18:56', NULL);
INSERT INTO `logbook` VALUES (32, '2019-01-02 10:41:42', 155, 2, 5, '2018-11-24 15:09:59', '2019-01-02 09:19:00', NULL);
INSERT INTO `logbook` VALUES (33, '2019-01-02 14:51:54', 209, 4, 5, '2018-11-24 16:13:50', '2019-01-02 09:19:05', NULL);
INSERT INTO `logbook` VALUES (34, '2019-01-02 14:51:54', 53, 4, 5, '2018-11-24 16:13:50', '2019-01-02 09:19:09', NULL);
INSERT INTO `logbook` VALUES (35, '2019-01-02 17:17:58', 61, 2, 5, '2018-12-04 19:17:21', '2019-01-02 09:19:14', NULL);
INSERT INTO `logbook` VALUES (36, '2019-01-02 17:17:58', 209, 2, 5, '2018-12-04 19:17:21', '2019-01-02 09:19:18', NULL);
INSERT INTO `logbook` VALUES (37, '2019-01-03 13:46:53', 165, 1, 5, '2018-12-11 13:47:08', '2019-01-02 09:19:24', NULL);
INSERT INTO `logbook` VALUES (38, '2019-01-03 15:19:45', 209, 2, 5, '2018-12-11 15:20:31', '2019-01-02 09:19:30', NULL);
INSERT INTO `logbook` VALUES (39, '2019-01-03 15:19:45', 216, 2, 5, '2018-12-11 15:20:31', '2019-01-02 09:19:37', NULL);
INSERT INTO `logbook` VALUES (40, '2019-01-03 09:05:24', 2, 1, 5, '2018-12-13 09:23:51', '2019-01-02 09:19:44', NULL);
INSERT INTO `logbook` VALUES (41, '2019-01-03 09:05:24', 21, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:07', NULL);
INSERT INTO `logbook` VALUES (42, '2019-01-03 09:05:24', 18, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:12', NULL);
INSERT INTO `logbook` VALUES (43, '2019-01-03 09:05:24', 29, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:17', NULL);
INSERT INTO `logbook` VALUES (44, '2019-01-03 09:05:24', 55, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:21', NULL);
INSERT INTO `logbook` VALUES (45, '2019-01-03 09:05:24', 36, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:25', NULL);
INSERT INTO `logbook` VALUES (46, '2019-01-03 09:05:24', 57, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:35', NULL);
INSERT INTO `logbook` VALUES (47, '2019-01-03 09:05:24', 73, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:39', NULL);
INSERT INTO `logbook` VALUES (48, '2019-01-03 09:05:24', 80, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:45', NULL);
INSERT INTO `logbook` VALUES (49, '2019-01-03 09:05:24', 108, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:49', NULL);
INSERT INTO `logbook` VALUES (50, '2019-01-03 09:05:24', 75, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:52', NULL);
INSERT INTO `logbook` VALUES (51, '2019-01-03 09:05:24', 115, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:45:56', NULL);
INSERT INTO `logbook` VALUES (52, '2019-01-03 09:05:24', 89, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:46:03', NULL);
INSERT INTO `logbook` VALUES (53, '2019-01-03 09:05:24', 193, 1, 5, '2018-12-13 09:23:51', '2019-01-02 11:46:08', NULL);
INSERT INTO `logbook` VALUES (54, '2019-01-03 13:51:28', 209, 2, 5, '2018-12-13 17:52:31', '2019-01-02 11:46:11', NULL);

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for nilai
-- ----------------------------
DROP TABLE IF EXISTS `nilai`;
CREATE TABLE `nilai`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `periode` date NULL DEFAULT NULL,
  `id_perawat` int(10) UNSIGNED NULL DEFAULT NULL,
  `id_indikator` int(10) UNSIGNED NULL DEFAULT NULL,
  `target` float UNSIGNED NULL DEFAULT 0,
  `capaian` float NULL DEFAULT 0,
  `bobot` float NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `periode_id_perawat_id_indikator`(`periode`, `id_perawat`, `id_indikator`) USING BTREE,
  INDEX `FK_nilai_indikator`(`id_indikator`) USING BTREE,
  INDEX `FK_nilai_users`(`id_perawat`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 611 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Penilaian kinerja perawat' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of nilai
-- ----------------------------
INSERT INTO `nilai` VALUES (578, '2018-10-01', 4, 35, 1, 0, 1, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (577, '2018-10-01', 4, 34, 3, 0, 2, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (576, '2018-10-01', 4, 33, 3, 0, 2, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (575, '2018-10-01', 4, 32, 3, 0, 2, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (574, '2018-10-01', 4, 31, 100, 0, 4, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (573, '2018-10-01', 4, 30, 100, 0, 4, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (572, '2018-10-01', 4, 29, 100, 0, 1, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (571, '2018-10-01', 4, 28, 100, 0, 1, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (570, '2018-10-01', 4, 27, 100, 0, 1, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (569, '2018-10-01', 4, 26, 100, 0, 1, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (568, '2018-10-01', 4, 24, 100, 0, 2, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (567, '2018-10-01', 4, 23, 100, 0, 3, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (566, '2018-10-01', 4, 21, 100, 100, 4, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (565, '2018-10-01', 4, 20, 100, 100, 4, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (564, '2018-10-01', 4, 19, 100, 100, 5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (563, '2018-10-01', 4, 18, 90, 0, 2, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (562, '2018-10-01', 4, 17, 100, 0, 2, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (561, '2018-10-01', 4, 16, 100, 90, 2, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (560, '2018-10-01', 4, 15, 100, 100, 5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (559, '2018-10-01', 4, 14, 100, 90, 3, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (558, '2018-10-01', 4, 13, 100, 90, 5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (557, '2018-10-01', 4, 12, 100, 100, 3, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (556, '2018-10-01', 4, 11, 100, 100, 4, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (555, '2018-10-01', 4, 10, 100, 100, 4, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (554, '2018-10-01', 4, 9, 9, 15, 5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (553, '2018-10-01', 4, 8, 36, 33, 5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (552, '2018-10-01', 4, 7, 378, 378, 2, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (551, '2018-10-01', 4, 6, 605, 0, 5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (550, '2018-10-01', 4, 5, 756, 756, 2.5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (549, '2018-10-01', 4, 4, 378, 420, 2.5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (548, '2018-10-01', 4, 3, 180, 310, 3, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (547, '2018-10-01', 4, 2, 9, 15, 5, '2019-01-04 21:01:13', NULL, NULL);
INSERT INTO `nilai` VALUES (515, '2018-10-01', 2, 2, 9, 15, 5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (516, '2018-10-01', 2, 3, 180, 310, 3, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (517, '2018-10-01', 2, 4, 378, 420, 2.5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (518, '2018-10-01', 2, 5, 756, 756, 2.5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (519, '2018-10-01', 2, 6, 605, 2, 5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (520, '2018-10-01', 2, 7, 378, 378, 2, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (521, '2018-10-01', 2, 8, 36, 33, 5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (522, '2018-10-01', 2, 9, 9, 15, 5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (523, '2018-10-01', 2, 10, 100, 100, 4, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (524, '2018-10-01', 2, 11, 100, 100, 4, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (525, '2018-10-01', 2, 12, 100, 100, 3, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (526, '2018-10-01', 2, 13, 100, 100, 5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (527, '2018-10-01', 2, 14, 100, 100, 3, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (528, '2018-10-01', 2, 15, 100, 100, 5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (529, '2018-10-01', 2, 16, 100, 89, 2, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (530, '2018-10-01', 2, 17, 100, 98, 2, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (531, '2018-10-01', 2, 18, 90, 90, 2, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (532, '2018-10-01', 2, 19, 100, 100, 5, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (533, '2018-10-01', 2, 20, 100, 100, 4, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (534, '2018-10-01', 2, 21, 100, 100, 4, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (535, '2018-10-01', 2, 23, 100, 100, 3, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (536, '2018-10-01', 2, 24, 100, 100, 2, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (537, '2018-10-01', 2, 26, 100, 90, 1, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (538, '2018-10-01', 2, 27, 100, 100, 1, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (539, '2018-10-01', 2, 28, 100, 100, 1, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (540, '2018-10-01', 2, 29, 100, 100, 1, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (541, '2018-10-01', 2, 30, 100, 100, 4, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (542, '2018-10-01', 2, 31, 100, 100, 4, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (543, '2018-10-01', 2, 32, 3, 0, 2, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (544, '2018-10-01', 2, 33, 3, 0, 2, '2018-10-20 12:26:34', NULL, NULL);
INSERT INTO `nilai` VALUES (545, '2018-10-01', 2, 34, 3, 0, 2, '2018-10-20 12:26:35', NULL, NULL);
INSERT INTO `nilai` VALUES (546, '2018-10-01', 2, 35, 1, 0, 1, '2018-10-20 12:26:35', NULL, NULL);
INSERT INTO `nilai` VALUES (579, '2020-01-01', 7, 2, 9, 0, 5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (580, '2020-01-01', 7, 3, 180, 0, 3, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (581, '2020-01-01', 7, 4, 378, 0, 2.5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (582, '2020-01-01', 7, 5, 756, 0, 2.5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (583, '2020-01-01', 7, 6, 605, 0, 5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (584, '2020-01-01', 7, 7, 378, 0, 2, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (585, '2020-01-01', 7, 8, 36, 0, 5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (586, '2020-01-01', 7, 9, 9, 0, 5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (587, '2020-01-01', 7, 10, 100, 50, 4, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (588, '2020-01-01', 7, 11, 100, 50, 4, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (589, '2020-01-01', 7, 12, 100, 50, 3, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (590, '2020-01-01', 7, 13, 100, 50, 5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (591, '2020-01-01', 7, 14, 100, 50, 3, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (592, '2020-01-01', 7, 15, 100, 50, 5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (593, '2020-01-01', 7, 16, 100, 50, 2, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (594, '2020-01-01', 7, 17, 100, 50, 2, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (595, '2020-01-01', 7, 18, 90, 50, 2, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (596, '2020-01-01', 7, 19, 100, 50, 5, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (597, '2020-01-01', 7, 20, 100, 50, 4, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (598, '2020-01-01', 7, 21, 100, 50, 4, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (599, '2020-01-01', 7, 23, 100, 50, 3, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (600, '2020-01-01', 7, 24, 100, 50, 2, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (601, '2020-01-01', 7, 26, 100, 50, 1, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (602, '2020-01-01', 7, 27, 100, 50, 1, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (603, '2020-01-01', 7, 28, 100, 50, 1, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (604, '2020-01-01', 7, 29, 100, 50, 1, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (605, '2020-01-01', 7, 30, 100, 50, 4, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (606, '2020-01-01', 7, 31, 100, 50, 4, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (607, '2020-01-01', 7, 32, 3, 50, 2, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (608, '2020-01-01', 7, 33, 3, 50, 2, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (609, '2020-01-01', 7, 34, 3, 50, 2, '2020-09-14 12:39:20', NULL, NULL);
INSERT INTO `nilai` VALUES (610, '2020-01-01', 7, 35, 1, 50, 1, '2020-09-14 12:39:20', NULL, NULL);

-- ----------------------------
-- Table structure for pangkat
-- ----------------------------
DROP TABLE IF EXISTS `pangkat`;
CREATE TABLE `pangkat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pangkat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `golongan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `id_jabatan` int(11) UNSIGNED NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_pangkat_jabatan`(`id_jabatan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Pangkat perawat' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pangkat
-- ----------------------------
INSERT INTO `pangkat` VALUES (1, 'Pengatur', 'II.C', 1, '2018-09-02 07:21:18', '2018-09-02 07:21:20', NULL);
INSERT INTO `pangkat` VALUES (2, 'Pengatur TK I', 'II.D', 1, '2018-09-02 07:23:44', '2018-09-02 07:23:49', NULL);

-- ----------------------------
-- Table structure for pasien
-- ----------------------------
DROP TABLE IF EXISTS `pasien`;
CREATE TABLE `pasien`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `no_rm` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_pasien` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '',
  `sapaan` enum('Tn','Ny','Nn','An','By') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `no_rm`(`no_rm`) USING BTREE,
  INDEX `jenis_kelamin`(`jenis_kelamin`) USING BTREE,
  INDEX `tgl_lahir`(`tgl_lahir`) USING BTREE,
  INDEX `nama_pasien`(`nama_pasien`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pasien
-- ----------------------------
INSERT INTO `pasien` VALUES (1, '000001', 'Udin', 'Tn', 'L', '1993-02-03', '2018-09-06 22:03:37', '2019-01-02 00:02:53', NULL);
INSERT INTO `pasien` VALUES (2, '000002', 'Abu Rizal Bakri', 'Tn', 'L', '1990-06-28', '2018-09-06 22:41:37', NULL, NULL);
INSERT INTO `pasien` VALUES (4, '000003', 'Rahma', 'Nn', 'P', '1994-06-22', '2018-09-06 22:56:56', '2019-01-02 00:03:20', NULL);

-- ----------------------------
-- Table structure for pengaturan
-- ----------------------------
DROP TABLE IF EXISTS `pengaturan`;
CREATE TABLE `pengaturan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameter` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `nilai` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Referensi pengaturan aplikasi' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengaturan
-- ----------------------------
INSERT INTO `pengaturan` VALUES (1, 'nama_rs', 'RSUP Dr. Wahidin Sudirohusodo', '', '2018-09-15 08:34:32', '2018-10-13 14:56:51', NULL);
INSERT INTO `pengaturan` VALUES (2, 'alamat_rs', 'Jl. Perintis Kemerdekaan VII KM.11, Tamalanrea Indah, Tamalanrea, Kota Makassar, Sulawesi Selatan 90245', '', '2018-09-15 08:44:22', '2018-09-15 08:50:39', NULL);

-- ----------------------------
-- Table structure for unit_kerja
-- ----------------------------
DROP TABLE IF EXISTS `unit_kerja`;
CREATE TABLE `unit_kerja`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_unit_kerja` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'Unit kerja / ruangan' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unit_kerja
-- ----------------------------
INSERT INTO `unit_kerja` VALUES (1, 'Lontara 1 Atas Belakang', '2018-09-02 07:53:05', '2018-09-15 07:24:31', NULL);
INSERT INTO `unit_kerja` VALUES (3, 'Lontara 1 Atas Depan', '2018-10-12 14:56:59', '2018-11-21 22:33:18', NULL);
INSERT INTO `unit_kerja` VALUES (4, 'Lontara 1  bawah belakang', '2018-10-22 21:27:24', '2018-11-22 08:15:59', NULL);
INSERT INTO `unit_kerja` VALUES (5, 'Lontara 1 Bawah Depan', '2018-10-22 21:28:12', '2018-11-22 08:18:17', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activation_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `forgotten_password_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `forgotten_password_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `remember_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  `nip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama_lengkap` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_pangkat` int(11) UNSIGNED NULL DEFAULT NULL,
  `id_unit_kerja` int(11) UNSIGNED NULL DEFAULT NULL,
  `jenjang_pk` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1600058336, 1, '', 'Administrator', NULL, NULL, NULL);
INSERT INTO `users` VALUES (2, '127.0.0.1', 'perawat', '$2y$08$WgDAI6.3boyXOF4iGrEF7O3NFx2HHEhS2dQSvMP3ph8EfRBaxWy1a', '', 'perawat@perawat.com', '', NULL, NULL, NULL, 1268889823, 1540815081, 1, '198608162014022001', 'Jane Doe, AMK', 1, 1, 1);
INSERT INTO `users` VALUES (4, '127.0.0.1', 'akbar', '$2y$08$E9YdO5eU64kcdUUYV.9wD.jW2VqQ4K3wUgHv.Ah9xzs7TPn2k9rXC', NULL, 'aksa.uncp@gmail.com', '', '', NULL, '', 1535896958, 1546399565, 1, '196411271986021001', 'Akbar Syarif', 1, 1, 2);
INSERT INTO `users` VALUES (5, '114.125.182.175', 'rumahsakit', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, 'armila@gmail.com', '', '', NULL, '', 1540513053, 1546401059, 1, '197607222005012007', 'Perawat A', 2, 1, 1);
INSERT INTO `users` VALUES (6, '114.125.166.13', 'l1db', '$2y$08$IcTYaVsDC2zXpJlf9j22Z.8SA4htqDPRsIRvPE6GX/fRzpeqDmW7m', NULL, 'aisyahsupri212@gmail.com', '', '', NULL, '', 1540513178, 1542957073, 1, '198211192008012010', 'Perawat B', 2, 1, 2);
INSERT INTO `users` VALUES (7, '192.168.194.1', 'lontara1ab', '$2y$08$fsalai/FbNnPBD4rEIEQtuHwA5Xnk2WCGoU8fUhSjVKfZ.zcMSCGm', NULL, 'aisyah@yahoo.com', '', '', NULL, '', 1542843954, 1542942809, 1, '19821119200801210', 'Perawat C', 2, 1, 3);
INSERT INTO `users` VALUES (8, '192.168.194.1', 'lontara1ad', '$2y$08$NOjmeRCM2Xw2/Yb8we2DyuZD7UvuHup8ivmfES1p7WxvdxX7MCrvS', NULL, 'Dea@gmail.com', '', '', NULL, '', 1542845195, 1542967377, 1, '197607222005012008', 'Perawat D', 1, 3, 1);
INSERT INTO `users` VALUES (9, '192.168.194.1', 'rswahidin', '$2y$08$C5lm/eahCNbY.VD9jfV8yO9xwikW0ngECfJ5Xc2pxiOHUiLrp6Tzq', NULL, 'ela@gmail.com', '', '', NULL, '', 1542845384, 1542978791, 1, '197607222005012009', 'Perawat E', 2, 3, 2);
INSERT INTO `users` VALUES (10, '192.168.194.1', 'rsws', '$2y$08$fV5HYn9tbztRbV.FNdmUTuJanCOTC0nw3yMQuZg2Qjiyo323Tvc9S', NULL, 'fara@gmail.com', '', '', NULL, '', 1542845491, NULL, 1, '197607222005012010', 'Perawat F', 2, 3, 3);

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_users_groups`(`user_id`, `group_id`) USING BTREE,
  INDEX `fk_users_groups_users1_idx`(`user_id`) USING BTREE,
  INDEX `fk_users_groups_groups1_idx`(`group_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (1, 1, 1);
INSERT INTO `users_groups` VALUES (3, 2, 2);
INSERT INTO `users_groups` VALUES (5, 4, 2);
INSERT INTO `users_groups` VALUES (6, 5, 2);
INSERT INTO `users_groups` VALUES (7, 6, 2);
INSERT INTO `users_groups` VALUES (8, 7, 2);
INSERT INTO `users_groups` VALUES (9, 8, 2);
INSERT INTO `users_groups` VALUES (10, 9, 2);
INSERT INTO `users_groups` VALUES (11, 10, 2);

-- ----------------------------
-- View structure for v_indikator
-- ----------------------------
DROP VIEW IF EXISTS `v_indikator`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_indikator` AS select `indikator`.`id` AS `id`,`indikator`.`id_kategori` AS `id_kategori`,`indikator`.`urutan` AS `urutan_indikator`,`indikator`.`nama_indikator` AS `nama_indikator`,`indikator`.`parent` AS `id_parent`,`parent`.`nama_indikator` AS `nama_parent`,`indikator`.`bobot` AS `bobot`,`indikator`.`satuan` AS `satuan`,`indikator`.`target` AS `target`,exists(select 1 from `indikator` `tmp` where `tmp`.`parent` = `indikator`.`id`) AS `has_child`,`indikator`.`created_at` AS `created_at`,`indikator`.`updated_at` AS `updated_at`,`indikator`.`deleted_at` AS `deleted_at`,`kategori_indikator`.`nama_kategori` AS `nama_kategori`,`kategori_indikator`.`urutan` AS `urutan_kategori` from ((`indikator` left join `kategori_indikator` on(`indikator`.`id_kategori` = `kategori_indikator`.`id`)) left join `indikator` `parent` on(`indikator`.`id_kategori` = `parent`.`id`)) order by `kategori_indikator`.`urutan`,`indikator`.`urutan` ;

-- ----------------------------
-- View structure for v_kewenangan_klinis
-- ----------------------------
DROP VIEW IF EXISTS `v_kewenangan_klinis`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_kewenangan_klinis` AS select `kewenangan_klinis`.`id` AS `id`,`kewenangan_klinis`.`id_kategori` AS `id_kategori`,`kategori_kewenangan_klinis`.`nama_kategori` AS `nama_kategori`,`kewenangan_klinis`.`jenjang_pk` AS `jenjang_pk`,case `kewenangan_klinis`.`jenjang_pk` when 1 then 'PK I' when 2 then 'PK II' when 3 then 'PK III' when 4 then 'PK IV' when 5 then 'PK V' else '' end AS `nama_jenjang_pk`,`kewenangan_klinis`.`urutan` AS `urutan`,`kewenangan_klinis`.`nama_kompetensi` AS `nama_kompetensi`,`kewenangan_klinis`.`created_at` AS `created_at`,`kewenangan_klinis`.`deleted_at` AS `deleted_at`,`kewenangan_klinis`.`updated_at` AS `updated_at` from (`kewenangan_klinis` join `kategori_kewenangan_klinis` on(`kewenangan_klinis`.`id_kategori` = `kategori_kewenangan_klinis`.`id`)) ;

-- ----------------------------
-- View structure for v_logbook
-- ----------------------------
DROP VIEW IF EXISTS `v_logbook`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_logbook` AS select `logbook`.`id` AS `id`,`logbook`.`waktu` AS `waktu`,`logbook`.`id_kewenangan_klinis` AS `id_tindakan`,`logbook`.`id_pasien` AS `id_pasien`,`logbook`.`id_perawat` AS `id_perawat`,`logbook`.`created_at` AS `created_at`,`logbook`.`updated_at` AS `updated_at`,`logbook`.`deleted_at` AS `deleted_at`,`kewenangan_klinis`.`nama_kompetensi` AS `nama_tindakan`,`kewenangan_klinis`.`urutan` AS `urutan`,`kewenangan_klinis`.`jenjang_pk` AS `jenjang_pk`,case `kewenangan_klinis`.`jenjang_pk` when 1 then 'PK I' when 2 then 'PK II' when 3 then 'PK III' when 4 then 'PK IV' when 5 then 'PK V' else '' end AS `nama_jenjang_pk`,`kewenangan_klinis`.`id_kategori` AS `id_kategori`,`kategori_kewenangan_klinis`.`nama_kategori` AS `nama_kategori`,`pasien`.`no_rm` AS `no_rm`,`pasien`.`nama_pasien` AS `nama_pasien`,`pasien`.`jenis_kelamin` AS `jenis_kelamin_pasien`,`pasien`.`tgl_lahir` AS `tgl_lahir`,`perawat`.`nip` AS `nip`,`perawat`.`nama_lengkap` AS `nama_perawat` from ((((`logbook` join `kewenangan_klinis` on(`logbook`.`id_kewenangan_klinis` = `kewenangan_klinis`.`id`)) left join `kategori_kewenangan_klinis` on(`kewenangan_klinis`.`id_kategori` = `kategori_kewenangan_klinis`.`id`)) join `pasien` on(`logbook`.`id_pasien` = `pasien`.`id`)) join `users` `perawat` on(`logbook`.`id_perawat` = `perawat`.`id`)) ;

-- ----------------------------
-- View structure for v_nilai
-- ----------------------------
DROP VIEW IF EXISTS `v_nilai`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_nilai` AS select `nilai`.`id` AS `id`,`nilai`.`periode` AS `periode`,`nilai`.`id_perawat` AS `id_perawat`,`nilai`.`id_indikator` AS `id_indikator`,`nilai`.`capaian` AS `capaian`,`nilai`.`target` AS `target`,`nilai`.`bobot` AS `bobot`,ifnull(format(`nilai`.`capaian` / `nilai`.`target` * `nilai`.`bobot`,1),0) AS `nilai`,`nilai`.`created_at` AS `created_at`,`nilai`.`updated_at` AS `updated_at`,`nilai`.`deleted_at` AS `deleted_at` from (`nilai` join `indikator` on(`nilai`.`id_indikator` = `indikator`.`id`)) ;

-- ----------------------------
-- View structure for v_pangkat
-- ----------------------------
DROP VIEW IF EXISTS `v_pangkat`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_pangkat` AS select `pangkat`.`id` AS `id`,`pangkat`.`nama_pangkat` AS `nama_pangkat`,`pangkat`.`golongan` AS `golongan`,`pangkat`.`id_jabatan` AS `id_jabatan`,`pangkat`.`created_at` AS `created_at`,`jabatan`.`nama_jabatan` AS `nama_jabatan` from (`pangkat` join `jabatan` on(`pangkat`.`id_jabatan` = `jabatan`.`id`)) ;

-- ----------------------------
-- View structure for v_pasien
-- ----------------------------
DROP VIEW IF EXISTS `v_pasien`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_pasien` AS select `pasien`.`id` AS `id`,`pasien`.`no_rm` AS `no_rm`,`pasien`.`nama_pasien` AS `nama_pasien`,`pasien`.`sapaan` AS `sapaan`,concat(`pasien`.`nama_pasien`,', ',`pasien`.`sapaan`) AS `nama_lengkap_pasien`,`pasien`.`jenis_kelamin` AS `jenis_kelamin`,`pasien`.`tgl_lahir` AS `tgl_lahir`,`pasien`.`created_at` AS `created_at`,`pasien`.`updated_at` AS `updated_at`,`pasien`.`deleted_at` AS `deleted_at` from `pasien` ;

-- ----------------------------
-- View structure for v_perawat
-- ----------------------------
DROP VIEW IF EXISTS `v_perawat`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_perawat` AS select `users`.`id` AS `id`,`users`.`username` AS `username`,`users`.`email` AS `email`,`users`.`nama_lengkap` AS `nama_lengkap`,`users`.`id_pangkat` AS `id_pangkat`,`users`.`jenjang_pk` AS `jenjang_pk`,case `users`.`jenjang_pk` when 1 then 'PK I' when 2 then 'PK II' when 3 then 'PK III' when 4 then 'PK IV' when 5 then 'PK V' else '' end AS `nama_jenjang_pk`,`pangkat`.`nama_pangkat` AS `nama_pangkat`,`pangkat`.`golongan` AS `golongan`,`pangkat`.`id_jabatan` AS `id_jabatan`,`jabatan`.`nama_jabatan` AS `nama_jabatan`,`users`.`nip` AS `nip`,`users`.`id_unit_kerja` AS `id_unit_kerja`,`unit_kerja`.`nama_unit_kerja` AS `nama_unit_kerja` from (((((`users` join `users_groups` on(`users_groups`.`user_id` = `users`.`id`)) join `groups` on(`users_groups`.`group_id` = `groups`.`id`)) left join `pangkat` on(`users`.`id_pangkat` = `pangkat`.`id`)) join `jabatan` on(`pangkat`.`id_jabatan` = `jabatan`.`id`)) left join `unit_kerja` on(`users`.`id_unit_kerja` = `unit_kerja`.`id`)) ;

-- ----------------------------
-- View structure for v_tindakan
-- ----------------------------
DROP VIEW IF EXISTS `v_tindakan`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_tindakan` AS select `kewenangan_klinis`.`id` AS `id`,`kewenangan_klinis`.`id_kategori` AS `id_kategori`,`kategori_kewenangan_klinis`.`nama_kategori` AS `nama_kategori`,`kewenangan_klinis`.`jenjang_pk` AS `jenjang_pk`,case `kewenangan_klinis`.`jenjang_pk` when 1 then 'PK I' when 2 then 'PK II' when 3 then 'PK III' when 4 then 'PK IV' when 5 then 'PK V' else '' end AS `nama_jenjang_pk`,`kewenangan_klinis`.`urutan` AS `urutan`,`kewenangan_klinis`.`nama_kompetensi` AS `nama_kompetensi`,`kewenangan_klinis`.`created_at` AS `created_at`,`kewenangan_klinis`.`deleted_at` AS `deleted_at`,`kewenangan_klinis`.`updated_at` AS `updated_at` from (`kewenangan_klinis` left join `kategori_kewenangan_klinis` on(`kewenangan_klinis`.`id_kategori` = `kategori_kewenangan_klinis`.`id`)) ;

-- ----------------------------
-- View structure for v_users
-- ----------------------------
DROP VIEW IF EXISTS `v_users`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_users` AS select `users`.`id` AS `id`,`users`.`ip_address` AS `ip_address`,`users`.`username` AS `username`,`users`.`salt` AS `salt`,`users`.`email` AS `email`,`users`.`activation_code` AS `activation_code`,`users`.`remember_code` AS `remember_code`,from_unixtime(`users`.`created_on`) AS `created_on`,from_unixtime(`users`.`last_login`) AS `last_login`,`users`.`active` AS `active`,`users`.`nama_lengkap` AS `nama_lengkap`,`users`.`forgotten_password_code` AS `forgotten_password_code`,from_unixtime(`users`.`forgotten_password_time`) AS `forgotten_password_time`,`users_groups`.`group_id` AS `group_id`,`groups`.`name` AS `group_name`,`groups`.`description` AS `group_description` from ((`users` join `users_groups` on(`users_groups`.`user_id` = `users`.`id`)) join `groups` on(`groups`.`id` = `users_groups`.`group_id`)) where `groups`.`name` <> 'perawat' and `groups`.`name` <> 'kepala_ruangan' ;

-- ----------------------------
-- Procedure structure for px_dashboard_badge_info
-- ----------------------------
DROP PROCEDURE IF EXISTS `px_dashboard_badge_info`;
delimiter ;;
CREATE PROCEDURE `px_dashboard_badge_info`()
BEGIN
	SELECT
		(
			SELECT COUNT(*) FROM v_perawat WHERE jenjang_pk = 1
		) AS jumlah_perawat_pk_1,
		(
			SELECT COUNT(*) FROM v_perawat WHERE jenjang_pk = 2
		) AS jumlah_perawat_pk_2,
		(
			SELECT COUNT(*) FROM v_perawat WHERE jenjang_pk = 3
		) AS jumlah_perawat_pk_3,
		(
			SELECT COUNT(*) FROM v_perawat WHERE jenjang_pk = 4
		) AS jumlah_perawat_pk_4,
		(
			SELECT COUNT(*) FROM v_perawat WHERE jenjang_pk = 5
		) AS jumlah_perawat_pk_5,
		(
			SELECT COUNT(*) FROM v_perawat
		) AS total_perawat
	;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for px_dashboard_log_book_chart
-- ----------------------------
DROP PROCEDURE IF EXISTS `px_dashboard_log_book_chart`;
delimiter ;;
CREATE PROCEDURE `px_dashboard_log_book_chart`(IN `in_begin_date` date,IN `in_end_date` date,IN `in_id_perawat` VARCHAR(11))
BEGIN	
	IF in_id_perawat = '' THEN
		SELECT
			DATE(waktu) AS tgl,
			COUNT(*) AS jumlah_log_book
		FROM logbook
		WHERE DATE(waktu) >= DATE(in_begin_date) AND
					DATE(waktu) <= DATE(in_end_date)
		GROUP BY DATE(waktu)
		ORDER BY waktu ASC;
	ELSE
		SELECT
			DATE(waktu) AS tgl,
			COUNT(*) AS jumlah_log_book
		FROM logbook
		WHERE id_perawat = in_id_perawat AND
					DATE(waktu) >= DATE(in_begin_date) AND
					DATE(waktu) <= DATE(in_end_date)
		GROUP BY DATE(waktu)
		ORDER BY waktu ASC;
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for px_penilaian_per_perawat
-- ----------------------------
DROP PROCEDURE IF EXISTS `px_penilaian_per_perawat`;
delimiter ;;
CREATE PROCEDURE `px_penilaian_per_perawat`(IN `in_id_perawat` int,IN `in_periode` date)
BEGIN
	SELECT
		tmp_tbl.id,
		tmp_tbl.id_kategori,
		tmp_tbl.nama_kategori,
		tmp_tbl.nama_indikator,
		tmp_tbl.parent,
		tmp_tbl.has_child,
		tmp_tbl.satuan,
		tmp_tbl.target,
		tmp_tbl.bobot,
		IF(tmp_tbl.has_child = 0, tmp_tbl.capaian, NULL) AS capaian,
		IF(tmp_tbl.has_child = 0, IFNULL(FORMAT((tmp_tbl.capaian / tmp_tbl.target) * tmp_tbl.bobot, 1), 0.0), NULL) AS nilai
	FROM
	(
		SELECT
			indikator.id,
			indikator.id_kategori,
			kategori_indikator.nama_kategori,
			indikator.nama_indikator,
			indikator.parent,
			indikator.satuan,
			IFNULL(
				(
					SELECT target
					FROM nilai 
					WHERE
						id_indikator = indikator.id AND
						id_perawat = in_id_perawat AND 
						DATE_FORMAT( periode, '%Y-%m' ) = DATE_FORMAT( in_periode, '%Y-%m' )
					ORDER BY periode DESC
					LIMIT 1
				),
				indikator.target
			) AS target,
			IFNULL(
				(
					SELECT bobot
					FROM nilai 
					WHERE
						id_indikator = indikator.id AND
						id_perawat = in_id_perawat AND 
						DATE_FORMAT( periode, '%Y-%m' ) = DATE_FORMAT( in_periode, '%Y-%m' )
					ORDER BY periode DESC
					LIMIT 1
				),
				indikator.bobot
			) AS bobot,
			IF(
				indikator.id = 6,
				(
					SELECT COUNT(*)
					FROM logbook
					WHERE
						id_perawat = in_id_perawat AND 
						DATE_FORMAT( waktu, '%Y-%m' ) = DATE_FORMAT( in_periode, '%Y-%m' )
				),
				(
					IFNULL(
						(
							SELECT capaian
							FROM nilai 
							WHERE
								id_indikator = indikator.id AND
								id_perawat = in_id_perawat AND 
								DATE_FORMAT( periode, '%Y-%m' ) = DATE_FORMAT( in_periode, '%Y-%m' )
							ORDER BY periode DESC
							LIMIT 1
						),
						0
					)
				)
			) AS capaian,
			EXISTS(
				SELECT * FROM indikator x WHERE x.parent = indikator.id
			) AS has_child
		FROM indikator
		JOIN kategori_indikator ON indikator.id_kategori = kategori_indikator.id
		ORDER BY
			indikator.id_kategori ASC,
			indikator.urutan ASC
	) AS tmp_tbl;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for px_rekap_penilaian_per_unit_kerja
-- ----------------------------
DROP PROCEDURE IF EXISTS `px_rekap_penilaian_per_unit_kerja`;
delimiter ;;
CREATE PROCEDURE `px_rekap_penilaian_per_unit_kerja`(IN `in_id_unit_kerja` int, IN `in_periode` date)
BEGIN
	SELECT
		a.id,
		a.nip,
		a.nama_lengkap,
		a.nama_pangkat,
		a.golongan,
		a.nama_jabatan,
		a.nama_unit_kerja,
		EXISTS (
			SELECT id FROM nilai WHERE id_perawat = a.id AND DATE_FORMAT(periode,'%Y-%m') = DATE_FORMAT(in_periode,'%Y-%m')
		) AS sudah_dinilai,
		IFNULL(
			(
				SELECT SUM(nilai) FROM v_nilai WHERE id_perawat = a.id AND DATE_FORMAT(periode,'%Y-%m') = DATE_FORMAT(in_periode,'%Y-%m')
			), 0
		) AS nilai
	FROM v_perawat a
	WHERE id_unit_kerja = in_id_unit_kerja;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for px_tindakan_perawat
-- ----------------------------
DROP PROCEDURE IF EXISTS `px_tindakan_perawat`;
delimiter ;;
CREATE PROCEDURE `px_tindakan_perawat`(IN `in_id_perawat` int,IN `in_periode` date)
BEGIN
	SELECT
		logbook.*,
		kewenangan_klinis.nama_kompetensi AS nama_tindakan,
		kategori_kewenangan_klinis.nama_kategori,
		COUNT(kewenangan_klinis.id_kategori) AS jumlah_tindakan
	FROM logbook
	JOIN kewenangan_klinis ON logbook.id_kewenangan_klinis = kewenangan_klinis.id
	JOIN kategori_kewenangan_klinis ON kewenangan_klinis.id_kategori = kategori_kewenangan_klinis.id
	WHERE 
		id_perawat = in_id_perawat AND
		DATE_FORMAT(waktu,'%Y-%m') = DATE_FORMAT(in_periode,'%Y-%m')
	GROUP BY kategori_kewenangan_klinis.id
	ORDER BY kategori_kewenangan_klinis.urutan ASC;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
