/**
 * Generate random unique code with chance.js
 * @param  Integer
 * @return String
 */
function generateUniqueCode(length=10, pool='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789') {
	return chance.string({
		length: length,
		pool: pool
	});
};


/**
 * Villages dinamyc dropdown
 * @param  Object property of configuration
 * @return Void
 */
function villagesDinamycDropdown(config) {
	config.empty_first = typeof config.empty_first !== "undefined" ? config.empty_first : true;
	config.first_label = typeof config.first_label !== "undefined" ? config.first_label : "- Pilih -";

	if (config.districtId == '' || typeof config.districtId == 'undefined') {
		console.log('Error : ID Kecamatan harus diisi.');
	}
	else {
		$.ajax({
			url: config.url,
			method: 'GET',
			data: {
				district_id: config.districtId
			},
			dataType: 'json',
			beforeSend: function() {
				if (typeof config.beforeSend != 'undefined') {
					config.beforeSend();
				}
			},
			success: function(xhr) {
				try	{
					if (xhr.metadata.code !== 200) {
						swal({
							title: 'Oops..',
							type: 'error',
							html: true,
							text: xhr.metadata.message
						});
					}
					else {
						let r = xhr.response;

						html = '';

						if (config.empty_first) {
							html += '<option value="">'+ config.first_label +'</option>';
						}

						$(config.target).html(function() {
							for (i=0; i<=r.data.length - 1; i++) {
								var current_option = r.data[i];

								if (typeof config.selected != 'undefined') {
									if (config.selected == current_option.VillageId) {
										html += '<option selected value="'+ current_option.VillageId +'">'+ current_option.VillageName +'</option>';
									}
									else {
										html += '<option value="'+ current_option.VillageId +'">'+ current_option.VillageName +'</option>';
									}
								}
								else {
									html += '<option value="'+ current_option.VillageId +'">'+ current_option.VillageName +'</option>';
								}
							}

							return html;
						});
					}
				}
				catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
					console.log(e);
				}
			},
			error: function(xhr) {
				try {
					let error = JSON.parse(xhr.responseText);
					swal({
						title: 'Oops..',
						type: 'error',
						html: true,
						text: error.metadata.message
					});
				}
				catch(e) {
					swal('Oops..', 'Terjadi kesalahan', 'error');
					console.log(e);
				}
			},
			complete: function() {
				if (typeof config.complete != 'undefined') {
					config.complete();
				}
			}
		})
	}
}