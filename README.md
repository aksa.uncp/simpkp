# SIMPKP #

Sistem Informasi Penilaian Kinerja Perawat

### Cara Install ###

* Copy isi folder public sesuai nama foldernya masing-masing. Misalnya isi folder config, copy ke folder application/config
* Edit isi filenya sesuai konfigurasi server yang digunakan
* Untuk database menggunakan MySQL, buat database baru dengan nama simpkp (sesuaikan dengan konfigurasi di file database.php)
* Import file mysql.sql yang terletak di folder sql/mysql.sql
